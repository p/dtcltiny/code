Name: 		dtcltiny
Summary: 	A modeltrain control program.
Summary(de):	Lokomotivsteuerung für eine digitale Modelleisenbahn
Version:	0.8.4
Release: 	1%{?dist}
Source:		%{name}-%{version}.tar.bz2
Group:          Amusements/Games
License:	GPL
URL:		http://dtcltiny.sourceforge.net/
Prefix:		/usr
Requires:	qt3
BuildRequires:	glibc-devel qt3-devel qt3-devel-tools
BuildRoot:	%{_tmppath}/%{name}-%{version}-buildroot

%description
dtcltiny is a Qt based SRCP-client to control digital modeltrains.

Author:
--------
  Markus Pfeiffer

%description -l de
dtcltiny ist eine grafischer SRCP-Client zur Steuerung von Lokomotiven
auf einer digitalen Modelleisenbahn. dtcltiny benötigt einen
SRCP-Server (z.B. erddcd oder srcpd) mit Protokollversion 0.8.x.

Autor:
--------
  Markus Pfeiffer

%prep
%setup

%build
CFLAGS=$RPM_OPT_FLAGS \
%configure --prefix=%{_prefix} --with-doc-dir=%{_docdir}
make

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install-strip

install -d $RPM_BUILD_ROOT%{_datadir}/applications
install -p -m 644 %{name}.SuSE.desktop "$RPM_BUILD_ROOT%{_datadir}/applications/%{name}.desktop"

for i in AUTHORS COPYING INSTALL README TODO NEWS ChangeLog ; do
  install -p -m 644 $i "$RPM_BUILD_ROOT%{_docdir}/%{name}/$i"
done

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
# %doc README AUTHORS COPYING INSTALL ChangeLog TODO NEWS
%{_bindir}/%{name}
%{_datadir}/%{name}/*
%{_datadir}/pixmaps/%{name}*
%{_datadir}/applications/%{name}.desktop
%docdir %{_docdir}/%{name}
%{_docdir}/%{name}

%changelog
* Sun Jul 29 2007 Guido Scholz <guido.scholz@bayernline.de>
- Version updated
- Updated package dependencies

* Sun Feb 27 2005 Guido Scholz <guido.scholz@bayernline.de>
- Changed rpm package group

* Thu Nov 03 2004 Guido Scholz <guido.scholz@bayernline.de>
- Update to version 0.3.6-qt3-03

* Thu Oct 28 2004 Guido Scholz <guido.scholz@bayernline.de>
- Update to version 0.3.6-qt3-02

* Tue Oct 19 2004 Guido Scholz <guido.scholz@bayernline.de>
- Update to Qt3 port

