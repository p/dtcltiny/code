/***************************************************************************
                               LocoDialog.cpp
                             -------------------
    begin                : 11.11.2000
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "LocoDialog.h"
#include "Loco.h"
#include "pixmaps/fileopen.xpm"


LocoDialog::LocoDialog(QWidget* parent, Loco *lc,
                       LocoControl* locoC):
    QDialog(parent, "LocoDialog", true)
{
    if (loadDecoderFile() && loadProtocolFile()) 
    {
    		if (lc == 0) 
    		{
    			dlgType = NEW_LOCO;
    			loco = new Loco(NULL, "newLoco");	// create new default loco
    		}
        else 
        {
        	dlgType = PROPERTIES;
        	loco = lc;
        }
        contr = locoC;
        QVBoxLayout* baseLayout = new QVBoxLayout(this, 10, 10,
                "baseLayout");

        tabs = new QTabWidget(this, "Tabs");
        baseLayout->addWidget(tabs);

        setupTabGeneral();
        setupTabFuncs();
        setupTabSpeeds();

        QHBoxLayout* buttonLayout = new QHBoxLayout(baseLayout, 6,
                "buttonLayout");
        buttonLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                    QSizePolicy::Minimum));

        if (dlgType == NEW_LOCO) {
            setCaption(tr("Add a new Loco"));
            QPushButton* defaultButton =
                new QPushButton(tr("Default"), this, "defaultButton");
            connect(defaultButton, SIGNAL(clicked()), SLOT(slotDefault()));
            buttonLayout->addWidget(defaultButton);
            // set up default values
            slotDefault();      
        }
        else if (dlgType == PROPERTIES) {
            setCaption(tr("Change Loco Properties"));
            QPushButton* resetButton =
                new QPushButton(tr("&Reset"), this, "resetButton");
            connect(resetButton, SIGNAL(clicked()), SLOT(slotGetData()));
            buttonLayout->addWidget(resetButton);
            slotGetData();
        }

        QPushButton* okButton = new QPushButton(tr("OK"), this, "okButton");
        buttonLayout->addWidget(okButton);
        connect(okButton, SIGNAL(clicked()), SLOT(slotApply()));
        okButton->setDefault(true);

        QPushButton* cancelButton = new QPushButton(tr("Cancel"), this,
                "cancelButton");
        buttonLayout->addWidget(cancelButton);
        connect(cancelButton, SIGNAL(clicked()), SLOT(reject()));

        leName->setFocus();
    }
}

int LocoDialog::exec()
{
    if (decoders && protocols)
        return QDialog::exec();
    else
        return false;
}


void LocoDialog::setupTabGeneral()
{
    QLabel *label;

    QWidget *w = new QWidget(this, "page one");
    QHBoxLayout* baseLayout = new QHBoxLayout(w, 10, 10);
    QVBoxLayout* leftLayout = new QVBoxLayout(baseLayout, 10);

    // left side 
    // names group box
    QGroupBox *namesGB = new QGroupBox(tr("Identification"), w, "namesGB");
    leftLayout->addWidget(namesGB);

    leName = new QLineEdit(namesGB, "nameLE");
    leName->setGeometry(100, 18, 58, 20);
    leName->setMaxLength(8);

    leAlias = new QLineEdit(namesGB, "aliasLE");
    leAlias->setGeometry(100, 48, 104, 20);
    leAlias->setMaxLength(16);

    laPicture = new QLabel(namesGB, "pictureLbl");
    laPicture->setGeometry(100, 78, 152, 20);
    laPicture->setFrameStyle(QFrame::Box | QFrame::Sunken);

    QPushButton* buttGetIcon = new QPushButton(namesGB, "getIcon");
    buttGetIcon->setPixmap(QPixmap(fileopen_xpm));
    buttGetIcon->setGeometry(256, 74, 26, 26);
    connect(buttGetIcon, SIGNAL(clicked()), this, SLOT(slotGetIcon()));

    label = new QLabel(tr("&Name (Loco):"), namesGB);
    label->setGeometry(10, 20, 90, 15);
    label->setBuddy(leName);

    label = new QLabel(tr("&Alias (Train):"), namesGB);
    label->setGeometry(10, 50, 90, 15);
    label->setBuddy(leAlias);

    label = new QLabel(tr("&Icon:"), namesGB);
    label->setGeometry(10, 80, 90, 15);
    label->setBuddy(laPicture);

    /*
       QButtonGroup *directionGB = new QButtonGroup( "Direction", w );
       directionGB->setGeometry( 10, 132, 290, 90 );

       rbDirectionFwd = new QRadioButton( "Forward", directionGB );
       rbDirectionFwd->move( 10, 20 );
       rbDirectionRev = new QRadioButton( "Reverse", directionGB );
       rbDirectionRev->move( 10, 50 );
       if( dlgType == PROPERTIES) {
           rbDirectionFwd->setEnabled(FALSE);
           rbDirectionRev->setEnabled(FALSE);
           directionGB->setEnabled(FALSE);
       }
     */
    
    // direction group box
    QGroupBox *directionGB = new QGroupBox(1, Qt::Vertical,
            tr("Direction"), w, "directionGB");
    leftLayout->addWidget(directionGB);
    cbReverseDirection = new QCheckBox(tr("Reverse Logic (show "
                "forward, move backward)"), directionGB);


    // right side 
    // logic group box
    QGroupBox *logicGB = new QGroupBox(tr("Logic"), w, "Logic");
    baseLayout->addWidget(logicGB);

    coboDecoder = new QComboBox(false, logicGB);
    Decoder *decoder;
    for (decoder = decoders->first(); decoder != NULL;
         decoder = decoders->next())
        coboDecoder->insertItem(decoder->name);
    coboDecoder->setGeometry(100, 18, 180, 20);
    connect(coboDecoder, SIGNAL(highlighted(int)),
            this, SLOT(slotSetProtocols(int)));

    coboProtocol = new QComboBox(false, logicGB);
    coboProtocol->setGeometry(100, 48, 180, 20);
    connect(coboProtocol, SIGNAL(highlighted(const QString&)),
            this, SLOT(slotSetProtocolDefaults(const QString&)));

    spAddress = new QSpinBox(logicGB, "addressSB");
    spAddress->setGeometry(100, 78, 70, 20);
    spAddress->setWrapping(TRUE);

    // this line edit is only for information, bus value can be changed
    // for the whole set of locomotives in list property dialog
    busLE = new QLineEdit(logicGB, "busLE");
    busLE->setGeometry(100, 110, 70, 20);
    busLE->setReadOnly(true);

    label = new QLabel(tr("D&ecoder:"), logicGB);
    label->setGeometry(10, 20, 90, 15);
    label->setBuddy(coboDecoder);

    label = new QLabel(tr("&Protocol:"), logicGB);
    label->setGeometry(10, 50, 90, 15);
    label->setBuddy(coboProtocol);

    label = new QLabel(tr("A&ddress:"), logicGB);
    label->setGeometry(10, 80, 90, 15);
    label->setBuddy(spAddress);

    label = new QLabel(tr("Bus:"), logicGB);
    label->setGeometry(10, 110, 90, 15);
    //label->setBuddy(busLE);

    /*empty label for protocol property description*/
    labelDescription = new QLabel("", logicGB);
    labelDescription->setGeometry(10, 140, 90, 15);

    // finally insert page one into the tab dialog
    tabs->addTab(w, tr("&General"));
}


void LocoDialog::setupTabFuncs()
{
    QLabel *label;

    QWidget *w = new QWidget(this, "page two");

    label = new QLabel(tr("Available:"), w);
    label->setGeometry(10, 42, 80, 15);

    label = new QLabel(tr("Type:"), w);
    label->setGeometry(10, 72, 80, 15);

    label = new QLabel(tr("Activate as:"), w);
    label->setGeometry(10, 102, 80, 15);

    QString labelText;
    for (int i = 0; i <= 4; i++) {
        labelText.sprintf("F %d", i);
        QGroupBox *groupF = new QGroupBox(labelText, w, labelText);
        groupF->setGeometry(95 + i * 100, 10, 110, 130);
        groupF->setAlignment(AlignHCenter);

        cbFuncAvail[i] = new QCheckBox(w);
        cbFuncAvail[i]->setGeometry(140 + i * 100, 40, 20, 20);

        coboFuncIcon[i] = new QComboBox(false, w);      // false == read-only
        coboFuncIcon[i]->insertItem("light");
        //coboFuncIcon[i]->insertItem("bright light");
        coboFuncIcon[i]->insertItem("smoke");
        coboFuncIcon[i]->insertItem("coupling");
        coboFuncIcon[i]->insertItem("sound");
        coboFuncIcon[i]->insertItem("shunting");
        //coboFuncIcon[i]->insertItem("breaking");
        //coboFuncIcon[i]->insertItem("whistle");
        //coboFuncIcon[i]->insertItem("turbines");
        coboFuncIcon[i]->setGeometry(100 + i * 100, 70, 90, 20);

        coboFuncType[i] = new QComboBox(false, w);
        coboFuncType[i]->insertItem("switch");
        coboFuncType[i]->insertItem("pushbutton");
        coboFuncType[i]->setGeometry(100 + i * 100, 100, 90, 20);
    }

    // finally insert page one into the tab dialog
    tabs->addTab(w, tr("&Functions"));
}


    // set up page three of the tab dialog
void LocoDialog::setupTabSpeeds()
{
    QLCDNumber *lcdNum;
    QLabel *label;

    QWidget *w = new QWidget(this, "page three");
    QVBoxLayout* baseLayout = new QVBoxLayout(w, 10, 10);

    // static group box
    QGroupBox *staticGB = new QGroupBox(tr("Static"), w, "Statics");
    baseLayout->addWidget(staticGB);

    // setup sliders
    sliderMaxSpeed =
        new QSlider(QSlider::Horizontal, staticGB, "sliderMaxSpeed");
    sliderMaxSpeed->setTickmarks(QSlider::Below);
    sliderMaxSpeed->setGeometry(100, 20, 300, 20);

    sliderAvgSpeed =
        new QSlider(QSlider::Horizontal, staticGB, "sliderAvgSpeed");
    sliderAvgSpeed->setTickmarks(QSlider::Below);
    sliderAvgSpeed->setGeometry(100, 50, 300, 20);

    sliderMinSpeed =
        new QSlider(QSlider::Horizontal, staticGB, "sliderMinSpeed");
    sliderMinSpeed->setTickmarks(QSlider::Below);
    sliderMinSpeed->setGeometry(100, 80, 300, 20);

    // setup labels
    label = new QLabel(tr("&Maximum:"), staticGB);
    label->setBuddy(sliderMaxSpeed);
    label->setGeometry(10, 20, 90, 15);

    label = new QLabel(tr("&Average:"), staticGB);
    label->setBuddy(sliderMaxSpeed);
    label->setGeometry(10, 50, 90, 15);

    label = new QLabel(tr("M&inimum:"), staticGB);
    label->setBuddy(sliderMaxSpeed);
    label->setGeometry(10, 80, 90, 15);

    // setup digital displays
    lcdNum = new QLCDNumber(3, staticGB, "");
    lcdNum->setGeometry(420, 10, 50, 30);
    lcdNum->setFrameStyle(QFrame::NoFrame);
    lcdNum->setSegmentStyle(QLCDNumber::Filled);
    connect(sliderMaxSpeed, SIGNAL(valueChanged(int)), lcdNum,
            SLOT(display(int)));

    lcdNum = new QLCDNumber(3, staticGB, "");
    lcdNum->setGeometry(420, 40, 50, 30);
    lcdNum->setFrameStyle(QFrame::NoFrame);
    lcdNum->setSegmentStyle(QLCDNumber::Filled);
    connect(sliderAvgSpeed, SIGNAL(valueChanged(int)), lcdNum,
            SLOT(display(int)));

    lcdNum = new QLCDNumber(3, staticGB, "");
    lcdNum->setGeometry(420, 70, 50, 30);
    lcdNum->setFrameStyle(QFrame::NoFrame);
    lcdNum->setSegmentStyle(QLCDNumber::Filled);
    connect(sliderMinSpeed, SIGNAL(valueChanged(int)), lcdNum,
            SLOT(display(int)));

    // dynamic group box
    QGroupBox *dynamicGB = new QGroupBox(tr("Dynamic"), w, "Dynamics");
    baseLayout->addWidget(dynamicGB);

    sliderAccelTime = new QSlider(QSlider::Horizontal, dynamicGB,
                                  "sliderAccelTime");
    sliderAccelTime->setRange(0, 1500);
    sliderAccelTime->setTickmarks(QSlider::Below);
    sliderAccelTime->setTickInterval(100);
    sliderAccelTime->setSteps(50, 50);
    sliderAccelTime->setGeometry(100, 20, 300, 20);

    sliderBreakTime =
        new QSlider(QSlider::Horizontal, dynamicGB, "sliderBreakTime");
    sliderBreakTime->setRange(0, 1500); // max 1500 ms = 1.5 secs
    sliderBreakTime->setTickmarks(QSlider::Below);
    sliderBreakTime->setTickInterval(100);
    sliderBreakTime->setSteps(50, 50);
    sliderBreakTime->setGeometry(100, 50, 300, 20);

    label = new QLabel(tr("&Acceleration:"), dynamicGB);
    label->setBuddy(sliderAccelTime);
    label->setGeometry(10, 20, 90, 15);

    label = new QLabel(tr("&Breaking:"), dynamicGB);
    label->setBuddy(sliderBreakTime);
    label->setGeometry(10, 50, 90, 15);


    lcdNum = new QLCDNumber(4, dynamicGB, "");
    lcdNum->setGeometry(420, 10, 50, 30);
    lcdNum->setFrameStyle(QFrame::NoFrame);
    lcdNum->setSegmentStyle(QLCDNumber::Filled);
    connect(sliderAccelTime, SIGNAL(valueChanged(int)), lcdNum,
            SLOT(display(int)));

    lcdNum = new QLCDNumber(4, dynamicGB, "");
    lcdNum->setGeometry(420, 40, 50, 30);
    lcdNum->setFrameStyle(QFrame::NoFrame);
    lcdNum->setSegmentStyle(QLCDNumber::Filled);
    connect(sliderBreakTime, SIGNAL(valueChanged(int)), lcdNum,
            SLOT(display(int)));

    QLabel *m = new QLabel(" = ", staticGB);
    m->setGeometry(470, 10, 30, 30);

    leRealMaxSpeed = new QLineEdit(staticGB, "RealMax");
    leRealMaxSpeed->setGeometry(500, 12, 30, 30);
    leRealMaxSpeed->setMaxLength(3);

    QButtonGroup *unitsBG = new QButtonGroup("Units", staticGB);
    unitsBG->setGeometry(530, 9, 60, 80);
    unitsBG->setTitle("");
    unitsBG->setFrameStyle(QFrame::NoFrame);

    rbUnitkm = new QRadioButton("k&m/h", unitsBG);
    rbUnitkm->move(10, -6);
    rbUnitml = new QRadioButton("m&ph", unitsBG);
    rbUnitml->move(10, 14);

    // finally insert page one into the tab dialog
    tabs->addTab(w, tr("&Speeds"));
}


void LocoDialog::slotUpdateIconField(const QString& newText)
{
    laPicture->setText(newText + ".xpm");
}


void LocoDialog::slotApply()
{
    if ((QString(spAddress->text()).toInt() > maxAddress)
        || (QString(spAddress->text()).toInt() < 1)) {
        QMessageBox::information(this, tr("Error"),
                tr("Address out of Range.\n"
                    "Please enter a correct value."));
        return;
    }

    spAddress->clearFocus();
    spAddress->setFocus();
    if (dlgType == NEW_LOCO) 
    {
    	// Nothing to do so far
    }
    else if (dlgType == PROPERTIES) 
    {
        // Write back loco data to LocoControl
        loco->setLocoName(leName->text());
        loco->setLocoAlias(leAlias->text());
        loco->setLocoPicture(laPicture->text());
        loco->setLocoDecoder(coboDecoder->currentText());
        loco->setLocoProtocol(coboProtocol->currentText());
        loco->setAddress(spAddress->text().toUInt());
        loco->setBus(busLE->text().toUInt());

        for (int i = 0; i <= 4; i++) {
            loco->setLocoFuncAvail(i, cbFuncAvail[i]->isChecked());
            loco->setLocoFuncTypeSwitch(i, !coboFuncType[i]->currentItem());
            QString temp(coboFuncIcon[i]->currentText());
            temp.truncate(5);
            temp.append(".xpm");
            loco->setLocoFuncIcon(i, temp);
        }
        loco->setLocoMaxSpeed(sliderMaxSpeed->value());
        loco->setLocoAvgSpeed(sliderAvgSpeed->value());
        loco->setLocoMinSpeed(sliderMinSpeed->value());
        loco->setLocoAccelTime(sliderAccelTime->value());
        loco->setLocoBreakTime(sliderBreakTime->value());
        QString *temp = new QString(leRealMaxSpeed->text());
        loco->setLocoRealMaxSpeed(temp->toInt());
        loco->setReverseDirection(cbReverseDirection->isChecked());

        if (rbUnitkm->isChecked())
            loco->setLocoSpeedUnit("km/h");
        else
            loco->setLocoSpeedUnit("mph");
    }
    accept();
}


void LocoDialog::slotDefault()
{
    // We always get the default from the loco
    // So there is only one point to maintain good default values !
    slotGetData();
    /*leName->clear();
    leAlias->clear();
    laPicture->setText("");
//  rbDirectionFwd->setChecked( 1 );
    cbReverseDirection->setChecked(false);
    coboDecoder->setCurrentItem(0);
    slotSetProtocols(0);
    slotSetProtocolDefaults(coboProtocol->currentText());
    spAddress->setValue(1);
    busLE->setText("1");
    cbFuncAvail[0]->setChecked(1);
    sliderAccelTime->setValue(1000);
    sliderBreakTime->setValue(700);
    leRealMaxSpeed->setText("100");
    rbUnitkm->setChecked(1);*/
}


void LocoDialog::slotGetIcon()
{
    QString filename =
        QFileDialog::getOpenFileName(LOCO_PIX_DIR, "*.xpm", this);
    if (!filename.isEmpty()) {
        QFileInfo file(filename);
        if (!file.exists()) {
            QMessageBox::warning(this, tr("Error"),
                tr("Sorry, this file doesn't exist.\n%1").
                arg(filename));
            return;
        }
        if ((file.dirPath() + '/') != QString(LOCO_PIX_DIR)) {
            QMessageBox::warning(this, tr("Error"),
                tr("Sorry, only files in directory %1 are valid.").
                arg(LOCO_PIX_DIR));
            return;
        }
        filename = file.fileName();     // remove path
        laPicture->setText(filename);
    }
    else
        return;
}


// hier muss noch der Adressrange uebergeben werden
void LocoDialog::slotSetProtocolDefaults(const QString& forProtocol)
{
    Protocol *protocol = protocols->find(forProtocol);
    if (protocol) {
        setNewValues(protocol->addressRange, protocol->steps,
                     protocol->functions);
        printDescription(protocol);
    }
    else {
        QMessageBox::critical(this, tr("Error"),
            tr("Sorry, this protocol is not defined.\n"
               "Please check the protocols and decoders files."));
        reject();
    }
}


void LocoDialog::setNewValues(int maxAddr, int max, int functions)
{
    maxAddress = maxAddr;
    spAddress->setRange(1, maxAddr);
    sliderMaxSpeed->setRange(0, max);
    sliderAvgSpeed->setRange(0, max);
    sliderMinSpeed->setRange(0, max);

    sliderMaxSpeed->setValue(sliderMaxSpeed->maxValue());
    sliderAvgSpeed->setValue(sliderMaxSpeed->maxValue() / 2);
    sliderMinSpeed->setValue(sliderMaxSpeed->maxValue() / 3);

    sliderMaxSpeed->setTickInterval(max / 10);
    sliderAvgSpeed->setTickInterval(max / 10);
    sliderMinSpeed->setTickInterval(max / 10);

    bool state;
    for (int i = 0; i <= 4; i++) {
        if (i < functions)
            state = true;
        else {
            state = false;
            // turn off before disable to avoid on-state at next enable
            cbFuncAvail[i]->setChecked(false);
        }
        cbFuncAvail[i]->setEnabled(state);
        coboFuncIcon[i]->setEnabled(state);
        coboFuncType[i]->setEnabled(state);
    }
}

void LocoDialog::slotGetData()
{

    leName->setText(loco->getLocoName());
    leAlias->setText(loco->getLocoAlias());
    laPicture->setText(loco->getLocoPicture());
    cbReverseDirection->setChecked(loco->getReverseDirection());

    bool found = false;
    for (int i = 0; i < coboDecoder->count(); i++) {
        if (coboDecoder->text(i) == loco->getLocoDecoder()) 
        {
            coboDecoder->setCurrentItem(i);
            slotSetProtocols(i);
            found = true;
            break;
        }
    }

    if (!found)
        QMessageBox::warning(this, tr("Error"),
                             tr("Decoder not recognized.\n"
                                "Check to choose the right one\n"
                                "and then save your configuration."));

    // recognize decoder to insert protocol
    if (coboProtocol->count() > 0)
    {
        for (int i = 0; i < coboProtocol->count(); i++) 
        {
            if (coboProtocol->text(i) == loco->getLocoProtocol())
            {
                coboProtocol->setCurrentItem(i);
                break;
            }
        }
    }
    else 
    {
        slotSetProtocols(coboDecoder->currentItem());
    }
    spAddress->setValue(loco->getAddress());
    busLE->setText(QString::number(loco->getBus()));
    sliderAccelTime->setValue(loco->getLocoAccelTime());
    sliderBreakTime->setValue(loco->getLocoBreakTime());
    sliderMaxSpeed->setValue(loco->getLocoMaxSpeed());
    sliderAvgSpeed->setValue(loco->getLocoAvgSpeed());
    sliderMinSpeed->setValue(loco->getLocoMinSpeed());
    QString temp;
    leRealMaxSpeed->setText(temp.setNum(loco->getLocoRealMaxSpeed()));
    
    if ((loco->getLocoSpeedUnit()).compare("km/h") == 0)
        rbUnitkm->setChecked(true);
    else
        rbUnitml->setChecked(true);
    
    for (int i = 0; i <= 4; i++) 
    {
        cbFuncAvail[i]->setChecked(loco->getLocoFuncAvail(i));
        coboFuncType[i]->setCurrentItem(!loco->getLocoFuncType(i));
        for (int j = 0; j < coboFuncIcon[i]->count(); j++)
        {
            if (!strncmp(coboFuncIcon[i]->text(j), loco->getLocoFuncIcon(i), 5)) 
            {
                coboFuncIcon[i]->setCurrentItem(j);
            }
        }
    }
}


bool LocoDialog::loadDecoderFile()
{
    QFile file(DECODER_FILE);
    if (file.open(IO_ReadOnly)) {
        QTextStream ts(&file);
        ts.setEncoding(QTextStream::UnicodeUTF8);
        QString s, name, protocols;
        decoders = new QList < Decoder > ();

        while (!ts.eof()) {
            s = ts.readLine();
            // ignore lines beginning with '#'
            if (s.stripWhiteSpace().left(1) == "#");
            else if (s.contains("DECODER_NAME=")) {
                name = getValue(s);
            }
            else if (s.contains("DECODER_PROTOCOLS=")) {
                protocols = getValue(s);
                decoders->append(new Decoder(name, protocols));
            }
        }
        return true;
    }
    else {
        QMessageBox::critical(this,
                              tr("Error"),
                              tr("File '%1' not found.").arg(DECODER_FILE));
        decoders = NULL;
        return false;
    }
}

bool LocoDialog::loadProtocolFile()
{
    QFile file(PROTOCOL_FILE);
    if (file.open(IO_ReadOnly)) {
        QTextStream ts(&file);
        ts.setEncoding(QTextStream::UnicodeUTF8);
        QString s, name;
        int steps = 0, functions = 0, addressRange = 0;
        protocols = new QDict < Protocol > ();
        while (!ts.eof()) {
            s = ts.readLine();
            // ignore lines beginning with '#'
            if (s.stripWhiteSpace().left(1) == "#")
                ;
            else if (s.contains("PROTOCOL_NAME=")) {
                name = getValue(s);
            }
            else if (s.contains("PROTOCOL_STEPS=")) {
                steps = getValue(s).toInt();
            }
            else if (s.contains("PROTOCOL_FUNCTIONS=")) {
                functions = getValue(s).toInt();
            }
            else if (s.contains("PROTOCOL_ADDRESSRANGE=")) {
                addressRange = getValue(s).toInt();
                protocols->insert(name,
                                  new Protocol(name, steps, functions,
                                               addressRange));
            }
        }
        return true;
    }
    else {
        QMessageBox::critical(this,
                tr("Error"),
                tr("File '%1' not found.").arg(PROTOCOL_FILE));
        protocols = NULL;
        return false;
    }
}

QString LocoDialog::getValue(QString str)
{
    QString value;

    if (str.find('=') < 0)
        return QString("");

    // move all before, and '='
    str.remove(0, str.find('=') + 1);

    // strip white spaces
    str = str.stripWhiteSpace();

    // strip beginning and ending '"'
    if (str.find('"') == 0)
        str.remove(0, 1);
    if (str.findRev('"') == (int) str.length() - 1)
        str.remove(str.length() - 1, 1);

    // strip white spaces
    value = str.stripWhiteSpace();

    return value;
}


void LocoDialog::slotSetProtocols(int index)
{
    QString protocols = decoders->at(index)->protocols;
    QString prot;
    QTextStream ts(protocols, IO_ReadWrite);
    coboProtocol->clear();
    for (ts >> prot; !prot.isEmpty(); ts >> prot) {
        coboProtocol->insertItem(prot);
    }
    slotSetProtocolDefaults(coboProtocol->currentText());
}


void LocoDialog::printDescription(Protocol * protocol)
{

    descriptionText.sprintf(tr("This protocol supports %d speed steps,\n"
                               "%d addresses and up to %d functions."),
                            protocol->steps, protocol->addressRange,
                            protocol->functions);
    labelDescription->setText(descriptionText);
    labelDescription->resize(labelDescription->sizeHint());
}
