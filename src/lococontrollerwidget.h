//
// C++ Interface: LocoControllerWidget
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef LOCO_CONTROLLER_LIST_H
#define LOCO_CONTROLLER_LIST_H

#include <qframe.h>
#include <qptrlist.h>
#include <qwidget.h>

#include "Loco.h"
#include "LocoControl.h"

class LocoControllerWidget : public QWidget
{
    public:
        LocoControllerWidget( QWidget *parent=0, const char *name=0 );
        ~LocoControllerWidget();
        
        void setParent(QWidget *);
        void add(Loco *);       // Add a controller for loco, if not already present
        void remove(LocoControl *);    // remove controller for Loco
        void clear(void);
        bool isVisible(Loco *); // True if a controller exist for this loco
        LocoControl *getFirst();
        LocoControl *getLast();
        LocoControl *getNext();
        LocoControl *getPrev();
        
    private:
        QPtrList<LocoControl> list;
        QWidget *parentWidget;
        QFrame *controlerArea;
        QBoxLayout *layoutMain;
};

#endif /* LOCO_CONTROLLER_LIST_H */
