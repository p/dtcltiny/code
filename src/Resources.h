/***************************************************************************
                               Resources.h
                             -----------------
    begin                : 11.11.2000
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RESOURCES_H
#define RESOURCES_H

#include <config.h>


#define MAXMSGSIZE 4096

// Constants for LocoDialog.cpp
#define NEW_LOCO 0
#define PROPERTIES 1

// where to find the icons, decoders and protocols
// #define RESOURCE_DIR    PREFIX "/share/" PACKAGE "/" 
#define PIX_DIR         RESOURCE_DIR "/"
#define LOCO_PIX_DIR    RESOURCE_DIR "/locopixmaps/"
#define DECODER_FILE 	RESOURCE_DIR "/decoders"
#define PROTOCOL_FILE   RESOURCE_DIR "/protocols"

// Constants for LocoControl
#define C_WIDTH_ 100
#define C_HEIGHT_ 400
#define C_MID_ C_WIDTH_ / 2
#define C_Y_START_ 8
#define MAX_CONTROLLERS 100
#define my_font_ "Helvetica"

/* user preferences file */
#define DTCLTINYRC ".dtcltinyrc"
#define DTCLFILEEXT ".dtcl"

/* Sourceforge related stuff */
#define DTCLTINY_HOMEPAGE "http://sourceforge.net/projects/dtcltiny"

#endif    //RESOURCES_H
