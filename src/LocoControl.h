/***************************************************************************
                               LocoControl.h
                             -----------------
    begin                : 11.11.2000
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LOCOCONTROL_H
#define LOCOCONTROL_H

#include <qapplication.h>
#include <qbuttongroup.h>
#include <qcursor.h>
#include <qfont.h>
#include <qframe.h>
#include <qlabel.h>
#include <qpixmap.h>
#include <qpopupmenu.h>
#include <qpushbutton.h>
#include <qslider.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qtextstream.h>
#include <qtimer.h>
#include <qtooltip.h>
#include <qvbox.h>
#include <qwidget.h>
#include <unistd.h>

#include "Resources.h"
#include "LocoDialog.h"
#include "Loco.h"


class LocoControl: public QWidget 
{
    Q_OBJECT
       
  friend class LocoDialog;

  public:
    //LocoControl(Loco *lc, QTextStream&, QWidget* parent = 0, unsigned int index = 0);
    LocoControl(QWidget *parent = 0, const char *name = "", Loco *lc = 0);
    ~LocoControl();
    
    void setSrcpBus(unsigned int);
    bool isMyLoco(Loco *);

  private:
    void handleFuncKeyPress(int);
    void handleFuncKeyRelease(int);
    void initLayout();
    void updateDirectionFwd();
    void updateDirectionEnable();
    void updateDirectionPix();
    void updateDisplay();
    void updateNameDisplay();
    void updateSliderValue(int);
    void updateSpeedButtonText();
    void updateSpeedSliderSettings();
    
    unsigned int getSliderValue();
    // private data accessible by LocoDialog
		class Loco *loco;
		
    QFrame *frameController;
    QFrame *frameSpeed;
    QVBox *frameLoco;
    QPushButton *buttF[5];
    QPushButton *buttBreak;
    QPushButton *buttDirection;
    QPushButton *buttEmergency;
    QPushButton *buttName;
    QPushButton *buttSpeed;
    QSlider *sliderSpeed;
    QButtonGroup *groupFuncs;
    QLabel *labelAddress;
    QLabel *labelPicture;
    QPixmap pixPicture;
    QPixmap pixFuncs;
    QPixmap pixEmergency;
    QPixmap pixDirection;
    QPopupMenu *contextmenu;
    QButtonGroup *m_group;

  protected:
    virtual void mousePressEvent(QMouseEvent *);
    virtual void keyPressEvent(QKeyEvent *);
    virtual void keyReleaseEvent(QKeyEvent *);

  private slots:
    void slotControllerVisibleChanged(Loco *, bool);
    void slotDelete();
    void slotHide();
    void slotM_GroupClicked(int);
    void slotUpdateFuncsTouched(int);
    void slotUpdateFuncsSwitched(int);
    void slotUpdateSpeed(int);
    void slotReverseDirection();
    void slotToggleNameAlias();
    void slotToggleSpeedDisplay();
    void slotUpdate(void);
    void slotUpdateProperties(Loco *);

  public slots:
		void slotEmergencyHalt();
    void slotProperties();

  signals:
    void closed(unsigned int);
    void modified();
    //TODO: void sendSrcpMessage(const SrcpMessage*);
};

#endif                          //LOCOCONTROL_H
