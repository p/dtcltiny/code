/***************************************************************************
                               MainWindow.cpp
                             -------------------
    begin                : 11.11.2000
    last update		 : 2004-10-30
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : dtcltiny@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// for service port detection
#include <netdb.h>
#include <qlistview.h>
#include <qscrollview.h>

#include "aboutdialog.h"
#include "listpropertiesdialog.h"
#include "MainWindow.h"
#include "LocoDialog.h"
#include "Programmer.h"
#include "preferencesdlg.h"
#include "serverinfodialog.h"
#include "config.h"
#include "LocoList.h"
#include "locolistview.h"


//constants for data file
#define KS             "="
#define DS             ":"
#define DF_HOST        "host"
#define DF_BUS         "bus"
#define DF_AUTOCONNECT "autoconnect"
#define DF_AUTOPOWERON "autopoweron"
#define DF_AUTOSEND    "autosendinit"

//constants for user preferences file
#define PF_SHOWMSGTIME   "showmessagetime"
#define PF_TRANSLSRVTIME "translateservertime"
#define PF_AUTOLOAD      "autoload"
#define PF_AUTOLOADFILE  "autoloadfile"
#define PF_BROWSERNO     "browserno"
#define PF_LASTDIR       "lastdir"
#define PF_SHOWTOOLBAR   "showtoolbar"
#define PF_SHOWSTATUSBAR "showstatusbar"

// IDs for menu item handling
#define FILE_ID_NEW  	     101
#define FILE_ID_OPEN 	     102
#define FILE_ID_SAVE 	     103
#define FILE_ID_SAVEAS	     104
#define FILE_ID_QUIT 	     105
#define VIEW_ID_TOOLBAR      201
#define VIEW_ID_STATUSBAR    202
#define VIEW_ID_STATUSLINE   203
#define DAEMON_ID_CONNECT    301
#define DAEMON_ID_DISCONNECT 302
#define DAEMON_ID_RESET      303
#define DAEMON_ID_SHUTDOWN   304
#define DAEMON_ID_INFO       305
#define DAEMON_ID_POWER      306
#define DAEMON_ID_EMERGENCY  307
#define LOCOMOTIVE_ID_ADD      401
#define LOCOMOTIVE_ID_EDIT     402
#define LOCOMOTIVE_ID_DUPLICATE     403
#define LOCOMOTIVE_ID_REMOVE   404
#define LOCOMOTIVE_ID_PROP     405
#define PRGRM_ID_NMRA        501
#define PRGRM_ID_UHL         502

/* application icon */
#include "../icons/dtcltiny_32.xpm"

// file menu
#include "pixmaps/filenew.xpm"
#include "pixmaps/fileopen.xpm"
#include "pixmaps/filesave.xpm"
#include "pixmaps/newloco.xpm"

// daemon menu
#include "pixmaps/daemonconnect.xpm"
#include "pixmaps/daemondisconnect.xpm"
#include "pixmaps/daemonreset.xpm"
#include "pixmaps/daemonshutdown.xpm"
#include "pixmaps/daemoninfo.xpm"
#include "pixmaps/startpower.xpm"
#include "pixmaps/stoppower.xpm"
#include "pixmaps/emergency_all.xpm"

MainWindow::MainWindow(): QMainWindow(0, "dtcltinyMainWindow")
{
    setCaption(PACKAGE);
    setIcon(QPixmap(dtcltiny_32));

    // reset dynamic data
    serverConnected = false;
    powerOn = false;
    modified = false;

    // default settings for user preferences
    autoLoad = false;
    cAutoloadFile = "";
    iBrowser = 0;
    lastDir = QDir::homeDirPath();

    // receive connection state changes
    connect(locoList.getCommandPort(), SIGNAL(connectionStateChanged(bool)),
            this, SLOT(updateConnectionState(bool)));

    // receive info messages to detect power changes
    connect(locoList.getInfoPort(), SIGNAL(messageReceived(const QString&)),
            this, SLOT(processInfoMessage(const QString&)));
    
    connect(&locoList, SIGNAL(locoControllerStateChanged( Loco*, bool )),
             this, SLOT(slotLocoControllerStateChanged( Loco*, bool )));
    
    connect(&locoList, SIGNAL(listModified( bool )),
             this, SLOT(slotModified(bool)));

    setupMainWindow();
    slotModified(true);
    loadPreferences();
    sleep(1);
    slotModified(false);
}


MainWindow::~MainWindow()
{
	
}


// setup main window menubar, toolbar etc.
void MainWindow::setupMainWindow()
{
    // setup window layout
    QVBox *vBox = new QVBox(this, "vbox", 0);
    Q_CHECK_PTR(vBox);

    setCentralWidget(vBox);
    
    QHBox *hBox = new QHBox(vBox, "hbox", 0);
    locoListView = new LocoListView(hBox, "locoListView", &locoList);
    // setup the background widget for the controllers
    controlerArea = new LocoControllerWidget(hBox, "controlerArea");
    controlerArea->setSizePolicy ( QSizePolicy::Preferred, QSizePolicy::Preferred, false );
    
    //setup message history line
    messageHistory = new MessageHistory(vBox);
    Q_CHECK_PTR(messageHistory);

    connect(locoList.getCommandPort(), SIGNAL(messageReceived(const QString&)),
            messageHistory, SLOT(addCommand(const QString&)));
    connect(locoList.getCommandPort(), SIGNAL(messageSend(const QString&)),
            messageHistory, SLOT(addCommand(const QString&)));
    connect(locoList.getCommandPort(), SIGNAL(statusMessage(const QString&)),
            messageHistory, SLOT(addHint(const QString&)));

    connect(locoList.getInfoPort(), SIGNAL(messageReceived(const QString&)),
            messageHistory, SLOT(addInfo(const QString&)));
    connect(locoList.getInfoPort(), SIGNAL(messageSend(const QString&)),
            messageHistory, SLOT(addInfo(const QString&)));
    connect(locoList.getInfoPort(), SIGNAL(statusMessage(const QString&)),
            messageHistory, SLOT(addHint(const QString&)));

    connect(this, SIGNAL(statusMessage(const QString&)),
            messageHistory, SLOT(addHint(const QString&)));


    //create main menu 
    QMenuBar* menuBar = new QMenuBar(this);
    Q_CHECK_PTR(menuBar);

    fileMenu = new QPopupMenu;
    Q_CHECK_PTR(fileMenu);
    menuBar->insertItem(tr("&File"), fileMenu);

    fileMenu->insertItem(tr("&New"), this, SLOT(slotFileNew()),
            CTRL + Key_N, FILE_ID_NEW);
    fileMenu->insertItem(tr("&Open..."), this, SLOT(slotFileOpen()),
            CTRL + Key_O, FILE_ID_OPEN);
    fileMenu->insertItem(tr("&Save"), this, SLOT(slotFileSave()),
            CTRL + Key_S, FILE_ID_SAVE);
    fileMenu->insertItem(tr("Save &as..."), this, SLOT(slotFileSaveAs()),
            0, FILE_ID_SAVEAS);

    fileMenu->insertSeparator();
    
    fileMenu->insertItem(tr("&Quit"), this, SLOT(close()),
            CTRL + Key_Q, FILE_ID_QUIT);
    fileMenu->setItemEnabled(FILE_ID_SAVE, false);


    viewMenu = new QPopupMenu;
    Q_CHECK_PTR(viewMenu);
    menuBar->insertItem(tr("&View"), viewMenu);

    viewMenu->setCheckable(true);
    viewMenu->insertItem(tr("&Toolbar"), this,
            SLOT(slotToggleToolbar()), 0, VIEW_ID_TOOLBAR);
    viewMenu->setItemChecked(VIEW_ID_TOOLBAR, true);
    viewMenu->insertItem(tr("&Statusbar"), this,
            SLOT(slotToggleStatusbar()), 0, VIEW_ID_STATUSBAR);
    viewMenu->setItemChecked(VIEW_ID_STATUSBAR, true);
    viewMenu->insertItem(tr("&Toggle statusline"), messageHistory,
            SLOT(toggleLine()), CTRL + Key_D, VIEW_ID_STATUSLINE);


    daemonMenu = new QPopupMenu;
    Q_CHECK_PTR(daemonMenu);
    menuBar->insertItem(tr("&Daemon"), daemonMenu);

    daemonMenu->insertItem(tr("&Connect"), this,
            SLOT(slotDaemonConnect()), 0, DAEMON_ID_CONNECT);
    daemonMenu->insertItem(tr("&Disconnect"), this,
            SLOT(slotDaemonDisconnect()), 0, DAEMON_ID_DISCONNECT);

    daemonMenu->insertSeparator();

    daemonMenu->insertItem(tr("&Reset"), this, SLOT(slotDaemonReset()),
            0, DAEMON_ID_RESET);
    daemonMenu->insertItem(tr("Shu&tdown"), this,
            SLOT(slotDaemonShutdown()), 0, DAEMON_ID_SHUTDOWN);
    daemonMenu->insertItem(tr("&Information..."), this,
            SLOT(slotDaemonInfo()), 0, DAEMON_ID_INFO);

    daemonMenu->insertSeparator();

    daemonMenu->insertItem(tr("&Power on/off"), this,
            SLOT(slotDaemonPower()), CTRL + Key_P, DAEMON_ID_POWER);
    daemonMenu->insertItem(tr("&Emergency stop"), this,
            SLOT(slotDaemonEmergencyStop()), 0, DAEMON_ID_EMERGENCY);


    locomotiveMenu = new QPopupMenu;
    Q_CHECK_PTR(locomotiveMenu);
    menuBar->insertItem(tr("&Locomotive"), locomotiveMenu);

    locomotiveMenu->insertItem(tr("&Add"), this,
            SLOT(slotLocomotiveAdd()), 0, LOCOMOTIVE_ID_ADD);
    locomotiveMenu->insertItem(tr("&Edit..."), this,
            SLOT(slotLocomotiveEdit()), 0, LOCOMOTIVE_ID_EDIT);
    locomotiveMenu->insertItem(tr("&Duplicate"), this,
            SLOT(slotLocomotiveDuplicate()), 0, LOCOMOTIVE_ID_DUPLICATE);
    locomotiveMenu->insertItem(tr("&Remove"), this,
            SLOT(slotLocomotiveRemove()), 0, LOCOMOTIVE_ID_REMOVE);
    // As long as the menues does not work, we disable them
    locomotiveMenu->setItemEnabled(LOCOMOTIVE_ID_EDIT, false);
    locomotiveMenu->setItemEnabled(LOCOMOTIVE_ID_DUPLICATE, false);
    locomotiveMenu->setItemEnabled(LOCOMOTIVE_ID_REMOVE, false);
    
    locomotiveMenu->insertSeparator();

    locomotiveMenu->insertItem(tr("List &properties..."), this,
            SLOT(slotDaemonProperties()), 0, LOCOMOTIVE_ID_PROP);


    programMenu = new QPopupMenu;
    Q_CHECK_PTR(programMenu);
    menuBar->insertItem(tr("&Programmer"), programMenu);

    programMenu->insertItem(tr("&NMRA-Decoder..."), this,
            SLOT(slotPrgrmNMRA()), 0, PRGRM_ID_NMRA);
    programMenu->insertItem(tr("&Uhlenbrock-Decoder..."), this,
            SLOT(slotPrgrmUhl()), 0, PRGRM_ID_UHL);


    optionsMenu = new QPopupMenu;
    Q_CHECK_PTR(optionsMenu);
    menuBar->insertItem(tr("&Options"), optionsMenu);

    optionsMenu->insertItem(tr("&Preferences..."), this,
            SLOT(slotPreferences()));


    helpMenu = new QPopupMenu;
    Q_CHECK_PTR(helpMenu);
    menuBar->insertItem(tr("&Help"), helpMenu);

    helpMenu->insertItem(tr("&Help"), this, SLOT(slotHelpHelp()));
    helpMenu->insertItem(tr("&About dtcltiny..."), this, SLOT(slotHelpAbout()));
    helpMenu->insertItem(tr("About &Qt..."), this, SLOT(slotHelpAboutQt()));



    // setup toolbars
    // toolbar for file operations
    fileTb = new QToolBar(this, "fileTb");
    Q_CHECK_PTR(fileTb);

    fileNewTBtn = new QToolButton(QPixmap(filenew),
                            tr("Create new file"), 0, this,
                            SLOT(slotFileNew()), fileTb);
    fileOpenTBtn = new QToolButton(QPixmap(fileopen_xpm),
                             tr("Open loco controller file"), 0, this,
                             SLOT(slotFileOpen()), fileTb);
    fileSaveTBtn = new QToolButton(QPixmap(filesave_xpm),
                             tr("Save loco controller file"), 0, this,
                             SLOT(slotFileSave()), fileTb);
    fileSaveTBtn->setEnabled(false);


    // toolbar for daemon operations
    daemonTb = new QToolBar(this, "daemonTb");
    Q_CHECK_PTR(daemonTb);

    daemonConnectTBtn = new QToolButton(QPixmap(daemonconnect_xpm),
                              tr("Connect to SRCP server"), 0, this,
                              SLOT(slotDaemonConnect()), daemonTb);

    daemonDisconnectTBtn = new QToolButton(QPixmap(daemondisconnect_xpm),
                              tr("Disconnect SRCP server"), 0, this,
                              SLOT(slotDaemonDisconnect()), daemonTb);
    daemonDisconnectTBtn->setEnabled(false);

    daemonTb->addSeparator();

    daemonResetTBtn = new QToolButton(QPixmap(daemonreset_xpm),
                              tr("Reset SRCP server"), 0, this,
                              SLOT(slotDaemonReset()), daemonTb);
    daemonResetTBtn->setEnabled(false);

    daemonShutdownTBtn = new QToolButton(QPixmap(daemonshutdown_xpm),
                              tr("Shutdown SRCP server"), 0, this,
                              SLOT(slotDaemonShutdown()), daemonTb);
    daemonShutdownTBtn->setEnabled(false);

    daemonInfoTBtn = new QToolButton(QPixmap(daemoninfo_xpm),
                              tr("Show SRCP server information"), 0, this,
                              SLOT(slotDaemonInfo()), daemonTb);
    daemonInfoTBtn->setEnabled(false);


    daemonTb->addSeparator();

    daemonPowerTBtn = new QToolButton(QPixmap(startpower),
                              tr("Set power on"), 0, this,
                              SLOT(slotDaemonPower()), daemonTb);
    daemonPowerTBtn->setEnabled(false);

    daemonPowerOffTBtn = new QToolButton(QPixmap(stoppower),
                             tr("Set power off"), 0, this,
                             SLOT(slotDaemonPower()), daemonTb);
    daemonPowerOffTBtn->setEnabled(false);

    daemonEmergencyTBtn = new QToolButton(QPixmap(emergency_all),
                                  tr("Emergency stop"), 0, this,
                                  SLOT(slotDaemonEmergencyStop()), daemonTb);
    daemonEmergencyTBtn->setEnabled(false);


    // toolbar for locomotive list operations
    locolistTb = new QToolBar(this, "locolistTb");
    Q_CHECK_PTR(locolistTb);

    locolistAddTBtn = new QToolButton(QPixmap(newloco),
                            tr("Add new loco controller"), 0, this,
                            SLOT(slotLocomotiveAdd()), locolistTb);
    
    updateDaemonMenuItems();
}

/*
 * create a new file
 */
void MainWindow::slotFileNew()
{
    if (modified) {
        int choice = querySaveChanges();
        switch (choice) {
            case 0:
                if (saveFile())
                    newFile();
                break;
            case 1:
                newFile();
                break;
            case 2:
            default:
                break;
        }
    }
    else {
        newFile();
    } 
}

void MainWindow::newFile()
{   
    slotDaemonDisconnect();

    // reset dynamic data
    serverConnected = false;
    powerOn = false;
    modified = false;

    // reset file data
    // read port number from service database file
    struct servent *serviceentry;
    unsigned int port = 4303;
    serviceentry = getservbyname("srcp","tcp");
    if (serviceentry == NULL)
        port = 4303;
    else 
        port = ntohs(serviceentry->s_port);

    locoList.getCommandPort()->setServer("localhost", port);
    locoList.getInfoPort()->setServer("localhost", port);

    // delete all current controllers
    locoList.clear( );

    slotUpdateCaption();
    //updateFileMenuItems();
    emit statusMessage(tr("New file created."));
}


void MainWindow::slotUpdateCaption()
{
    if (locoList.getFileName().isEmpty())
        setCaption(QString(PACKAGE) + " - [" + tr("noname") + "]");
    else
        setCaption(QString(PACKAGE) + " - [" + locoList.getFileName() + "]");
}


int MainWindow::querySaveChanges()
{
    QString queryStr;

    if (locoList.getFileName().isEmpty())
        queryStr = tr("Unnamed file was changed.\nSave changes?");
    else
        queryStr = tr("File '%1' was changed.\n"
                "Save changes?").arg(locoList.getFileName());

    return QMessageBox::warning(this, tr("Save changes"),
            queryStr, tr("&Yes"), tr("&No"), tr("Cancel"));
}


void MainWindow::slotFileOpen()
{
    if (modified) {
        int choice = querySaveChanges();
        switch (choice) {
            case 0:
                if (saveFile())
                    chooseFile();
                break;
            case 1:
                chooseFile();
                break;
            case 2:
            default:
                break;
        }

    }
    else {
        chooseFile();
    }
}


void MainWindow::chooseFile()
{
    QStringList fnl = QFileDialog::getOpenFileNames(
        QString(tr("Loco sets")) + " (*" + DTCLFILEEXT + ")", lastDir, this);

    if (fnl.isEmpty())
        return;
    else {
        openFile(fnl.first());
        fnl.pop_front();

        QStringList::Iterator it = fnl.begin();
        while (it != fnl.end()) {
                openFileWindow(*it);
                ++it;
        }
    }
}


void MainWindow::slotFileSave()
{
    saveFile();
}


bool MainWindow::saveFile()
{
    if (locoList.getFileName().isEmpty())
    {
        slotFileSaveAs();
        return true;
    }
		
		return locoList.save( locoList.getFileName() );
}


void MainWindow::slotFileSaveAs()
{
    QString fn = QFileDialog::getSaveFileName(lastDir,
            QString(tr("Loco sets")) + " (*" + DTCLFILEEXT + ")", this);

    if (!fn.isEmpty()) {

        lastDir = fn.left(fn.findRev('/'));

        /*check for file extension*/
        if (!fn.endsWith(DTCLFILEEXT))
            fn.append(DTCLFILEEXT);

        /*check for existing file*/
        if (QFile::exists(fn)){
            int choice = QMessageBox::warning(this, tr("Warning"),
                    tr("File '%1' exists!\n"
                        "Do you want to overwrite it?").arg(fn),
                    tr("&Yes"), tr("&No"), 0, 0, 1);
            if (choice == 1)
                return;
        }

        fileMenu->setItemEnabled(FILE_ID_SAVE, true);
        locoList.setFileName(fn);
        saveFile();
    }
}


void MainWindow::openFile(const QString& fn)
{
    lastDir = fn.left(fn.findRev('/'));
    slotDaemonDisconnect();
    qApp->processEvents();
    locoList.load( fn );
        
    //TODO: updateLocoNumberSensibleItems();
    if (locoList.getLocoCount() > 0) 
    {
        fileMenu->setItemEnabled(FILE_ID_SAVE, true);
        fileSaveTBtn->setEnabled(true);
        updatePowerMenuItems();
    }
    else 
    {
        fileMenu->setItemEnabled(FILE_ID_SAVE, false);
        fileSaveTBtn->setEnabled(false);
        updatePowerMenuItems();
    }
    slotUpdateCaption();
    setFileIDNewEnabled();
    
    if (locoList.getAutoConnect()) 
    {
        slotDaemonConnect();
        qApp->processEvents();
    }
    updateControllerBus();
}


// Enable or disable the New Loco Menu
// and Toolbar if there is space for more
void MainWindow::setFileIDNewEnabled()
{
    if (locoList.getLocoCount() >= MAX_CONTROLLERS) {
        locomotiveMenu->setItemEnabled(LOCOMOTIVE_ID_ADD, false);
        locolistAddTBtn->setEnabled(false);
    }
    else {
        locomotiveMenu->setItemEnabled(LOCOMOTIVE_ID_ADD, true);
        locolistAddTBtn->setEnabled(true);
    }
}

/*
 * show user preferences dialog
 */
void MainWindow::slotPreferences()
{
    PreferencesDialog *dlg = new PreferencesDialog(this);

    dlg->setHistoryData(messageHistory->isTimeEnabled(),
            locoList.getCommandPort()->isTimeTranslationEnabled());

    dlg->setFileData(autoLoad, cAutoloadFile, iBrowser);

    if (dlg->exec() == QDialog::Accepted) {
        messageHistory->enableTime(dlg->getShowMessageTime());
        locoList.getCommandPort()->enableTimeTranslation(dlg->getTimeTranslation());
        locoList.getInfoPort()->enableTimeTranslation(dlg->getTimeTranslation());
        autoLoad = dlg->getAutoload();
        cAutoloadFile = dlg->getAutoloadFile();
        iBrowser = dlg->getBrowserNr();

        savePreferences();
    }
    delete dlg;
}

/*
 * load user preferences
 */
void MainWindow::loadPreferences()
{
    QString s, key;

    QFile file(QDir::homeDirPath() + "/" + DTCLTINYRC);
    if (!file.open(IO_ReadOnly)) {
        emit statusMessage(tr("Error: Could not open configuration "
                    "file: ~/%1").arg(DTCLTINYRC));
        return;
    }
    QTextStream ts(&file);
    ts.setEncoding(QTextStream::UnicodeUTF8);

    while (!ts.eof()) 
    {
        s = ts.readLine();
        // ignore comment lines
        if (!s.startsWith("#")) 
        {
            key = s.section(KS, 0, 0);
            // values are read sequence independent
            if (key.compare(PF_SHOWMSGTIME) == 0)
                 messageHistory->enableTime(s.section(KS, 1, 1).toInt());
            else if (key.compare(PF_TRANSLSRVTIME) == 0) 
            {
                bool enable = s.section(KS, 1, 1).toInt();
                locoList.getCommandPort()->enableTimeTranslation(enable);
                locoList.getInfoPort()->enableTimeTranslation(enable);
            }
            else if (key.compare(PF_AUTOLOAD) == 0)
                autoLoad = (bool) s.section(KS, 1, 1).toInt();
            else if (key.compare(PF_AUTOLOADFILE) == 0)
                cAutoloadFile = s.section(KS, 1, 1);
            else if (key.compare(PF_BROWSERNO) == 0)
                iBrowser = s.section(KS, 1, 1).toInt();
            else if (key.compare(PF_LASTDIR) == 0)
                lastDir = s.section(KS, 1, 1);
            else if (key.compare(PF_SHOWTOOLBAR) == 0)
                showToolbar((bool) s.section(KS, 1, 1).toInt());
            else if (key.compare(PF_SHOWSTATUSBAR) == 0)
                showStatusbar((bool) s.section(KS, 1, 1).toInt());
        }
    }
    file.close();
}

/*
 * save user preferences
 */
void MainWindow::savePreferences()
{
    QFile file(QDir::homeDirPath() + "/" + DTCLTINYRC);

    if (!file.open(IO_WriteOnly)) {
        emit statusMessage(tr("Error: Could not save configuration "
                    "file: ~/%1").arg(QString(DTCLTINYRC)));
        return;
    }

    QDateTime dt = QDateTime::currentDateTime();
    QTextStream ts(&file);
    ts.setEncoding(QTextStream::UnicodeUTF8);

    ts << "# " PACKAGE "configuration file" << endl;
    ts << "# last modified: " << dt.toString(Qt::ISODate) << endl;
    ts << "#" << endl;
    ts << PF_SHOWMSGTIME << KS << messageHistory->isTimeEnabled() << endl;
    ts << PF_TRANSLSRVTIME << KS
        << locoList.getCommandPort()->isTimeTranslationEnabled() << endl;
    ts << PF_AUTOLOAD << KS << autoLoad << endl;
    ts << PF_AUTOLOADFILE << KS << cAutoloadFile << endl;
    ts << PF_BROWSERNO << KS << iBrowser << endl;
    ts << PF_LASTDIR << KS << lastDir << endl;
    ts << PF_SHOWTOOLBAR << KS
        << viewMenu->isItemChecked(VIEW_ID_TOOLBAR) << endl;
    ts << PF_SHOWSTATUSBAR << KS
        << viewMenu->isItemChecked(VIEW_ID_STATUSBAR) << endl;

    file.close();
}

/*
 * response to window close event
 */
void MainWindow::closeEvent(QCloseEvent* e)
{
    if (!modified) {
        e->accept();
        return;
    }

    int choice = querySaveChanges();
    switch (choice) {
        case 0:
            if (saveFile())
                e->accept();
            else
                e->ignore();
            break;
        case 1:
            e->accept();
            break;
        case 2:
        default:
            e->ignore();
            break;
    }
}


void MainWindow::showToolbar(bool show)
{
    if (show) {
        viewMenu->setItemChecked(VIEW_ID_TOOLBAR, true);
        fileTb->show();
        daemonTb->show();
        locolistTb->show();
    }
    else {
        viewMenu->setItemChecked(VIEW_ID_TOOLBAR, false);
        fileTb->hide();
        daemonTb->hide();
        locolistTb->hide();
    }
}

/*
 * view/hide toolbar
 */
void MainWindow::slotToggleToolbar()
{
    if (viewMenu->isItemChecked(VIEW_ID_TOOLBAR))
        showToolbar(false);
    else
        showToolbar(true);
}


void MainWindow::showStatusbar(bool show)
{
    if (show) {
        viewMenu->setItemChecked(VIEW_ID_STATUSBAR, true);
        messageHistory->show();
    }
    else {
        viewMenu->setItemChecked(VIEW_ID_STATUSBAR, false);
        messageHistory->hide();
    }
}

/*
 * view/hide statusbar
 */
void MainWindow::slotToggleStatusbar()
{
    if (viewMenu->isItemChecked(VIEW_ID_STATUSBAR))
        showStatusbar(false);
    else
        showStatusbar(true);
}

/*
 * connect to SRCP server
 */
void MainWindow::slotDaemonConnect()
{
    if (!locoList.getCommandPort()->hasServerConnection())
        locoList.getCommandPort()->serverConnect();
    if (!locoList.getInfoPort()->hasServerConnection())
        locoList.getInfoPort()->serverConnect();
}

/*
 * disconnect from SRCP server
 */
void MainWindow::slotDaemonDisconnect()
{
    locoList.getCommandPort()->serverDisconnect();
    locoList.getInfoPort()->serverDisconnect();
}

/*
 * reset SRCP server
 */
void MainWindow::slotDaemonReset()
{
    sendSrcpMessageString("RESET 0 SERVER");
}

/*
 * shutdown SRCP server
 */
void MainWindow::slotDaemonShutdown()
{
    sendSrcpMessageString("TERM 0 SERVER");
}

/*
 * show dialog with server information
 */
void MainWindow::slotDaemonInfo()
{
    ServerInfoDialog* sid = new ServerInfoDialog(this);
    if (sid == NULL)
        return;

    sid->setCommandSessionData(
            locoList.getCommandPort()->getSrcpServer(),
            locoList.getCommandPort()->getSrcpVersion(),
            locoList.getCommandPort()->getSrcpOther(),
            locoList.getCommandPort()->getSessionId());

    sid->setInfoSessionData(
            locoList.getInfoPort()->getSrcpServer(),
            locoList.getInfoPort()->getSrcpVersion(),
            locoList.getInfoPort()->getSrcpOther(),
            locoList.getInfoPort()->getSessionId());

    sid->exec();
    delete sid;
}

/*
 * toggle power of current SRCP bus
 */
void MainWindow::slotDaemonPower()
{
    if (powerOn)
        sendSrcpMessageString(QString("SET %1 POWER OFF").arg(locoList.getSRCPBus()));
    else
        sendSrcpMessageString(QString("SET %1 POWER ON").arg(locoList.getSRCPBus()));
}

/*
 * send emergency stop to all locomotives and switch off bus power
 */
void MainWindow::slotDaemonEmergencyStop()
{
    locoList.slotEmergencyHalt();

    slotDaemonPower();
    emit statusMessage(tr("Emergency stop done"));
}

/*
 * show programmer dialog with NMRA layout
 */
void MainWindow::slotPrgrmNMRA()
{
    Programmer *dp = new Programmer(this, NMRA, powerOn);
    connect(dp, SIGNAL(sendSrcpMessageString(const QString&)),
            this, SLOT(sendSrcpMessageString(const QString&)));

    dp->exec();
}

/*
 * show programmer dialog with Uhlenbrock layout
 */
void MainWindow::slotPrgrmUhl()
{
    Programmer *dp = new Programmer(this, UHL, powerOn);
    connect(dp, SIGNAL(sendSrcpMessageString(const QString&)),
            this, SLOT(sendSrcpMessageString(const QString&)));

    dp->exec();
}

/*
 * update all daemon related menu items
 */
void MainWindow::updateDaemonMenuItems()
{
    // toolbar buttons
    daemonConnectTBtn->setEnabled(!serverConnected);
    daemonDisconnectTBtn->setEnabled(serverConnected);
    daemonResetTBtn->setEnabled(serverConnected);
    daemonShutdownTBtn->setEnabled(serverConnected);
    daemonInfoTBtn->setEnabled(serverConnected);

    // menu items
    daemonMenu->setItemEnabled(DAEMON_ID_CONNECT, !serverConnected);
    daemonMenu->setItemEnabled(DAEMON_ID_DISCONNECT, serverConnected);
    daemonMenu->setItemEnabled(DAEMON_ID_RESET, serverConnected);
    daemonMenu->setItemEnabled(DAEMON_ID_SHUTDOWN, serverConnected);
    daemonMenu->setItemEnabled(DAEMON_ID_INFO, serverConnected);
    daemonMenu->setItemEnabled(DAEMON_ID_POWER, serverConnected);

    programMenu->setItemEnabled(PRGRM_ID_NMRA, serverConnected);
    programMenu->setItemEnabled(PRGRM_ID_UHL, serverConnected);

    updatePowerMenuItems();
}

/*
 * update all layout power related menu items
 */
void MainWindow::updatePowerMenuItems()
{
    if (serverConnected) {
        daemonPowerTBtn->setEnabled(!powerOn);
        daemonPowerOffTBtn->setEnabled(powerOn);

        bool enableEmergency = powerOn && (locoList.getLocoCount() > 0);
        daemonEmergencyTBtn->setEnabled(enableEmergency);
        daemonMenu->setItemEnabled(DAEMON_ID_EMERGENCY, enableEmergency);
    }
    else {
        daemonPowerTBtn->setEnabled(false);
        daemonPowerOffTBtn->setEnabled(false);
        daemonEmergencyTBtn->setEnabled(false);
        daemonMenu->setItemEnabled(DAEMON_ID_EMERGENCY, false);
    }
}

//TODO: showContextHelp(unsigned int ctx);

/*
 * open browser with help file
 */
void MainWindow::slotHelpHelp()
{
    QString sURL, sBrowserCmd;

    sURL = QString("%1/index.html").arg(HTML_DOC_DIR);

    switch (iBrowser) {
        case 0:
            sBrowserCmd = "firefox";
            break;
        case 1:
            sBrowserCmd = "konqueror";
            break;
        case 2:
            sBrowserCmd = "mozilla";
            break;
        case 3:
            sBrowserCmd = "netscape";
            break;
        case 4:
            sBrowserCmd = "opera";
            break;
    }

    if (iBrowser == 0 || iBrowser == 2) {
        if (system(sBrowserCmd + " -remote 'ping()'") == 0)
            system(sBrowserCmd + " -remote 'openURL(" + sURL +
                   ",new-tab)'");
        else
            system(sBrowserCmd + " " + sURL + " &");
    }
    else
        system(sBrowserCmd + " " + sURL + " &");
}


void MainWindow::slotHelpAbout()
{
    AboutDialog* ad = new AboutDialog(this);
    if (ad == NULL)
        return;

    ad->exec();
    delete ad;
}


void MainWindow::slotHelpAboutQt()
{
    QMessageBox::aboutQt(this, tr("About Qt"));
}


void MainWindow::slotDeleteLoco(unsigned int cn)
{
    setFileIDNewEnabled();
    //iNumOfLocos--;
    //FIXME delete loco

    if (locoList.getLocoCount() == 0)
        updatePowerMenuItems();

    modified = true;
    slotCleanupController(cn);
}


void MainWindow::slotCleanupController(unsigned int cn)
{
    //FIXME is this still needed ?
}

/*
 * is data of file modified?
 */
void MainWindow::slotModified(bool state)
{
    qWarning(QString("MainWindow::slotModified(bool state) %1").arg(state ? "true" : "false"));
    modified = state;
    fileSaveTBtn->setEnabled(modified);
    fileMenu->setItemEnabled(FILE_ID_SAVE, modified);
}

/*
 * send SRCP message to command port
 */
void MainWindow::sendSrcpMessageString(const QString& sm)
{
    locoList.getCommandPort()->sendToServer(sm);
}

/*
 * show response to server (command session) connection state changes
 */
void MainWindow::updateConnectionState(bool connected)
{
    if (serverConnected != connected) 
    {
        serverConnected = connected;
        updateDaemonMenuItems();
    }
}

/*
 * show response to layout power state changes
 */
void MainWindow::updatePowerState(bool ison)
{
    if (powerOn != ison) {
        powerOn = ison;
        updatePowerMenuItems();
    }
}

/*
 * filter incoming power change info message
 * select currently used bus number, other buses are ignored
 */
void MainWindow::processInfoMessage(const QString& info)
{
    QStringList tokens = QStringList::split(" ", info);

    //   <time> 100 INFO <bus> POWER ON
    //   <time> 100 INFO <bus> POWER OFF
    //     0     1   2     3     4    5
    if ((tokens[1] == "100") && (tokens[2] == "INFO") &&
            (tokens[3].toUInt() == locoList.getSRCPBus()) &&
            (tokens[4] == "POWER")) {
        updatePowerState(tokens[5] == "ON");
    }
}


void MainWindow::slotLocomotiveAdd()
{
    // FIXME bring this back to life
    Loco *newLoco = new Loco(NULL, "newLoco");

    newLoco->setBus(locoList.getSRCPBus());
    locoList.append( newLoco );
    // New controller will be at end of list !
    newLoco->slotEditProperties();
}


void MainWindow::slotLocomotiveEdit()
{
    //TODO
}


void MainWindow::slotLocomotiveDuplicate()
{
    //TODO
}


void MainWindow::slotLocomotiveRemove()
{
    //TODO
}

/*
 * update bus value of all loco controllers
 */
void MainWindow::updateControllerBus()
{
    //FIXME locoList.setBus(srcpbus); is this still needed ?
}


void MainWindow::slotDaemonProperties()
{
    ListPropertiesDialog *dlg = new ListPropertiesDialog(this);

    dlg->setServerData(locoList.getCommandPort()->getHostname(),
                       locoList.getCommandPort()->getPortNumber(), 
                       locoList.getSRCPBus());
    dlg->setActionData(locoList.getAutoConnect(), 
                       locoList.getAutoPowerOn(), 
                       locoList.getAutoSendInit());

    if (dlg->exec() == QDialog::Accepted) {
        locoList.getCommandPort()->setServer(dlg->getHostname(), dlg->getPort());
        locoList.getInfoPort()->setServer(dlg->getHostname(), dlg->getPort());
        unsigned int tmpbus = dlg->getBus();

        if (locoList.getSRCPBus() != tmpbus) {
            locoList.setSRCPBus(tmpbus);
            updateControllerBus();
        }

        locoList.setAutoConnect(dlg->getAutoConnect());
        locoList.setAutoPowerOn(dlg->getAutoPowerOn());
        locoList.setAutoSendInit(dlg->getAutoSend());

        modified = true;

        if (locoList.getAutoConnect())
            slotDaemonConnect();

    }
    delete dlg;
}


void MainWindow::readAutoloadFile()
{
    if (!autoLoad)
        return;

    // if autoload file from config data does
    // not exist ask user to change options
    
    if (!QFile::exists(cAutoloadFile)) {
        qApp->beep(); 
        int choice = QMessageBox::warning(this, tr("Autoloader failed"),
                         tr("The selected autoload file '%1'\n"
                            "does not exist. Please adjust your"
                            " options.").arg(cAutoloadFile),
                         tr("&Now"), tr("&Later"), 0, 0, 0);
        if (choice == 0)
            slotPreferences();
    }
    else
        openFile(cAutoloadFile);
}

/*
 * create new application window and open file
 * */
void MainWindow::openFileWindow(const QString& fn)
{               
    MainWindow *sw = new MainWindow();
    sw->resize(640, 480);
    sw->show();
    sw->openFile(fn);
}

void MainWindow::slotLocoControllerStateChanged(Loco *lc, bool state)
{
    if (state == true)
    {
        controlerArea->add( lc );
    }
}

