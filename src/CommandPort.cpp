/***************************************************************************
 CommandPort.cpp
 ------------
 Begin    : 17.08.2007
 Copyright: (C) 2007 by Guido Scholz <guido.scholz@bayernline.de>
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "CommandPort.h"



CommandPort::CommandPort(QObject* parent, const char* name,
        const char* host, unsigned int port,
        CommunicationStyle commstyle, bool translate):
    SrcpPort(parent, name, host, port, commstyle, translate)
{
}


QString CommandPort::getConnectionMode()
{
    return QString("COMMAND");
}
