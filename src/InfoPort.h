/***************************************************************************
                                InfoPort.h
                             -----------------
    begin                : 08.02.2001
    copyright            : (C) 2001 by Markus Pfeiffer
                           (C) 2007 by Guido Scholz
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef INFOPORT_H
#define INFOPORT_H

#include "srcpport.h"


class InfoPort: public SrcpPort {
  Q_OBJECT
   
public:
    InfoPort(QObject* parent = 0, const char * name = 0,
            const char* host = "localhost", unsigned int port = 4303,
            CommunicationStyle commstyle = csBoth, bool translate = false);

protected:
    QString getConnectionMode();
};

#endif                          //INFOPORT_H
