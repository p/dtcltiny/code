/***************************************************************************
                               Programmer.cpp
                             -------------------
    begin                : 11.11.2000
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <qapplication.h>
#include <qgroupbox.h>
#include <qhbox.h>
#include <qlayout.h>
#include <qmessagebox.h>
#include <qprogressbar.h>
#include <qpushbutton.h>
#include <qradiobutton.h>

#include "Programmer.h"


/*
 * SRCP commands in this file are only SRCP 0.7 so far
 */ 

Programmer::Programmer(QWidget* parent, int ptype, bool bStarted_):
    QDialog(parent, "ProgrammerDialog", true)
{
    type = ptype;
    bStarted = bStarted_;

    if (type == NMRA)
        setCaption(tr("Programmer for NMRA-Decoders"));
    else
        setCaption(tr("Programmer for Uhlenbrock-Decoders"));
    
    QVBoxLayout* baseLayout = new QVBoxLayout(this, 10, 6);
    QHBox* lrBox = new QHBox(this);
    lrBox->setSpacing (10);
    baseLayout->addWidget(lrBox);


    // action button group
    QButtonGroup *actionBG = new QButtonGroup(4, Qt::Vertical,
            tr("Action"), lrBox);
    actionBG->setExclusive(true);

    QRadioButton *rbCV_Set = new QRadioButton(tr("&Set a CV"), actionBG);
    QRadioButton *rbCVbit_Set =
        new QRadioButton(tr("Set &one bit of a CV"), actionBG);
    //QRadioButton *rbReg_Set =
        new QRadioButton(tr("Set a re&gister"), actionBG);
    QRadioButton *rbNMRA_Set =
        new QRadioButton(tr("Set Decoder to &NMRA only"), actionBG);
    connect(actionBG, SIGNAL(clicked(int)), this,
            SLOT(actionButtonClicked(int)));
    
    if (type == UHL) {
        rbCV_Set->setEnabled(false);
        rbCVbit_Set->setEnabled(false); // Registers
        rbNMRA_Set->setEnabled(false);
        actionBG->setButton(2);
    }
    else 
        actionBG->setButton(0);

    // parameter group box
    QGroupBox* paramGB = new QGroupBox(0, Qt::Horizontal,
            tr("Parameters"), lrBox, "fieldsGB");

    // grid layout with 4 rows and 2 columns
    QGridLayout* paramLayout = new QGridLayout(paramGB->layout(),
            4, 2, 10, "paramLayout");

    leCV_Reg = new QLineEdit(paramGB, "cv_reg");
    paramLayout->addWidget(leCV_Reg, 0, 1);
    leCV_Reg->setMaxLength(4);

    leBit = new QLineEdit(paramGB, "bit");
    paramLayout->addWidget(leBit, 1, 1);
    leBit->setMaxLength(1);

    leValue = new QLineEdit(paramGB, "value");
    paramLayout->addWidget(leValue, 2, 1);
    leValue->setMaxLength(3);

    leAddress = new QLineEdit(paramGB, "address");
    paramLayout->addWidget(leAddress, 3, 1);
    leAddress->setMaxLength(3);
    leAddress->setEnabled(type == UHL);

    if (type == NMRA)
        labelCV_Reg = new QLabel("&CV #:", paramGB);
    else
        labelCV_Reg = new QLabel("&Register #:", paramGB);

    paramLayout->addWidget(labelCV_Reg, 0, 0);
    labelCV_Reg->setBuddy(leCV_Reg);

    labelBit = new QLabel(tr("&Bit #:"), paramGB);
    paramLayout->addWidget(labelBit, 1, 0);
    labelBit->setBuddy(leBit);

    labelValue = new QLabel(tr("&Value:"), paramGB);
    paramLayout->addWidget(labelValue, 2, 0);
    labelValue->setBuddy(leValue);

    labelAddress = new QLabel(tr("&Address:"), paramGB);
    paramLayout->addWidget(labelAddress, 3, 0);
    labelAddress->setEnabled(type == UHL);
    labelAddress->setBuddy(leAddress);


    // Program and Cancel buttons
    QHBoxLayout* buttonLayout = new QHBoxLayout(0, 0, 6);
    baseLayout->addLayout(buttonLayout);
    buttonLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));

    QPushButton *programmPB = new QPushButton(tr("Programm"), this,
            "programmPB");
    buttonLayout->addWidget(programmPB);
    programmPB->setDefault(true);
    connect(programmPB, SIGNAL(clicked()), this, SLOT(programmDecoder()));

    QPushButton* cancelPB = new QPushButton(tr("Cancel"), this,
            "cancelPB");
    buttonLayout->addWidget(cancelPB);
    connect(cancelPB, SIGNAL(clicked()), this, SLOT(reject()));

    actionButtonClicked(actionBG->selectedId());
}


// programmer selection
void Programmer::programmDecoder()
{
    if (type == UHL)
        programmUHL();
    else
        programmNMRA();

}


// sets up the right window half in respect to the left one
void Programmer::actionButtonClicked(int button)
{
    labelCV_Reg->setEnabled(button != 3);
    leCV_Reg->setEnabled(button != 3);

    labelValue->setEnabled(button != 3);
    leValue->setEnabled(button != 3);

    labelBit->setEnabled(button == 1);
    leBit->setEnabled(button == 1);

    labelAddress->setEnabled(button == 2 && type == UHL);
    leAddress->setEnabled(button == 2 && type == UHL);

    switch (button) {
        case 0:
            // CV set
            labelCV_Reg->setText(tr("&CV #:"));
            leCV_Reg->clear();
            leValue->clear();
            leBit->clear();
            leAddress->clear();
            break;
        case 1:
            // CV bit set
            labelCV_Reg->setText(tr("&CV #:"));
            leCV_Reg->clear();
            leValue->clear();
            leBit->clear();
            leAddress->clear();
            break;
        case 2:
            // Register set
            labelCV_Reg->setText(tr("&Register #:"));
            leCV_Reg->clear();
            leValue->clear();
            leBit->clear();
            leAddress->clear();
            break;
        case 3:
            // CV 29 bit 2 Value 0
            labelCV_Reg->setText(tr("&CV #:"));
            leCV_Reg->setText("29");
            leBit->setText("2");
            leValue->setText("0");
            break;
    }
}


// NMRA dependent programming code
void Programmer::programmNMRA()
{
    int CV_Reg = leCV_Reg->text().toInt();
    int Value = leValue->text().toInt();
    int Bit = leBit->text().toInt();
    QString args;

    switch (actionBG->selectedId()) {
        case 0:                // CV set
            args.sprintf("WRITE GL NMRA CV %d %d", CV_Reg - 1, Value);
            break;
        case 1:                // CV bit set
        case 3:
            args.sprintf("WRITE GL NMRA CVBIT %d %d %d", CV_Reg - 1,
                         Bit, Value);
            break;
        case 2:                // Register set
            args.sprintf("WRITE GL NMRA REG %d %d", CV_Reg, Value);
            break;
    }

    //TODO: insert bus information
    if (bStarted)
        // stop the refresh cycle
        emit sendSrcpMessageString("SET POWER OFF");

    emit sendSrcpMessageString(args);

    if (bStarted)
        emit sendSrcpMessageString("SET POWER ON");

    QMessageBox::information(this, "dtcltiny",
                             tr("Programming completed"));
    this->accept();
}


// Uhlenbrock dependent programming code
void Programmer::programmUHL()
{
    int CV_Reg = leCV_Reg->text().toInt();
    int Value = leValue->text().toInt();
    int Address = leAddress->text().toInt();
    int i, j;

    QProgressBar *progress = new QProgressBar(this);
    progress->setGeometry(20, 210, 410, 20);
    progress->show();
    qApp->processEvents();

    // activate loco
    emit sendSrcpMessageString("RESET SERVER");

    usleep(20000);

    if (!bStarted)
        emit sendSrcpMessageString("SET POWER ON");

    usleep(20000);
    progress->setProgress(5);
    qApp->processEvents();

    QString args;
    args.sprintf("SET GL M4 %d 1 1 0 0 4 0 0 0 0", Address);

    for (i = 0; i < 10; i++) {
        for (j = 0; j < 20; j++)
            emit sendSrcpMessageString(args);

        sleep(1);
        progress->setProgress(10 + i * 5);
        qApp->processEvents();
    }

    args.sprintf("SET GL M4 %d 1 0 0 0 4 0 0 0 0", Address);
    emit sendSrcpMessageString(args);
    
    for (i = 0; i < 4; i++) {
        sleep(1);
        progress->setProgress(60 + i * 5);
        qApp->processEvents();
    }

    // activate register
    args.sprintf("SET GL M4 %d 1 1 0 0 4 0 0 0 0", CV_Reg);
    emit sendSrcpMessageString(args);
    usleep(20000);
    args.sprintf("SET GL M4 %d 1 1 0 0 4 0 0 0 0", CV_Reg);
    emit sendSrcpMessageString(args);
    usleep(20000);

    // write data
    args.sprintf("SET GL M4 %d 1 1 0 0 4 0 0 0 0", Value);
    emit sendSrcpMessageString(args);
    usleep(20000);
    args.sprintf("SET GL M4 %d 1 0 0 0 4 0 0 0 0", Value);
    emit sendSrcpMessageString(args);
    usleep(20000);

    // send operation code for quit programming (80)
    args.sprintf("SET GL M4 80 1 1 0 0 4 0 0 0 0");
    emit sendSrcpMessageString(args);
    usleep(20000);
    args.sprintf("SET GL M4 80 1 0 0 0 4 0 0 0 0");
    emit sendSrcpMessageString(args);

    for (i = 0; i < 4; i++) {
        sleep(1);
        progress->setProgress(80 + i * 5);
        qApp->processEvents();
    }

    // stop the refresh cycle
    emit sendSrcpMessageString("SET POWER OFF");
    
    if (bStarted) {
        usleep(20000);
        emit sendSrcpMessageString("SET POWER ON");
    }

    progress->setProgress(100);
    qApp->processEvents();
    QMessageBox::information(this, "dtcltiny",
                             tr("Programming completed"));
    this->accept();
}
