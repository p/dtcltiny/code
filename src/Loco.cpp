//
// C++ Implementation: Loco
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <qpixmap.h>
#include <qtextstream.h>

#include "Loco.h"
#include "pixmaps/fdirection.xpm"
#include "pixmaps/bdirection.xpm"

/*string constant key values for data file*/
#define DF_ADDRESS      "address"
#define DF_CONTR_VISIBLE "controller_visible"
//#define DF_BUS          "bus"
#define DF_NAME         "name(loco)"
#define DF_ALIAS        "alias(train)"
#define DF_SHOWWHAT     "show what"
#define DF_ICON         "icon"
#define DF_PROTOCOL     "protocol"
#define DF_DECODER      "decoder"
#define DF_DIRECTION    "direction"
#define DF_SHOWSPEED    "show speed"
#define DF_SPEEDUNIT    "speed unit"
#define DF_REALMAXSPEED "real max speed"
#define DF_MINSPEED     "min_speed"
#define DF_AVGSPEED     "avg_speed"
#define DF_MAXSPEED     "max_speed"
#define DF_ACCELTIME    "accel_time"
#define DF_BREAKTIME    "break_time"
// functions
#define DF_F0_AVAILABLE "f0_available"
#define DF_F0_STATE     "f0_state"
#define DF_F0_ICON      "f0_icon"
#define DF_F0_TYPE      "f0_type"
#define DF_F1_AVAILABLE "f1_available"
#define DF_F1_STATE     "f1_state"
#define DF_F1_ICON      "f1_icon"
#define DF_F1_TYPE      "f1_type"
#define DF_F2_AVAILABLE "f2_available"
#define DF_F2_STATE     "f2_state"
#define DF_F2_ICON      "f2_icon"
#define DF_F2_TYPE      "f2_type"
#define DF_F3_AVAILABLE "f3_available"
#define DF_F3_STATE     "f3_state"
#define DF_F3_ICON      "f3_icon"
#define DF_F3_TYPE      "f3_type"
#define DF_F4_AVAILABLE "f4_available"
#define DF_F4_STATE     "f4_state"
#define DF_F4_ICON      "f4_icon"
#define DF_F4_TYPE      "f4_type"

Loco::Loco(QObject* parent, const char* name): QObject(parent, name)
{
	init();
	setDefaultValues();
}

Loco::Loco(QTextStream &fileTs)
{
	init();
	setDefaultValues();
	load(fileTs);
}

Loco::~Loco()
{
    emit willDeleteMe(this);    // Signal other that I will die !
}

void Loco::init(void)
{
	LCtimer = new QTimer(this, "LCtimer");
	connect(LCtimer, SIGNAL(timeout()), SLOT(slotSpeedTick()));
}

void Loco::setDefaultValues(void)
{
    reverseDirection = false;
    powerOn = false;
    nomSpeed = 0;
    iLocoSpeed = nomSpeed;

    // saveable values
    address = 1;
    controllerVisible = true;   // So we see it after creation
    bus = 1;
    locoName = "Name";
    locoAlias = "Alias";
    iLocoShow = 0;
    locoPicture = "";
    locoProtocol = "N1";
    locoDecoder = "";
    locoDirection = true;
    iLocoShowSpeed = 1;
    cLocoSpeedUnit = "km/h";
    iLocoRealMaxSpeed = 100;
    iLocoMinSpeed = 5;
    iLocoAvgSpeed = 14;
    iLocoMaxSpeed = 28;
    iLocoAccelTime = 1000;
    iLocoBreakTime = 700;
    iLocoFuncAvail[0] = 0;
    iLocoFuncState[0] = 0;
    cLocoFuncIcon[0] = "";
    iLocoFuncType[0] = 0;
    iLocoFuncAvail[1] = 0;
    iLocoFuncState[1] = 0;
    cLocoFuncIcon[1] = "";
    iLocoFuncType[1] = 0;
    iLocoFuncAvail[2] = 0;
    iLocoFuncState[2] = 0;
    cLocoFuncIcon[2] = "";
    iLocoFuncType[2] = 0;
    iLocoFuncAvail[3] = 0;
    iLocoFuncState[3] = 0;
    cLocoFuncIcon[3] = "";
    iLocoFuncType[3] = 0;
    iLocoFuncAvail[4] = 0;
    iLocoFuncState[4] = 0;
    cLocoFuncIcon[4] = "";
    iLocoFuncType[4] = 0;

    //calculated values
    emit valueChanged();
    emit propertyChanged(this);
}

QPixmap Loco::getDirectionPix(void)
{
		if (reverseDirection) 
		{
			if (locoDirection) //forward by negation
					return QPixmap(fdirection);
			else
					return QPixmap(bdirection);
		}
		if (locoDirection) // forward without negation
			return QPixmap(fdirection);
		else
			return QPixmap(bdirection);
}

void Loco::slotSetAutoSpeed(unsigned int which_AutoSpeed)
{
    unsigned int newSpeed = getLocoSpeed();
    if ((iLocoAccelTime == 0) && (iLocoBreakTime == 0)) 
    {
        switch (which_AutoSpeed) 
        {
            case 0:
                newSpeed = (0);
                break;
            case 1:
                newSpeed = (iLocoMinSpeed);
                break;
            case 2:
                newSpeed = (iLocoAvgSpeed);
                break;
            case 3:
                newSpeed = (iLocoMaxSpeed);
                break;
        }
    }
    else 
    {
        switch (which_AutoSpeed) 
        {
            case 0:
                newSpeed = 0;
                break;
            case 1:
                newSpeed = iLocoMinSpeed;
                break;
            case 2:
                newSpeed = iLocoAvgSpeed;
                break;
            case 3:
                newSpeed = iLocoMaxSpeed;
                break;
        }
    }
    setSpeed(newSpeed);
}

void Loco::startTimer()
{
    LCtimer->stop();
    if (nomSpeed > iLocoSpeed) 
    {
        LCtimer->start(iLocoAccelTime);
    }
    else if (nomSpeed < iLocoSpeed) 
    {
        LCtimer->start(iLocoBreakTime);
    }
}

void Loco::slotSpeedTick()
{
    qWarning("Loco::slotSpeedTick()");
    // Check for a change
    if (iLocoSpeed != getLocoSpeed()) 
    {
        qWarning("Loco::slotSpeedTick() change");
        // no manual- or infoport-manipulation
        if (iLocoSpeed < getLocoSpeed())
        {
            iLocoSpeed++;
            startTimer();sendLocoState( );
        }
        else if (iLocoSpeed > getLocoSpeed())
        {
            iLocoSpeed--;
            startTimer();
            sendLocoState( );
        }
        else if (iLocoSpeed == getLocoSpeed())
        {
            LCtimer->stop();
        }
    }
    else 
    {
        qWarning("Loco::slotSpeedTick() no change");
        LCtimer->stop();
        sendLocoState( );
    }
    // We always report our end speed
    updateSpeed(getLocoSpeed());    
}

void Loco::slotEmergencyHalt()
{
	setEmergencyHalt( );
}

void Loco::setEmergencyHalt(void)
{
    LCtimer->stop();
    QString args;
    iLocoSpeed = 0;
    nomSpeed = 0;
    slotSpeedTick();
}

// send SRCP command string
// .arg() does not work here because we have more than 10 arguments
void Loco::sendLocoState()
{
    unsigned int drivemode = (locoDirection != false) ? 1 : 0;
    /*
     * SRCP 0.8:
     * SET <bus> GL <addr> <drivemode> <V> <V_max> <f1> .. <fn>
     */
    QString message;
    message.sprintf("SET %d GL %d %d %d %d %d %d %d %d %d",
        bus,                // 1
        address,            // 2
        drivemode,          // 3
        iLocoSpeed,         // 4
        getDecoderSpeedSteps(),          // 5
        iLocoFuncState[0],  // 6
        iLocoFuncState[1],  // 7
        iLocoFuncState[2],  // 8
        iLocoFuncState[3],  // 9
        iLocoFuncState[4]); // 10

    qWarning(QString("Loco::sendLocoState(): speed=%1, nomSpeed=%2 CMD=%3").arg(iLocoSpeed).arg(nomSpeed).arg(message));
    emit sendSrcpMessageString(message);
}

/*
 * send init values to server when a new connection is established
 */
void Loco::sendLocoInit()
{
	QString decinit = "";
	
	if (locoProtocol == "NB")
		decinit = "N 1 14 0";
	else if (locoProtocol == "N1")
		decinit = "N 1 28 5";
	else if (locoProtocol == "N2")
		decinit = "N 1 128 5";
	else if (locoProtocol == "N3")
		decinit = "N 2 28 5";
	else if (locoProtocol == "N4")
		decinit = "N 2 128 5";
	else if (locoProtocol == "M1")
		decinit = "M 1 14 1";
	else if (locoProtocol == "M2")
		decinit = "M 2 14 5";
	
	/*
		* SRCP 0.8:
		* INIT <bus> GL <addr> <protocol> <protov> <decfahrst> <anz_funct>
		*/
	QString message = QString("INIT %1 GL %2 %3")
			.arg(bus)               // 1
			.arg(address)           // 2
			.arg(decinit);          // 3
	
	emit sendSrcpMessageString(message);
	
	/* TODO: check this
	QString *state = commandPort->init(command);
	parseInfo(state);
	*/
}

void Loco::updateFunctionState(unsigned int idx, bool state)
{
    if (iLocoFuncState[idx] != state)
    {
        iLocoFuncState[idx] = state;
        emit valueChanged();
    }
}

void Loco::setFunctionState(unsigned int idx, bool state)
{
    updateFunctionState(idx, state);
    sendLocoState();
}

QPixmap Loco::getFunctionPix(unsigned int idx)
{
	QString prefix;
	prefix.sprintf("%1d", iLocoFuncState[idx]);
	return QPixmap(PIX_DIR + prefix + cLocoFuncIcon[idx]);
}

void Loco::updateSpeed(unsigned int speed)
{
    if (nomSpeed != speed)
    {
        nomSpeed = speed;
        emit valueChanged();
    }
}

void Loco::setSpeed(unsigned int speed)
{
    if (speed == 0) 
    {}
    else if (speed > iLocoMaxSpeed)
        speed = iLocoMaxSpeed;
    else if (speed < iLocoMinSpeed)
        speed = iLocoMinSpeed;
    if (nomSpeed != speed)
    {
        updateSpeed(speed);
        slotSpeedTick();	// will send new value
    }
}

void Loco::updateDirectionFwd(bool dir)
{
    if (locoDirection != dir)
    {
        locoDirection = dir;
        emit valueChanged();
    }
}

void Loco::setDirectionFwd(bool dir)
{
    updateDirectionFwd(dir);
    sendLocoState();
}

unsigned int Loco::getDecoderSpeedSteps()
{
    // default value for protocols M1, M2, M4, NB
    int returnvalue = 14;

    if (locoProtocol == "M3")
        returnvalue = 28;
    else if (locoProtocol == "N1")
        returnvalue = 28;
    else if (locoProtocol == "N3")
        returnvalue = 28;
    else if (locoProtocol == "N2")
        returnvalue = 128;
    else if (locoProtocol == "N4")
        returnvalue = 128;
    
    return returnvalue;
}

bool Loco::save(QTextStream* fileTs)
{
    *fileTs << "LOCO  # loco    " << endl;
    *fileTs << "address:        " << address << endl;
//    *fileTs << "bus:            " << bus << endl;
    *fileTs << "name(loco):     " << locoName << endl;
    *fileTs << "alias(train):   " << locoAlias << endl;
    *fileTs << "show what:      " << ((iLocoShow) ? "alias" : "name") <<
        endl;
    *fileTs << "icon:           " << locoPicture << endl;
    *fileTs << "protocol:       " << locoProtocol << endl;
    *fileTs << "decoder:        " << locoDecoder << endl;
    for (int j = 0; j <= 4; j++) {
        *fileTs << "f" << j << "_available:   " << ((iLocoFuncAvail[j]) ?
                                                    "yes" : "no") << endl;
        *fileTs << "f" << j << "_state:       " << ((iLocoFuncState[j]) ?
                                                    "on" : "off") << endl;
        *fileTs << "f" << j << "_icon:        " << cLocoFuncIcon[j] <<
            endl;
        *fileTs << "f" << j << "_type:        " << ((iLocoFuncType[j]) ?
                                                    "switch" :
                                                    "pushbutton") << endl;
    }
    *fileTs << "direction:          " << ((locoDirection > 0) ? "fwd" : "rev") << endl;
    *fileTs << "show speed:         " << ((iLocoShowSpeed > 0) ? "real" : "steps") << endl;
    *fileTs << "speed unit:         " << cLocoSpeedUnit << endl;
    *fileTs << "real max speed:     " << iLocoRealMaxSpeed << endl;
    *fileTs << "min_speed:          " << iLocoMinSpeed << endl;
    *fileTs << "avg_speed:          " << iLocoAvgSpeed << endl;
    *fileTs << "max_speed:          " << iLocoMaxSpeed << endl;
    *fileTs << "accel_time:         " << iLocoAccelTime << endl;
    *fileTs << "break_time:         " << iLocoBreakTime << endl;
    *fileTs << "controller_visible: " << ((controllerVisible) ? "yes" : "no") << endl;
    *fileTs << "-------------------------------------" << endl;
    return true;
}

/*
 * read loco data from text stream
 */
bool Loco::load(QTextStream &ts)
{
    QString s, key, value;

    setDefaultValues();
    while (!ts.eof()) 
    {
        s = ts.readLine();
        if (!s.startsWith("#")) 
        {
            /*break if end of single loco data set*/
            if (s.startsWith("-"))
                break;

            key = s.section(":", 0, 0);
            value = s.section(":", 1, 1).stripWhiteSpace();

            if (key.compare(DF_ADDRESS) == 0)
            {
                address = value.stripWhiteSpace().toUInt();
            }
            /*
            else if (key.compare(DF_BUS) == 0){
                bus = value.stripWhiteSpace().toUInt();
            }
            */
            else if (key.compare(DF_NAME) == 0)
            {
                locoName = value.stripWhiteSpace();
            }
            else if (key.compare(DF_ALIAS) == 0)
            {
                locoAlias = value.stripWhiteSpace();
            }
            else if (key.compare(DF_SHOWWHAT) == 0)
            {
                iLocoShow = value.stripWhiteSpace().toInt();
            }
            else if (key.compare(DF_ICON) == 0)
            {
                locoPicture = value.stripWhiteSpace();
            }
            else if (key.compare(DF_PROTOCOL) == 0)
            {
                locoProtocol = value.stripWhiteSpace();
            }
            else if (key.compare(DF_DECODER) == 0)
            {
                locoDecoder = value.stripWhiteSpace();
            }
            else if (key.compare(DF_DIRECTION) == 0)
            {
                locoDirection = (value.stripWhiteSpace() == "fwd") ? 1 : -1;
            }
            else if (key.compare(DF_SHOWSPEED) == 0)
            {
                iLocoShowSpeed = (value.stripWhiteSpace() == "real") ? 1 : -1;
            }
            else if (key.compare(DF_SPEEDUNIT) == 0)
            {
                cLocoSpeedUnit = value.stripWhiteSpace();
            }
            else if (key.compare(DF_REALMAXSPEED) == 0)
            {
                iLocoRealMaxSpeed = value.stripWhiteSpace().toInt();
            }
            else if (key.compare(DF_MINSPEED) == 0)
            {
                iLocoMinSpeed = value.stripWhiteSpace().toInt();
            }
            else if (key.compare(DF_AVGSPEED) == 0)
            {
                iLocoAvgSpeed = value.stripWhiteSpace().toInt();
            }
            else if (key.compare(DF_MAXSPEED) == 0)
            {
                iLocoMaxSpeed = value.stripWhiteSpace().toInt();
            }
            else if (key.compare(DF_ACCELTIME) == 0)
            {
                iLocoAccelTime = value.stripWhiteSpace().toInt();
            }
            else if (key.compare(DF_BREAKTIME) == 0)
            {
                iLocoBreakTime = value.stripWhiteSpace().toInt();
            }
            else if (key.compare(DF_F0_AVAILABLE) == 0)
            {
                iLocoFuncAvail[0] =
                    (value.stripWhiteSpace() == "yes") ? 1 : 0;
            }
            else if (key.compare(DF_F0_STATE) == 0)
            {
                iLocoFuncState[0] =
                    (value.stripWhiteSpace() == "on") ? 1 : 0;
            }
            else if (key.compare(DF_F0_ICON) == 0)
            {
                cLocoFuncIcon[0] = value.stripWhiteSpace();
            }
            else if (key.compare(DF_F0_TYPE) == 0)
            {
                iLocoFuncType[0] =
                    (value.stripWhiteSpace() == "switch") ? 1 : 0;
            }
            else if (key.compare(DF_F1_AVAILABLE) == 0)
            {
                iLocoFuncAvail[1] =
                    (value.stripWhiteSpace() == "yes") ? 1 : 0;
            }
            else if (key.compare(DF_F1_STATE) == 0)
            {
                iLocoFuncState[1] =
                    (value.stripWhiteSpace() == "on") ? 1 : 0;
            }
            else if (key.compare(DF_F1_ICON) == 0)
            {
                cLocoFuncIcon[1] = value.stripWhiteSpace();
            }
            else if (key.compare(DF_F1_TYPE) == 0)
            {
                iLocoFuncType[1] =
                    (value.stripWhiteSpace() == "switch") ? 1 : 0;
            }
            else if (key.compare(DF_F2_AVAILABLE) == 0)
            {
                iLocoFuncAvail[2] =
                    (value.stripWhiteSpace() == "yes") ? 1 : 0;
            }
            else if (key.compare(DF_F2_STATE) == 0)
            {
                iLocoFuncState[2] =
                    (value.stripWhiteSpace() == "on") ? 1 : 0;
            }
            else if (key.compare(DF_F2_ICON) == 0)
            {
                cLocoFuncIcon[2] = value.stripWhiteSpace();
            }
            else if (key.compare(DF_F2_TYPE) == 0)
            {
                iLocoFuncType[2] =
                    (value.stripWhiteSpace() == "switch") ? 1 : 0;
            }
            else if (key.compare(DF_F3_AVAILABLE) == 0)
            {
                iLocoFuncAvail[3] =
                    (value.stripWhiteSpace() == "yes") ? 1 : 0;
            }
            else if (key.compare(DF_F3_STATE) == 0)
            {
                iLocoFuncState[3] =
                    (value.stripWhiteSpace() == "on") ? 1 : 0;
            }
            else if (key.compare(DF_F3_ICON) == 0)
            {
                cLocoFuncIcon[3] = value.stripWhiteSpace();
            }
            else if (key.compare(DF_F3_TYPE) == 0)
            {
                iLocoFuncType[3] =
                    (value.stripWhiteSpace() == "switch") ? 1 : 0;
            }
            else if (key.compare(DF_F4_AVAILABLE) == 0)
            {
                iLocoFuncAvail[4] =
                    (value.stripWhiteSpace() == "yes") ? 1 : 0;
            }
            else if (key.compare(DF_F4_STATE) == 0)
            {
                iLocoFuncState[4] =
                    (value.stripWhiteSpace() == "on") ? 1 : 0;
            }
            else if (key.compare(DF_F4_ICON) == 0)
            {
                cLocoFuncIcon[4] = value.stripWhiteSpace();
            }
            else if (key.compare(DF_F4_TYPE) == 0)
            {
                iLocoFuncType[4] =
                    (value.stripWhiteSpace() == "switch") ? 1 : 0;
            }
            else if (key.compare(DF_CONTR_VISIBLE) == 0)
            {
                controllerVisible = (value.stripWhiteSpace() == "yes") ? true : false;
            }
        }
    } // while
    emit valueChanged();
    emit propertyChanged(this);
    return true;
}

/*
 * parse incomming SRCP INFO messages
 *
 * SRCP 0.7:
 * INFO GL <protocol> <addr> <direction> <V> <V_max> <func> <nofn> <f1>...<fn>
 *   0  1      2        3        4        5     6      7      8     9...13
 *
 * SRCP 0.8:
 * <time> 101 INFO <bus> GL <addr> <protocol> <pvers> <speedsteps> <nofn>
 *   0     1   2     3   4    5        6         7        8          9
 * <time> 100 INFO <bus> GL <addr> <drivemode> <V> <V_max> <f1> ... <fn>
 *   0     1   2     3   4    5        6        7     8     9 10 11 12 13
 *
 * */
void Loco::parseInfo(const QString& info)
{
    /*
        NOTE:   We must not use any of the functions that will emit a
                loco command. This will result in an endless loop, since
                the loco command will generate a info command which will
                generate a loco command.......
                So make shure to use the update Function, not the set functions !
    */
    QStringList tokens = QStringList::split(" ", info);

    // only choose type 100 INFO messages for this bus
    if ((tokens[1] == "100") && (tokens[2] == "INFO") &&
         (tokens[3].toUInt() == bus)) 
    {
        // choose messages for GL
        if (tokens[4] == "GL") 
        {
            // check if message is for this loco, identified by bus and
            // address
            if (tokens[5].toUInt() == address) 
            {
                // extract speed
                // Max Speed of decoder as the server know
                int decMaxStep = tokens[8].toInt();
                // Current speed of decoder upto decMaxStep
                int decCurStep = tokens[7].toInt();
                // scale to our need (knowledge)
                int newSpeed = (decCurStep * getDecoderSpeedSteps()) / decMaxStep;
                // WE only take the new speed if it differs from
                // our last send value. If we simulate acceleration
                // we get values different from "nomSpeed"
                //FIXME we must not take the feedback, while we simulate acceleration !
                // This could leed to problems if two controllers give commands.
                if (!LCtimer->isActive())   
                {
                    nomSpeed = newSpeed;
                    updateSpeed(newSpeed);
                }

                // direction, emergency stop?
                if (tokens[6] == "1")
                    updateDirectionFwd(true);
                else if (tokens[6] == "0")
                    updateDirectionFwd(false);

                // functions
                // F0
                if (iLocoFuncType[0] == SWITCH)
                    updateFunctionState(0, tokens[9].toInt() > 0);

                // F1 - F4
                //send number of function values may be shorter
                for (unsigned int i = 1; i <= 4; i++)
                {
                    if (tokens.count() >= (i + 9))
                    {
                        if (iLocoFuncType[i] == SWITCH)
                        {
                            updateFunctionState(i, tokens[9+i].toInt() > 0);
                        }
                    }
                    else
                        break;
                }
            }   // If it is for us
        }   // if GL

        // choose POWER messages
        //   <time> 100 INFO <bus> POWER ON
        //   <time> 100 INFO <bus> POWER OFF
        //     0     1   2     3     4    5
        else if (tokens[4] == "POWER") 
        {
            updateSrcpData(tokens[5] == "ON");
        }
    }   // if info message
}


void Loco::processInfoMessage(const QString& info)
{
    parseInfo(info);
    //TODO: update only changed widged
    //emit valueChanged();	// Inform other about the change
}

/*
 * slot to get signals by server connection object
 * react to server connection state changes
 */
void Loco::updateSrcpData(bool ison)
{
    if (powerOn != ison) 
    {
        powerOn = ison;
        if (ison)
           sendLocoInit();
    }
}

void Loco::setLocoShow(int i) 
{
	if (iLocoShow != i)
	{
		iLocoShow = i;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoShowSpeed(unsigned int i) 
{
	if (iLocoShowSpeed != i)
	{
		iLocoShowSpeed = i;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoName(const QString& n) 
{
	if (locoName != n)
	{
		locoName = n;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoAlias(const QString& n) 
{
	if (locoAlias != n)
	{
		locoAlias = n;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoPicture(const QString& p) 
{
	if (locoPicture != p)
	{
		locoPicture = p;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoDecoder(const QString& d) 
{
	if (locoDecoder != d)
	{
		locoDecoder = d;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoProtocol(const QString& p) 
{
	if (locoProtocol != p)
	{
		locoProtocol = p;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setAddress(const unsigned int adr) 
{
	if (address != adr)
	{
		address = adr;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoFuncAvail(const unsigned int idx, const bool avail) 
{
	if (iLocoFuncAvail[idx] != avail)
	{
		iLocoFuncAvail[idx] = avail;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoFuncTypeSwitch(const unsigned int idx, const bool isSw) 
{
	if (iLocoFuncType[idx] != isSw)
	{
		iLocoFuncType[idx] = isSw;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoFuncIcon(const unsigned int idx, const QString& file) 
{
	if (cLocoFuncIcon[idx] != file)
	{
		cLocoFuncIcon[idx] = file;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoRealMaxSpeed(const unsigned int speed) 
{
	if (iLocoRealMaxSpeed != speed)
	{
		iLocoRealMaxSpeed = speed;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoMaxSpeed(const unsigned int speed) 
{
	if (iLocoMaxSpeed != speed)
	{
		iLocoMaxSpeed = speed;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoAvgSpeed(const unsigned int speed) 
{
	if (iLocoAvgSpeed != speed)
	{
		iLocoAvgSpeed = speed;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoMinSpeed(const unsigned int speed) 
{
	if (iLocoMinSpeed != speed)
	{
		iLocoMinSpeed = speed;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoAccelTime(const unsigned int time) 
{
	if (iLocoAccelTime != time)
	{
		iLocoAccelTime = time;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoBreakTime(const unsigned int time) 
{
	if (iLocoBreakTime != time)
	{
		iLocoBreakTime = time;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setReverseDirection(const bool reverse) 
{
	if (reverseDirection != reverse)
	{
		reverseDirection = reverse;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setLocoSpeedUnit(const QString& speedUnit) 
{
	if (cLocoSpeedUnit != speedUnit)
	{
		cLocoSpeedUnit = speedUnit;
		emit propertyChanged(this);	// Inform other about the change
	}
}

void Loco::setBus(unsigned int b)
{
	if (bus != b)
	{
		bus = b;
		emit propertyChanged(this);	// Inform other about the change
	}
}

bool Loco::getLocoDirectionFwd() 
{
    return locoDirection;
}

unsigned int Loco::getLocoShowSpeed() 
{
    return iLocoShowSpeed;
}

unsigned int Loco::getLocoSpeed() 
{
    return nomSpeed;
}

unsigned int Loco::getLocoRealMaxSpeed() 
{
    return iLocoRealMaxSpeed;
}

unsigned int Loco::getLocoMinSpeed() 
{
    return iLocoMinSpeed;
}

unsigned int Loco::getLocoAvgSpeed() 
{
    return iLocoAvgSpeed;
}

unsigned int Loco::getLocoMaxSpeed() 
{
    return iLocoMaxSpeed;
}

unsigned int Loco::getLocoAccelTime() 
{
    return iLocoAccelTime;
}

unsigned int Loco::getLocoBreakTime() 
{
    return iLocoBreakTime;
}

bool Loco::getLocoFuncAvail(unsigned int i) 
{
    return iLocoFuncAvail[i];
}

bool Loco::getLocoFuncState(unsigned int i) 
{
    return iLocoFuncState[i];
}

unsigned int Loco::getLocoFuncType(unsigned int i) 
{
    return iLocoFuncType[i];
}

QString Loco::getLocoFuncIcon(unsigned int i) 
{
    return cLocoFuncIcon[i];
}

QString Loco::getLocoPicture(void) 
{
    return QString(LOCO_PIX_DIR) + locoPicture;
}

int Loco::getLocoShow(void) 
{
    return iLocoShow;
}

QString Loco::getLocoName(void) 
{
    return locoName;
}

QString Loco::getLocoAlias(void) 
{
    return locoAlias;
}

float Loco::getLocoSpeedFactor() 
{
    return (float)getLocoRealMaxSpeed() / getLocoMaxSpeed();
}

QString Loco::getLocoSpeedUnit() 
{
    return cLocoSpeedUnit;
}

bool Loco::getReverseDirection() 
{
    return reverseDirection;
}

QString Loco::getLocoDecoder() 
{
    return locoDecoder;
}

QString Loco::getLocoProtocol() 
{
    return locoProtocol;
}

unsigned int Loco::getBus() 
{
    return bus;
}

unsigned int Loco::getAddress(void)
{
    return address;
}

void Loco::setControllerVisible(bool state)
{
    qWarning(QString("Loco::setControllerVisible(bool state) %1").arg(state ? "true" : "false"));
    if (controllerVisible != state)
    {
        controllerVisible = state;
        emit controllerVisibleChanged(this, controllerVisible);
    }
}

bool Loco::getControllerVisible(void)
{
    return controllerVisible;
}

void Loco::slotEditProperties(void)
{
    emit editProperties();
}

