/***************************************************************************
                               MainWindow.h
                             -----------------
    begin                : 11.11.2000
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <qapplication.h>
#include <qlistview.h>
#include <qtextstream.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qkeycode.h>
#include <qmainwindow.h>
#include <qmenubar.h>
#include <qmessagebox.h>
#include <qpixmap.h>
#include <qpopupmenu.h>
#include <qstatusbar.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qwidget.h>
#include <qlayout.h>


#include "LocoControl.h"
#include "lococontrollerwidget.h"
#include "LocoList.h"
#include "locolistview.h"
#include "messagehistory.h"
#include "Resources.h"


class MainWindow: public QMainWindow 
{
    Q_OBJECT
    
    public:
        MainWindow();
        ~MainWindow();
        void openFile(const QString&);
        void openFileWindow(const QString&);
        void readAutoloadFile();
    
    public slots:
        void processInfoMessage(const QString&);
        void updateConnectionState(bool);
        void updatePowerState(bool);
    
    signals:
        void statusMessage(const QString&);
    
    private:
        void slotCleanupController(unsigned int);
        void setupMainWindow();
        void chooseFile();
        void newFile();
        bool saveFile();
        void updateControllerBus();
        void updateDaemonMenuItems();
        void updatePowerMenuItems();
        void loadPreferences();
        void savePreferences();
        void showToolbar(bool);
        void showStatusbar(bool);
        void setFileIDNewEnabled();
        int querySaveChanges();
    
    protected:
        void closeEvent(QCloseEvent*);
    
    private slots:
        void sendSrcpMessageString(const QString&);
        void slotDaemonConnect();
        void slotDaemonDisconnect();
        void slotDaemonEmergencyStop();
        void slotDaemonInfo();
        void slotDaemonPower();
        void slotDaemonProperties();
        void slotDaemonReset();
        void slotDaemonShutdown();
        void slotDeleteLoco(unsigned int);
        void slotFileOpen();
        void slotFileNew();
        void slotFileSave();
        void slotFileSaveAs();
        void slotHelpAbout();
        void slotHelpAboutQt();
        void slotHelpHelp();
        void slotLocomotiveAdd();
        void slotLocomotiveDuplicate();
        void slotLocomotiveEdit();
        void slotLocomotiveRemove();
        void slotModified(bool);
        void slotPreferences();
        void slotPrgrmNMRA();
        void slotPrgrmUhl();
        void slotToggleStatusbar();
        void slotToggleToolbar();
        void slotUpdateCaption();
        void slotLocoControllerStateChanged(Loco *, bool);
    
    private:
        QPopupMenu *fileMenu;
        QPopupMenu *viewMenu;
        QPopupMenu *daemonMenu;
        QPopupMenu *locomotiveMenu;
        QPopupMenu *programMenu;
        QPopupMenu *optionsMenu;
        QPopupMenu *helpMenu;
        LocoControllerWidget *controlerArea;
        LocoListView *locoListView;
        QToolBar *fileTb;
        QToolBar *daemonTb;
        QToolBar *locolistTb;
    
        QToolButton *fileNewTBtn;
        QToolButton *fileOpenTBtn;
        QToolButton *fileSaveTBtn;
    
        QToolButton *daemonConnectTBtn;
        QToolButton *daemonDisconnectTBtn;
        QToolButton *daemonResetTBtn;
        QToolButton *daemonShutdownTBtn;
        QToolButton *daemonInfoTBtn;
        QToolButton *daemonEmergencyTBtn;
        QToolButton *daemonPowerTBtn;
        QToolButton *daemonPowerOffTBtn;
    
        QToolButton *locolistAddTBtn;
    
        MessageHistory* messageHistory;
        
        // file data
        LocoList locoList;

        bool modified;
        /*Preferences */
        bool autoLoad;
        QString cAutoloadFile;
        int iBrowser;
        //dynamic data
        bool powerOn;
        bool serverConnected;
        QString lastDir;
};

#endif                          //MAINWINDOW_H
