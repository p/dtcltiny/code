/***************************************************************************
                               listpropertiesdialog.h
                             -----------------
    begin                : 2004-10-26
    copyright            : (C) 2004-2007 by Guido Scholz
    email                : guido.scholz@bayernline.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LISTPROPERTIESDIALOG_H
#define LISTPROPERTIESDIALOG_H

#include <qcheckbox.h>
#include <qdialog.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qhbox.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qpushbutton.h>

#include "Resources.h"

class ListPropertiesDialog: public QDialog {
  Q_OBJECT
   
  public:
    ListPropertiesDialog(QWidget* parent = 0);
    void setServerData(const QString&, unsigned int, unsigned int);
    void setActionData(bool, bool, bool);
    QString getHostname();
    unsigned int getPort();
    unsigned int getBus();
    bool getServerLogin();
    bool getAutoConnect();
    bool getAutoPowerOn();
    bool getAutoSend();

  protected slots:
    void autologinChanged(int);
    void autopowerChanged(int);
    void slotReadServiceChanged(bool);

  private:
    QLabel* portLabel;
    QLineEdit *hostLE, *portLE, *fileLE, *busLE;
    QCheckBox* readserviceCB;
    QCheckBox* autoconnectCB;
    QCheckBox* autopoweronCB;
    QCheckBox* autosendCB;
};

#endif   //LISTPROPERTIESDIALOG_H
