/*
 * aboutdialog.cpp
 * ---------------
 * Begin
 *   2007-08-26
 * 
 * Copyright
 *   (C) 2007 Guido Scholz <guido.scholz@bayernline.de>
 * 
 * Description
 *   Dialog window to display program information
 *
 * License
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 */

#include <qfont.h>
#include <qgroupbox.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qgrid.h>

#include "aboutdialog.h"
#include "config.h"

/* application icon */
#include "../icons/dtcltiny_48.xpm"


AboutDialog::AboutDialog(QWidget* parent): QDialog(parent,
        "AboutDialog", true)
{
    setCaption(tr("About %1").arg(PACKAGE));
    QVBoxLayout* baseLayout = new QVBoxLayout(this, 10, 10);

    QLabel* label;

    // first line: pixmap, programname, version
    QHBoxLayout* pixmapLayout = new QHBoxLayout(baseLayout, 20, "xpmLayout");
    label = new QLabel(this, "pixmapLabel");
    pixmapLayout->addWidget(label);
    label->setPixmap(QPixmap(dtcltiny_48_xpm));

    label = new QLabel(PACKAGE_STRING, this, "appLabel");
    pixmapLayout->addWidget(label);
    QFont font;
    font.setPointSize(18);
    font.setWeight(QFont::Bold);
    label->setFont(font);

    pixmapLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));

    // second line: description
    label = new QLabel(
            tr("SRCP client to control digital model trains. It is based\n"
                "on \"DigitalTrainControl for Linux\" by Stefan Preis."),
          this, "descLabel");
    baseLayout->addWidget(label);

    // third line: web link
    label = new QLabel("http://sourceforge.net/projects/dtcltiny",
          this, "webLabel");
    baseLayout->addWidget(label);


    // authors groupbox
    QGroupBox* authorGB = new QGroupBox(4, Qt::Vertical,
            tr("Authors"), this, "authorsGB");
    baseLayout->addWidget(authorGB);

    label = new QLabel("Markus Pfeiffer <dtcltiny@markus-pfeiffer.de>",
            authorGB);
    label = new QLabel("Guido Scholz <guido.scholz@bayernline.de>",
            authorGB);
    label = new QLabel("Johann Vieselthaler <tpdap@users.sourceforge.net>",
            authorGB);
    label = new QLabel("Juergen Sachs <jsachs@users.sourceforge.net>",
            authorGB);

    /*
    // contributors groupbox
    QGroupBox* contribGB = new QGroupBox(1, Qt::Vertical,
            tr("Contributors"), this, "infoGB");
    baseLayout->addWidget(contribGB);

    label = new QLabel("Contributors will be listed here.", contribGB);
    */

    // OK button
    QHBoxLayout* buttonLayout = new QHBoxLayout(0, 0, 6);
    baseLayout->addLayout(buttonLayout);
    buttonLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));

    QPushButton* okPB = new QPushButton(tr("OK"), this);
    buttonLayout->addWidget(okPB);
    okPB->setDefault(true);
    connect(okPB, SIGNAL(clicked()), this, SLOT(accept()));

    buttonLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));
}


