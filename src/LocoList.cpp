//
// C++ Implementation: LocoList
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <qstring.h>
#include <qstringlist.h>
#include <qmessagebox.h>
#include <qdatetime.h>

#include "LocoList.h"
#include "Loco.h"

//constants for data file
#define KS             "="
#define DS             ":"
#define DF_HOST        "host"
#define DF_BUS         "bus"
#define DF_AUTOCONNECT "autoconnect"
#define DF_AUTOPOWERON "autopoweron"
#define DF_AUTOSEND    "autosendinit"

LocoList::LocoList(QObject *parent, const char *name) : 
        QObject(parent, name)
{
	// init sockets with default values
	commandPort = new CommandPort(this, "locoList.getCommandPort()");
	Q_CHECK_PTR(getCommandPort());
	
	infoPort = new InfoPort(this, "locoList.getInfoPort()");
	Q_CHECK_PTR(getInfoPort());
	
	clear();	// Init also file data
}

LocoList::~LocoList()
{
	clear();
	
	delete commandPort;
	delete infoPort;
}

void LocoList::clear(void)				// clear list of all locos
{
	Loco *lc;
	for ( lc = list.first(); lc; lc = list.next() )
	{
		list.remove(lc);
		delete lc;
		lc = 0;
	}
		
	// reset file data
	srcpbus = 1;
	autoConnect = false;
	autoPowerOn = false;
	autoSendInit = false;
	readservice = false;

	setModified(true);
}

int LocoList::load(QString fn)	// loads the list of locos from file
{
    QFile file(fn);
    if (!file.open(IO_ReadOnly)) 
    {
        emit statusMessage(tr("Error, could not read file '%1'.").arg(fn));
        return false;
    }
    setModified(true);
    //FIXME slotDaemonDisconnect(); ???
    //FIXME qApp->processEvents(); // we do not realy need that here ?! should not take tooooo long

    filename = fn;

    // delete all current controllers
    clear();

    QTextStream ts(&file);
    ts.setEncoding(QTextStream::UnicodeUTF8);

    QString s, key, value;

    while (!ts.eof()) {
        s = ts.readLine();
        key = s.section(KS, 0, 0);
        value = s.section(KS, 1, 1);

        /* ignore comment lines */
        if (s.startsWith("#"))
            continue;

        else if (key.compare(DF_HOST) == 0) 
        {
            QStringList tokens = QStringList::split(DS, value);
            commandPort->setServer(tokens[0], tokens[1].toUInt()); 
            infoPort->setServer(tokens[0], tokens[1].toUInt()); 
        }
        else if (key.compare(DF_BUS) == 0) 
        {
             srcpbus = value.toUInt();
        }
        else if (key.compare(DF_AUTOCONNECT) == 0) 
        {
             autoConnect = value.toInt();
        }
        else if (key.compare(DF_AUTOPOWERON) == 0) 
        {
             autoPowerOn = value.toInt();
        }
        else if (key.compare(DF_AUTOSEND) == 0) 
        {
             autoSendInit = value.toInt();
        }
        else if (s.startsWith("LOCO")) 
        {
            Loco* lc = new Loco(ts); //, controlerArea, iNumOfLocos);	// will create and load Loco
            if (lc != NULL) 
            {
                lc->setBus(srcpbus);
                append(lc);

                if (getLocoCount() >= MAX_CONTROLLERS) 
                {
                    QMessageBox::warning(0, PACKAGE,
                            tr("There is not enough space to load all locos.\n"
                               "Loading is aborted."),
                            QMessageBox::Ok | QMessageBox::Default,
                            QMessageBox::NoButton);
                    break;
                }
            }
            // else show error message, memory to low
        }

    }

    file.close();
    setBus( getSRCPBus() );	// update all controllers to the bus !
    
    emit fileNameChanged();	// Hmm, tbd ! Should not hurt either !
    emit statusMessage(tr("File with %1 locos loaded: %2")
            .arg(getLocoCount()).arg(fn));
        
    setModified(true);
    setModified(false);

    return true;
}

int LocoList::save(QString filename)	// save list of locos to file
{
    QFile file(filename);
    if (file.open(IO_WriteOnly) == false) {
        QMessageBox::warning(0, tr("Error"),
                             tr("Could not open %1 for writing.").
                             arg(filename));
        return false;
    }

    QTextStream ts(&file);
    ts.setEncoding(QTextStream::UnicodeUTF8);
    QDateTime dt = QDateTime::currentDateTime();

    // write the header
    ts << "# dtcltiny data file" << endl
        << "# version=" << VERSION << endl
        << "# last modified=" << dt.toString(Qt::ISODate) << endl
        << DF_HOST << KS << commandPort->getHostname()
        << DS << commandPort->getPortNumber() << endl
        << DF_BUS << KS << srcpbus << endl
        << DF_AUTOCONNECT << KS << autoConnect << endl
        << DF_AUTOPOWERON << KS << autoPowerOn << endl
        << DF_AUTOSEND << KS << autoSendInit << endl
        << "%" << endl
        << "# end of header section" << endl;


		Loco *lc;
		for ( lc = list.first(); lc; lc = list.next() )
		{
			lc->save( &ts );
		}

    file.close();
    emit fileNameChanged();
    emit statusMessage(tr("File %1 saved.").arg(filename));
    setModified(false);

    return true;
}

void LocoList::append(Loco *lc)		// appends Loco to List
{
    if (lc == 0)
    {
        qWarning("LocoList::append(): tried to add Loco qith Adr==NULL");
        return;
    }
    if (!isInList(lc))	// only if not in List
    {
        list.append(lc);
        connect(lc, SIGNAL(controllerVisibleChanged(Loco *,bool)), 
                this,SLOT(slotControllerVisibleChanged(Loco *, bool)));
        connect(lc, SIGNAL(sendSrcpMessageString( const QString& )), 
                commandPort, SLOT(sendSrcpMessageString(const QString&)));
        connect(lc, SIGNAL(propertyChanged( Loco* )), 
                this, SLOT(slotLocoPropertyChanged( Loco* )));
        connect(lc, SIGNAL(willDeleteMe( Loco* )),
                this, SLOT(slotLocoRemoved( Loco *)));
        connect(infoPort, SIGNAL(messageReceived( const QString& )), 
                lc, SLOT(processInfoMessage( const QString& )));
        // since we connected after load, force it now.
        slotControllerVisibleChanged(lc, lc->getControllerVisible());   
        
        emit newLocoAdded(lc); // Inform others that we have a new member
    }
    else
    {
        qWarning("Loco already in List");
    }
}

void LocoList::slotLocoRemoved( Loco *lc)
{
    remove(lc);
}

void LocoList::remove(Loco *lc)		// remove Loco from List
{
    if (isInList(lc))	// Not realy needed here, but save is save
    {
        lc->setControllerVisible( false );  // no longer in list, so close controller
        list.remove(lc);
        emit locoRemoved(lc);
        disconnect(lc, 0, 0, 0);
    }
}

int LocoList::isInList(Loco *lcToFind)		// remove loco from list if present
{
    Loco *lc;
    int pos = 0;
    for ( lc = list.first() ; lc != 0 ; lc = list.next() )
    {
        pos++;
        if (lc == lcToFind) return pos;
    }
    return 0;
}

uint LocoList::getLocoCount() const
{
	return list.count();
}

void LocoList::setBus(unsigned int bus)
{
    Loco *lc;
    for ( lc = list.first(); lc; lc = list.next() )
    {
        lc->setBus( bus );
    }
}

void LocoList::setSRCPBus(uint b) 
{
    srcpbus = b;
    setModified(true);
}

void LocoList::setAutoConnect(bool s)
{
    autoConnect = s;
    setModified(true);
}

void LocoList::setAutoPowerOn(bool s) 
{
    autoPowerOn = s;
    setModified(true);
}

void LocoList::setAutoSendInit(bool s) 
{
    autoSendInit = s;
    setModified(true);
}

void LocoList::setReadService(bool s) 
{
    readservice = s;
    setModified(true);
}

void LocoList::setModified(bool state) // maybe we want to inform someone later about the change
{
    qWarning(QString("LocoList::setModified(bool state) %1").arg(state ? "true" : "false"));
    if (true) //modified != state)
    {
        modified = state;	
        emit listModified(modified);
    }
}

uint LocoList::getSRCPBus() 
{
    return srcpbus;
}

bool LocoList::getAutoConnect() 
{
    return autoConnect;
}

bool LocoList::getAutoPowerOn() 
{
    return autoPowerOn;
}

bool LocoList::getAutoSendInit() 
{
    return autoSendInit;
}

bool LocoList::getReadService() 
{
    return readservice;
}

CommandPort *LocoList::getCommandPort() 
{
    return commandPort;
}

InfoPort *LocoList::getInfoPort() 
{
    return infoPort;
}

void LocoList::slotControllerVisibleChanged(Loco *lc, bool state)
{
    qWarning(QString("LocoList::slotControllerVisibleChanged(Loco *lc, bool state) %1").arg((state ? "true" : "false")));
    emit locoControllerStateChanged(lc, lc->getControllerVisible());
}

Loco *LocoList::getFirst()
{
    return list.first();
}

Loco *LocoList::getNext()
{
    return list.next();
}

Loco *LocoList::getPrev()
{
    return list.prev();
}

Loco *LocoList::getLast()
{
    return list.last();
}

void LocoList::slotEmergencyHalt(void)
{
    for (Loco *lc = list.first() ; lc != 0 ; lc = list.next() )
    {
        lc->setEmergencyHalt();
    }
}

QString LocoList::getFileName()
{
    return filename;
}

void LocoList::setFileName(QString& fn)
{
    filename = fn;
}

/** This slot is mainly for these who do not want
 *  or can connect to every loco
 *
 *  para: lc loco that has been changed
 */
void LocoList::slotLocoPropertyChanged(Loco *lc)
{
    setModified( true );
    emit locoPropertyChanged( lc );
}
