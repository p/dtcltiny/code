//
// C++ Implementation: locolistview
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <qstring.h>
#include <qlayout.h>
#include <qlistview.h>
#include <qvbox.h>

#include "Loco.h"
#include "LocoList.h"
#include "locolistview.h"

LocoListView::LocoListView(QWidget *parent, const char *name, LocoList *list) :
        QWidget(parent, name)
{
    this->list = list;
    vBox = new QVBox(parent, "locoListBox", 0);
    vBox->setSizePolicy ( QSizePolicy::Preferred, QSizePolicy::Preferred, false );
    locoListView = new QListView(vBox, "locoListView", 0);
    locoListView->setAllColumnsShowFocus(true);
    //TODO FIx the width of the Listview.
    
    locoListView->addColumn(tr("Visible"));
    //locoListView->setColumnWidthMode(0, QListView::Maximum);
    locoListView->addColumn(tr("Name"));
    //locoListView->setColumnWidthMode(1, QListView::Maximum);
    locoListView->addColumn(tr("Alias"));
    //locoListView->setColumnWidthMode(2, QListView::Maximum);
    locoListView->addColumn(tr(" x"));
    locoListView->setResizeMode(QListView::NoColumn); //AllColumns);
    locoListView->setSizePolicy ( QSizePolicy::Expanding, QSizePolicy::Expanding, false );
    locoListView->setShowToolTips(true);
    locoListView->setSorting( 1, true);
    
    update();   // Populate our list
    
    //connections to our listView
    connect(locoListView, SIGNAL(clicked( QListViewItem* )), 
            this, SLOT(clickedLocoItem( QListViewItem* )));
    connect(locoListView, SIGNAL(returnPressed( QListViewItem* )), 
            this, SLOT(clickedLocoItem( QListViewItem* )));
    connect(locoListView, SIGNAL(spacePressed( QListViewItem* )), 
            this, SLOT(clickedLocoItem( QListViewItem* )));
    connect(locoListView, SIGNAL(doubleClicked( QListViewItem *, const QPoint &, int )), 
            this, SLOT(doubleClickedLocoItem( QListViewItem *, const QPoint &, int )));
    
    // connections to our locoList
    connect(list, SIGNAL(newLocoAdded(Loco *)), 
            this, SLOT(slotLocoCreated(Loco *)));
    connect(list, SIGNAL(locoRemoved(Loco *)), 
            this, SLOT(slotLocoDeleted(Loco *)));
    connect(list, SIGNAL(locoControllerStateChanged(Loco *, bool)), 
            this, SLOT(slotLocoVisibleChanged(Loco *, bool)));
    connect(list, SIGNAL(locoPropertyChanged(Loco *)), 
            this, SLOT(slotLocoPropertyChanged( Loco* )));
}
        
LocoListView::~LocoListView()
{
    qWarning("locoListView::~LocoListView()");
    clear();
}

void LocoListView::update(void)
{
    qWarning("locoListView::update()");
    // Traverse the List an display all locos
    LocoItem *li;
    for(Loco *lc = list->getFirst() ; lc != 0 ; lc = list->getNext())
    {
        li = findLoco(lc);
        if (li == 0)  // If not in list already
            slotLocoCreated(lc);
        else
            li->update();   // update only
    }
}

void LocoListView::slotLocoCreated(Loco *lc)
{
    qWarning("locoListView::slotLocoCreated()");
    locoListView->insertItem(new LocoItem(locoListView, lc));
}

void LocoListView::slotLocoDeleted(Loco *lc)
{
    qWarning("locoListView::slotLocoDeleted()");
    LocoItem *li = findLoco( lc );
    if (li)
    {
        locoListView->takeItem( li );
        delete li;
    }
}

void LocoListView::clear(void)
{
    qWarning("locoListView::clear()");
    // We delete all childs, every time take the first and delete it.
    for (LocoItem *li = (LocoItem*)locoListView->firstChild() ; li != 0 ; li = (LocoItem*)locoListView->firstChild())
    {
        delete li;
    }
}

LocoItem *LocoListView::findLoco(Loco *lc)
{
    LocoItem *li = 0;
    QListViewItemIterator it( locoListView );
    while ( it.current() ) 
    {
        li = (LocoItem*)it.current();
        if (li->isMyLoco(lc)) return li;
        ++it;
    }
    return 0;
}

void LocoListView::slotLocoVisibleChanged(Loco *lc, bool state)
{
    qWarning(QString("locoListView::slotLocoVisibleChanged() %1").arg(state ? "true" : "false"));
    LocoItem *li = findLoco( lc );
    if (li)
    {
        li->update();
    }
}

void LocoListView::clickedLocoItem(QListViewItem *lvi)
{
    qWarning("locoListView::clickedLocoItem()");
    if (lvi)    // If not clicked on valid item, we get Zero -> seg Fault
    {
        LocoItem *li = (LocoItem *)lvi;
        li->setVisible(li->isOn());    // This also check if there is a change !
    }
    
}

void LocoListView::doubleClickedLocoItem( QListViewItem *lvi, const QPoint &, int )
{
    qWarning("locoListView::doubleClickedLocoItem()");
    if (lvi)
    {
        LocoItem *li = (LocoItem *)lvi;
        // Toggle state
        li->setVisible( !li->isOn() );
    }
}

void LocoListView::slotLocoPropertyChanged( Loco *lc )
{
    LocoItem *li = findLoco( lc );  // search for the ListItem
    if (li)
    {
        li->update();  // update if found
    }
}

/********************************************************************************
** LocoItem Parts..... they are here since no one else needs to access them ?! **
********************************************************************************/

LocoItem::LocoItem(QListView *parent, Loco *lc) : QCheckListItem(parent, "", QCheckListItem::CheckBox)
{
    loco = lc;
    
    update();
    
}

LocoItem::~LocoItem()
{
    
}

void LocoItem::update(void)
{
    qWarning(QString("LocoItem::update(void) %1").arg(loco->getControllerVisible() ? "true" : "false"));
    setText(1, loco->getLocoName());
    setText(2, loco->getLocoAlias());
    setOn(loco->getControllerVisible());
}

bool LocoItem::isMyLoco(Loco *lc)
{
    return (loco == lc);
}

void LocoItem::setVisible(bool state)
{
    if (state != loco->getControllerVisible())
    {
        loco->setControllerVisible(state);
        setOn(state);   // make shure they are in sync
    }
}
