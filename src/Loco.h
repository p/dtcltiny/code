//
// C++ Interface: Loco
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef LOCO_H
#define LOCO_H

#include <qfile.h>
#include <qtimer.h>

#include "LocoFunction.h"
#include "Resources.h"

enum showID {
    NAME,
    ALIAS
};

enum funcTypes {
    PUSHBUTTON,
    SWITCH
};

enum speedTypes {
    STEPS,
    REAL
};


class Loco : public QObject
{
    Q_OBJECT
    public:
        Loco(QObject* parent = NULL, const char* name = NULL);
        Loco(QTextStream &fileTs);	// load this one loco
        ~Loco();
        bool    load(QTextStream &fileTs);	// load this one loco
        bool    save(QTextStream* fileTs);	// save this one loco
        void    setDefaultValues(void);
        
        // get Functions
        bool    getLocoDirectionFwd();         // (1/-1) = (forward/reverse)
        unsigned int     getLocoShowSpeed();
        unsigned int     getLocoSpeed();
        unsigned int     getLocoRealMaxSpeed();
        unsigned int     getLocoMinSpeed();
        unsigned int     getLocoAvgSpeed();
        unsigned int     getLocoMaxSpeed();
        unsigned int     getLocoAccelTime();
        unsigned int     getLocoBreakTime();
        QPixmap getDirectionPix(void);
        bool     getLocoFuncAvail(unsigned int i);
        bool     getLocoFuncState(unsigned int i);
        unsigned int     getLocoFuncType(unsigned int i);
        QString getLocoFuncIcon(unsigned int i);
        unsigned int getAddress(void);
        QString getLocoPicture(void);
        int     getLocoShow(void);
        QString getLocoName(void);
        QString getLocoAlias(void);
        QPixmap getFunctionPix(unsigned int idx);
        unsigned int     getDecoderSpeedSteps(void);
        float   getLocoSpeedFactor(void);
        QString getLocoSpeedUnit(void);
        bool    getReverseDirection(void);
        QString getLocoDecoder(void);
        QString getLocoProtocol(void);
        unsigned int     getBus(void);
        bool    getControllerVisible(void);
        
        // Set Functions (Alpahbetical Order !)
        void sendLocoInit(void);	// init Loco to server
        void sendLocoState(void);	// send Loco state to server
        void setAddress(const unsigned int adr);
        void setBus(const unsigned int i);
        void setControllerVisible(bool);
        void setEmergencyHalt(void);
        void setLocoAlias(const QString& n);
        void setLocoAccelTime(const unsigned int time);
        void setLocoAvgSpeed(const unsigned int speed);
        void setLocoBreakTime(const unsigned int time);
        void setLocoDecoder(const QString& d);
        void setLocoFuncAvail(const unsigned int idx, const bool avail);
        void setLocoFuncIcon(const unsigned int idx, const QString& file);
        void setLocoFuncTypeSwitch(const unsigned int idx, const bool isSw);
        void setLocoMaxSpeed(const unsigned int speed);
        void setLocoMinSpeed(const unsigned int speed);
        void setLocoName(const QString& n);
        void setLocoPicture(const QString& p);
        void setLocoProtocol(const QString& p);
        void setLocoRealMaxSpeed(const unsigned int speed);
        void setLocoShow(int i);
        void setLocoShowSpeed(const unsigned int i);
        void setLocoSpeedUnit(const QString& speedUnit);
        void setReverseDirection(const bool reverse);
        
        // The following commands will call the cooresponding
        // update functions to react on changes and will
        // ALWAYS send a loco command. We do not cache commands !
        void setDirectionFwd(bool dir);
        void setFunctionState(unsigned int idx, bool state);
        void setSpeed(unsigned int speed);
        
        // The following functions will only store the current value
        // and emit a signal if there was a change
        void updateDirectionFwd(bool dir);
        void updateFunctionState(unsigned int, bool);
        void updateSpeed(unsigned int speed);
    
    signals:
        // The visablity of the controller changed
        void controllerVisibleChanged(Loco *, bool);
        // is there anyone who could show an edit property dialog ?!
        void editProperties(void);
        // some of the loco properties changed (name, alias etc.)
        void propertyChanged(Loco *);   
        // send a message to the srcp daemon
        void sendSrcpMessageString(const QString&);
        // a runtime value has changed (Speed, function etc.)
        void valueChanged(void);
        // This Loco will delete itself NOW, do not use it any more
        void willDeleteMe(Loco *);
    
    public slots:
        void slotEditProperties(void);
        void processInfoMessage(const QString&);
        void slotEmergencyHalt();
        void slotSetAutoSpeed(unsigned int);
        void updateSrcpData(bool);
        
    private:    // Functions
        void parseInfo(const QString& info);
        void startTimer(void);
        void init();
        
    private slots:
        void slotSpeedTick();
        
    private:    // Variables
        QTimer *LCtimer;
    
        QString locoName;
        QString locoAlias;
        QString locoPicture;
        QString locoProtocol;
        QString locoDecoder;
        QString cLocoFuncIcon[5];
        QString cLocoSpeedUnit;
        
        // Address of the Loco
        unsigned int address;
        // bus of the loco
        unsigned int bus;
        // this should be bool: showname
        int iLocoShow;
        // The Speed we simulate while accelerate
        unsigned int iLocoSpeed;
        // is function available
        unsigned int iLocoFuncAvail[5];
        // Current Function State
        bool iLocoFuncState[5];
        // Type of Function
        unsigned int iLocoFuncType[5];
        
        // should the direction of the loco be reversed
        bool reverseDirection;      // reverses the direction of the shown arrow
        // current direction of the loso
        bool locoDirection;         // true == Forward
        // is the corresponding controller visible
        bool controllerVisible;
        // How do we show the speed
        unsigned int iLocoShowSpeed;
        // Real Spped the train could reach ( mph / km/h )
        unsigned int iLocoRealMaxSpeed;
        // Minimum speed the loco will work with
        unsigned int iLocoMinSpeed;
        unsigned int iLocoAvgSpeed;
        unsigned int iLocoMaxSpeed;
        unsigned int iLocoAccelTime;
        unsigned int iLocoBreakTime;
        // our speed we want to reach
        unsigned int nomSpeed;
        bool powerOn;
};



#endif /* LOCO_H */
