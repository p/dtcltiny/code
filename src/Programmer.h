/***************************************************************************
                               Programmer.h
                             -----------------
    begin                : 11.11.2000
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PROGRAMMER_H
#define PROGRAMMER_H

#include <qbuttongroup.h>
#include <qdialog.h>
#include <qlineedit.h>
#include <qlabel.h>


enum PrgrmID { NMRA,
    UHL
};

class Programmer: public QDialog {
  Q_OBJECT
   
public:
    Programmer(QWidget* parent, int ptype, bool bStarted);

private slots:
    void programmDecoder();
    void programmNMRA();
    void programmUHL();
    void actionButtonClicked(int);

private:
    QButtonGroup* actionBG;
    QLabel *labelCV_Reg;
    QLabel *labelBit;
    QLabel *labelValue;
    QLabel *labelAddress;
    QLineEdit *leCV_Reg;
    QLineEdit *leBit;
    QLineEdit *leValue;
    QLineEdit *leAddress;
    int type;
    bool bStarted;

signals:
    void sendSrcpMessageString(const QString&);
};

#endif                          //PROGRAMMER_H
