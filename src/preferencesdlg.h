/***************************************************************************
                               preferencesdlg.h
                             -----------------
    begin                : 2004-10-26
    copyright            : (C) 2004-2007 by Guido Scholz
    email                : guido.scholz@bayernline.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef PREFERENCESDLG_H
#define PREFERENCESDLG_H

#include <qcheckbox.h>
#include <qcombobox.h>
#include <qdialog.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <qpushbutton.h>

#include "Resources.h"

class PreferencesDialog: public QDialog {
  Q_OBJECT
   
  public:
    PreferencesDialog(QWidget * parent = 0);
    void setHistoryData(bool, bool);
    void setFileData(bool, const QString&, int);
    bool getAutoload();
    QString getAutoloadFile();
    int getBrowserNr();
    bool getShowMessageTime();
    bool getTimeTranslation();

  protected slots:
    void chooseFile();
    void slotAutoloadChanged(bool);

  private:
    QLineEdit *fileLE;
    QCheckBox* autoloadCB;
    QCheckBox* showMessageTimeCB;
    QCheckBox* translateServerTimeCB;
    QPushButton* filePB;
    QComboBox* browserCB;
};

#endif   //PREFERENCESDLG_H
