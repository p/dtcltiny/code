/***************************************************************************
 InfoPort.cpp
 ------------
 begin    : 08.02.2001
 copyright: (C) 2001 by Markus Pfeiffer <mail@markus-pfeiffer.de>
            (C) 2007 by Guido Scholz <guido.scholz@bayernline.de>
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "InfoPort.h"



InfoPort::InfoPort(QObject* parent, const char* name,
        const char* host, unsigned int port,
        CommunicationStyle commstyle, bool translate):
    SrcpPort(parent, name, host, port, commstyle, translate)
{
}


QString InfoPort::getConnectionMode()
{
    return QString("INFO");
}
