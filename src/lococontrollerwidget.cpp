//
// C++ Implementation: LocoControllerWidget
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <qframe.h>
#include <qscrollview.h>

#include "lococontrollerwidget.h"

LocoControllerWidget::LocoControllerWidget( QWidget *parent, const char *name ) 
    : QWidget( parent, name )
{
    parentWidget = parent;
    
    QScrollView* sv = new QScrollView(parentWidget, "crontrollerSV");
    Q_CHECK_PTR(sv);

    controlerArea = new QFrame(sv->viewport(), "controlerArea");
    sv->addChild(controlerArea);
    
        //controlerArea->setMinimumSize(4 * C_WIDTH_, C_HEIGHT_);
    controlerArea->setSizePolicy(QSizePolicy::Expanding,
                                 QSizePolicy::Expanding, false);
    controlerArea->setFrameStyle(QFrame::Panel | QFrame::Plain);
    controlerArea->setBackgroundColor(darkGray);
    
    layoutMain = new QHBoxLayout(controlerArea);
    layoutMain->setAlignment(Qt::AlignLeft | Qt::AlignTop);

}

LocoControllerWidget::~LocoControllerWidget()
{
    clear();
}

void LocoControllerWidget::add(Loco *lc)
{
    if (!isVisible(lc))
    {
        LocoControl *locoC = new LocoControl(controlerArea, "LocoControl", lc);
        if (locoC != NULL) 
        {
            layoutMain->addWidget(locoC, 0, Qt::AlignLeft);
            locoC->show();
        }
    }
}

void LocoControllerWidget::remove(LocoControl *lcC)
{
    if (list.remove(lcC)) delete lcC;
}

void LocoControllerWidget::clear(void)
{
    for (LocoControl *lcC = getFirst() ; lcC != 0 ; lcC = getNext())
    {
        remove(lcC);
    }
}

bool LocoControllerWidget::isVisible(Loco *lc)
{
    for (LocoControl *lcC = getFirst() ; lcC != 0 ; lcC = getNext())
    {
        if (lcC->isMyLoco(lc)) return true;
    }
    return false;
}

LocoControl *LocoControllerWidget::getFirst()
{
    return list.first();
}

LocoControl *LocoControllerWidget::getLast()
{
    return list.last();
}

LocoControl *LocoControllerWidget::getNext()
{
    return list.next();
}

LocoControl *LocoControllerWidget::getPrev()
{
    return list.prev();
}
