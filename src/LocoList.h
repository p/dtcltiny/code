//
// C++ Interface: LocoList
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef LOCO_LIST_H
#define LOCO_LIST_H

#include <qptrlist.h>
#include <qfile.h>

#include "CommandPort.h"
#include "InfoPort.h"
#include "Loco.h"

class LocoList : public QObject
{
    Q_OBJECT
    public:
        LocoList(QObject *parent = 0, const char *name = NULL);
        ~LocoList();
        void    clear(void);				// clear list of all locos
        int     load(QString file);	// loads the list of locos from file
        int     save(QString file);	// save list of locos to file
        void    append(Loco *lc);		// appends Loco to List
        void    remove(Loco *lc);		// remove loco from list if present
        int     isInList(Loco *lc);	// Is Loco in List, returns pos nr in list
        uint    getLocoCount() const;
        uint    getSRCPBus();
        bool    getAutoConnect();
        bool    getAutoPowerOn();
        bool    getAutoSendInit();
        bool    getReadService();
        CommandPort *getCommandPort();	// Protect the pointer
        InfoPort *getInfoPort();	// Protect the pointer
        Loco    *getFirst();
        Loco    *getNext();
        Loco    *getPrev();
        Loco    *getLast();
        QString getFileName();
        
        void setFileName(QString& fn);
        void setBus(unsigned int bus);		// set Bus on all Locos
        void setSRCPBus(uint b);
        void setAutoConnect(bool s);
        void setAutoPowerOn(bool s);
        void setAutoSendInit(bool s);
        void setReadService(bool s);
        void setModified(bool);	// maybe we want to inform someone later about the change
    
    signals:
        void statusMessage(QString);	// Status message to display somewhere if possible
        void fileNameChanged(void);		// The filename changed
        void listModified(bool);			// list modified state changed
        void locoControllerStateChanged(Loco *, bool);
        void locoPropertyChanged(Loco *lc);
        void newLocoAdded(Loco *lc);  // new loco added to list
        void locoRemoved(Loco *lc);
    
    public slots:
        void slotEmergencyHalt(void);
        void slotLocoPropertyChanged(Loco *lc);
        
    private:
        QPtrList<Loco> list;	// our list with all known locos
        QString filename;			// name of last saved file
        
        // Maybe these did not belong exacly here, but.....
        unsigned int srcpbus;
        bool autoConnect;
        bool autoPowerOn;
        bool autoSendInit;
        bool readservice;
        bool modified;
        
        CommandPort *commandPort;
        InfoPort *infoPort;
    
    private slots:
        void slotControllerVisibleChanged(Loco *, bool);
        void slotLocoRemoved( Loco *lc);

};

#endif /* LOCO_LIST_H */
