/***************************************************************************
                              preferencesdlg.cpp
                             -------------------
    begin                : 2004-10-26
    copyright            : (C) 2004-2007 by Guido Scholz
    email                : guido.scholz@bayernline.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <qfiledialog.h>
#include "preferencesdlg.h"


PreferencesDialog::PreferencesDialog(QWidget* parent): QDialog(parent,
        "PreferencesDialog", true)
{
    setCaption(tr("Preferences"));

    QVBoxLayout* baseLayout = new QVBoxLayout(this, 10, 6);

    // message history group box
    QGroupBox* historyGB = new QGroupBox(2, Qt::Vertical,
            tr("Message history line"), this, "serverGB");
    baseLayout->addWidget(historyGB);

    // line for show time
    showMessageTimeCB = new QCheckBox(tr("Show time in &history message line"),
            historyGB, "showtimeCB");

    // line for translate server time
    translateServerTimeCB = new QCheckBox(tr("&Translate server time "
                "in SRCP 0.8 messages"), historyGB, "translateTimeCB");


    // file groupbox
    QGroupBox* ExtPreferencesGB = new QGroupBox(0, Horizontal,
                                     tr("Extended Preferences"), this,
                                     "ExtPreferencesGB");
    baseLayout->addWidget(ExtPreferencesGB);


    QVBoxLayout *extGBLayout =
        new QVBoxLayout(ExtPreferencesGB->layout(), 6);

    // line for autoload option
    QHBoxLayout* fileLayout = new QHBoxLayout(extGBLayout);
    autoloadCB = new QCheckBox(tr("&Autoload file on startup"),
                               ExtPreferencesGB, "autoloadB");
    connect(autoloadCB, SIGNAL(toggled(bool)),
            this, SLOT(slotAutoloadChanged(bool)));

    fileLayout->addWidget(autoloadCB);
    fileLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));
    filePB = new QPushButton(tr("&Choose..."), ExtPreferencesGB);
    fileLayout->addWidget(filePB);
    connect(filePB, SIGNAL(clicked()), this, SLOT(chooseFile()));

    // line for autoload filename
    fileLE = new QLineEdit(ExtPreferencesGB, "fileLE");
    fileLE->setMinimumWidth(180);
    extGBLayout->addWidget(fileLE);

    // line for browser option
    QHBoxLayout* browserLayout = new QHBoxLayout(extGBLayout);
    QLabel* browserLabel = new QLabel(tr("&Browser for online help"),
                              ExtPreferencesGB);
    browserLayout->addWidget(browserLabel);
    browserLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));

    browserCB = new QComboBox(false, ExtPreferencesGB);
    browserCB->insertItem("Firefox");   /*0 */
    browserCB->insertItem("Konqueror"); /*1 */
    browserCB->insertItem("Mozilla");   /*2 */
    browserCB->insertItem("Netscape");  /*3 */
    browserCB->insertItem("Opera");     /*4 */
    browserLayout->addWidget(browserCB);
    browserLabel->setBuddy(browserCB);


    // OK and Cancel buttons
    QHBoxLayout* buttonLayout = new QHBoxLayout(0, 0, 6);
    baseLayout->addLayout(buttonLayout);
    buttonLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));

    QPushButton* okPB = new QPushButton(tr("OK"), this);
    buttonLayout->addWidget(okPB);
    okPB->setDefault(true);
    connect(okPB, SIGNAL(clicked()), this, SLOT(accept()));

    QPushButton* cancelPB = new QPushButton(tr("Cancel"), this);
    buttonLayout->addWidget(cancelPB);
    connect(cancelPB, SIGNAL(clicked()), this, SLOT(reject()));
}


void PreferencesDialog::chooseFile()
{
    QString filename = QFileDialog::getOpenFileName(QDir::homeDirPath(),
                                                    tr("Loco sets") +
                                                    " (*" + DTCLFILEEXT +
                                                    ")", this,
                                                    "chooseFileDlg",
                                                    tr("Choose file"));
    if (filename != NULL)
        fileLE->setText(filename);
};


void PreferencesDialog::setHistoryData(bool showmessagetime,
                                      bool translateservertime)
{
    showMessageTimeCB->setChecked(showmessagetime);
    translateServerTimeCB->setChecked(translateservertime);
};


void PreferencesDialog::setFileData(bool autoload,
        const QString& filename, int browserno)
{
    autoloadCB->setChecked(autoload);
    fileLE->setText(filename);
    browserCB->setCurrentItem(browserno);
    slotAutoloadChanged(autoload);
};


bool PreferencesDialog::getShowMessageTime()
{
    return showMessageTimeCB->isChecked();
};


bool PreferencesDialog::getTimeTranslation()
{
    return translateServerTimeCB->isChecked();
};


bool PreferencesDialog::getAutoload()
{
    return autoloadCB->isChecked();
};


QString PreferencesDialog::getAutoloadFile()
{
    return fileLE->text();
};


int PreferencesDialog::getBrowserNr()
{
    return browserCB->currentItem();
};


void PreferencesDialog::slotAutoloadChanged(bool autoenabled)
{
    filePB->setEnabled(autoenabled);
    fileLE->setEnabled(autoenabled);
};

