SOURCES = \
	aboutdialog.cpp \
	CommandPort.cpp \
	InfoPort.cpp \
	listpropertiesdialog.cpp \
	Loco.cpp \
	LocoControl.cpp \
	lococontrollerwidget.cpp \
	LocoDialog.cpp \
	LocoFunction.cpp \
	LocoFunctionList.cpp \
	LocoList.cpp \
	locolistview.cpp \
	MainWindow.cpp \
	messagehistory.cpp \
	preferencesdlg.cpp \
	Programmer.cpp \
	serverinfodialog.cpp \
	srcpport.cpp

TRANSLATIONS = dtcltiny_de.ts

