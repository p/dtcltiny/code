/***************************************************************************
                               LocoDialog.h
                             -----------------
    begin                : 11.11.2000
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LOCODIALOG_H
#define LOCODIALOG_H

#include <qbuttongroup.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qdict.h>
#include <qfiledialog.h>
#include <qfont.h>
#include <qframe.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qlcdnumber.h>
#include <qlineedit.h>
//#include <qlist.h>
#include <qmessagebox.h>
#include <qpixmap.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qslider.h>
#include <qspinbox.h>
#include <qstring.h>
#include <qtabwidget.h>
#include <qtextstream.h>

#include "Resources.h"
#include "LocoControl.h"
#include "Loco.h"



class LocoControl;

class LocoDialog: public QDialog {
  Q_OBJECT
 
  public:
    LocoDialog(QWidget * parent = 0, Loco *lc = (Loco*)0,
               LocoControl * controller = 0);

    int exec();
 
  private:
    struct Protocol;            // forward-declaration
    
    void setupTabGeneral();     // sets up general tab
    void setupTabFuncs();       // sets up functional tab
    void setupTabSpeeds();      // sets up speed tab
    void setNewValues(int, int, int);   // sets the new Values for maxspeed etc.
    bool loadDecoderFile();     // loads decoder data from decoders.dat
    bool loadProtocolFile();
    QString getValue(QString str);      // extract values from lines in decoders.dat
    void printDescription(Protocol * protocol); // prints the description for the current protocol

  private slots:
    void slotApply();    // builds data string for new controller
    void slotDefault();         // sets general default values
    void slotUpdateIconField(const QString &);  // automatic transfer of entered text in "name"
    void slotGetIcon();         // gets the name of an icon
    void slotSetProtocolDefaults(const QString &);      // sets protocoll dependant default values
    void slotSetProtocols(int); // inserts the correct Protocols into the Listbox 
    void slotGetData();         // retrieves the current settings from a conroller   

  private:
  	struct Loco *loco;
    struct Decoder {
        Decoder(QString _name, QString _protocols) {
            name = _name;
            protocols = _protocols;
        } QString name;
        QString protocols;
    };
    QList <Decoder> *decoders;
    
    struct Protocol {
        Protocol(QString _name, int _steps, int _functions,
                 int _addressRange) {
            name = _name;
            steps = _steps;
            functions = _functions;
            addressRange = _addressRange;
        } QString name;
        int steps;
        int functions;
        int addressRange;
    };
    QDict <Protocol> *protocols;

    QLineEdit *leName;
    QLineEdit *leAlias;
    QSpinBox *spAddress;
    QLineEdit *busLE;
    QLabel *laPicture;
    QLineEdit *leRealMaxSpeed;
    QSlider *sliderMaxSpeed;
    QSlider *sliderAvgSpeed;
    QSlider *sliderMinSpeed;
    QSlider *sliderAccelTime;
    QSlider *sliderBreakTime;
    QCheckBox *cbFuncAvail[5];
    QComboBox *coboFuncType[5];
    QComboBox *coboFuncIcon[5];
    QComboBox *coboDecoder;
    QComboBox *coboProtocol;
//   QRadioButton *rbDirectionFwd;
//   QRadioButton *rbDirectionRev;
    QCheckBox *cbReverseDirection;
    QRadioButton *rbUnitml;
    QRadioButton *rbUnitkm;
    int maxAddress;
    int dlgType;
    LocoControl *contr;
    QTabWidget *tabs;
    QLabel *labelDescription;
    QString descriptionText;
};

#endif                          //LOCODIALOG_H
