<!DOCTYPE TS><TS>
<context>
    <name>AboutDialog</name>
    <message>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
    <message>
        <source>Authors</source>
        <translation>Autoren</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>SRCP client to control digital model trains. It is based
on &quot;DigitalTrainControl for Linux&quot; by Stefan Preis.</source>
        <translation>SRCP-Client zum Steuern von digitalen Modellbahnzügen.
Das Programm basiert auf &quot;DigitalTrainControl für Linux&quot; von Stefan Preis.</translation>
    </message>
</context>
<context>
    <name>ListPropertiesDialog</name>
    <message>
        <source>Locomotive list properties</source>
        <translation>Eigenschaften der Lokomotivliste</translation>
    </message>
    <message>
        <source>SRCP-Server</source>
        <translation>SRCP-Server</translation>
    </message>
    <message>
        <source>&amp;Hostname (IP or DNS)</source>
        <translation>&amp;Hostname (IP oder DNS)</translation>
    </message>
    <message>
        <source>&amp;Port (1-65535)</source>
        <translation>&amp;Port (1-65535)</translation>
    </message>
    <message>
        <source>B&amp;us (1-65535)</source>
        <translation>B&amp;us (1-65535)</translation>
    </message>
    <message>
        <source>&amp;Read port number from service database file</source>
        <translation>Portnummer aus Dienstdatenbankdatei lesen</translation>
    </message>
    <message>
        <source>Actions on file loading</source>
        <translation>Aktionen beim Laden der Datei</translation>
    </message>
    <message>
        <source>Autoconnect to &amp;server</source>
        <translation>Automatisch mit &amp;Server verbinden</translation>
    </message>
    <message>
        <source>Autostart layout &amp;voltage</source>
        <translation>&amp;Digitalspannung automatisch einschalten</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Autosend locomotive &amp;initialization</source>
        <translation>&amp;Lokomotivinitialisierung automatisch senden</translation>
    </message>
</context>
<context>
    <name>LocoControl</name>
    <message>
        <source>&amp;Properties...</source>
        <translation>&amp;Eigenschaften...</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Löschen</translation>
    </message>
    <message>
        <source>Change speed</source>
        <translation>Geschwindigkeit ändern</translation>
    </message>
    <message>
        <source>Brake to halt</source>
        <translation>Bremse zum Anhalten</translation>
    </message>
    <message>
        <source>Change speed to minimum</source>
        <translation>Minimale Geschwindigkeit ändern</translation>
    </message>
    <message>
        <source>Change speed to average</source>
        <translation>Durchschnittsgeschwindigkeit ändern</translation>
    </message>
    <message>
        <source>Change speed to maximum</source>
        <translation>Maximalgeschwindigkeit ändern</translation>
    </message>
    <message>
        <source>Emergency break for THIS loco</source>
        <translation>Nothalt für diese Lokomotive</translation>
    </message>
    <message>
        <source>Changes the direction of travel</source>
        <translation>Fahrtrichtung umschalten</translation>
    </message>
    <message>
        <source>Decoder address</source>
        <translation>Decoderadresse</translation>
    </message>
    <message>
        <source>Name (loco name)</source>
        <translation>Name (Lokomotive)</translation>
    </message>
    <message>
        <source>Alias (train name)</source>
        <translation>Alias (Zugbezeichnung)</translation>
    </message>
    <message>
        <source>Decoder speedstep</source>
        <translation>Decoder Geschwindigkeitsstufe</translation>
    </message>
    <message>
        <source>Real speed</source>
        <translation>Reale Geschwindigkeit</translation>
    </message>
    <message>
        <source>&amp;Hide</source>
        <translation>&amp;Verbergen</translation>
    </message>
</context>
<context>
    <name>LocoDialog</name>
    <message>
        <source>Add a new Loco</source>
        <translation>Neue Lok hinzufügen</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Voreinstellung</translation>
    </message>
    <message>
        <source>Change Loco Properties</source>
        <translation>Lokomotiveinstellungen ändern</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>&amp;Name (Loco):</source>
        <translation>&amp;Name (Lok):</translation>
    </message>
    <message>
        <source>&amp;Alias (Train):</source>
        <translation>&amp;Alias (Zug):</translation>
    </message>
    <message>
        <source>&amp;Icon:</source>
        <translation>&amp;Symbol:</translation>
    </message>
    <message>
        <source>Direction</source>
        <translation>Richtung</translation>
    </message>
    <message>
        <source>Reverse Logic (show forward, move backward)</source>
        <translation>Fahrtrichtungs-Logik umkehren</translation>
    </message>
    <message>
        <source>Logic</source>
        <translation>Logik</translation>
    </message>
    <message>
        <source>D&amp;ecoder:</source>
        <translation>D&amp;ecoder:</translation>
    </message>
    <message>
        <source>&amp;Protocol:</source>
        <translation>&amp;Protocol:</translation>
    </message>
    <message>
        <source>A&amp;ddress:</source>
        <translation>A&amp;dresse:</translation>
    </message>
    <message>
        <source>&amp;General</source>
        <translation>&amp;Allgemeines</translation>
    </message>
    <message>
        <source>Available:</source>
        <translation>Verfügbar:</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation>Typ:</translation>
    </message>
    <message>
        <source>Activate as:</source>
        <translation>Aktivieren als:</translation>
    </message>
    <message>
        <source>&amp;Functions</source>
        <translation>&amp;Funktionen</translation>
    </message>
    <message>
        <source>Static</source>
        <translation>Statisch</translation>
    </message>
    <message>
        <source>&amp;Maximum:</source>
        <translation>&amp;Maximum:</translation>
    </message>
    <message>
        <source>&amp;Average:</source>
        <translation>&amp;Durchschnitt:</translation>
    </message>
    <message>
        <source>M&amp;inimum:</source>
        <translation>M&amp;inimum:</translation>
    </message>
    <message>
        <source>Dynamic</source>
        <translation>Dynamisch</translation>
    </message>
    <message>
        <source>&amp;Speeds</source>
        <translation>&amp;Geschwindigkeit</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Address out of Range.
Please enter a correct value.</source>
        <translation>Adresse außerhalb des zulässigen Bereichs.
Bitte einen korrekten Wert eingeben.</translation>
    </message>
    <message>
        <source>Sorry, this protocol is not defined.
Please check the protocols and decoders files.</source>
        <translation>Dieses Protokoll ist ungültig.
Bitte kontrollieren Sie die Protokoll- und Decoder-Dateien.</translation>
    </message>
    <message>
        <source>Decoder not recognized.
Check to choose the right one
and then save your configuration.</source>
        <translation>Decoder wurde nicht erkannt.
Überprüfen Sie, ob Sie den richtigen gewählt haben
und speichern Sie dann Ihre Konfiguration</translation>
    </message>
    <message>
        <source>This protocol supports %d speed steps,
%d addresses and up to %d functions.</source>
        <translation>Dieses Protokoll unterstützt
%d Geschwindigkeitsstufen,
%d Adressen und bis zu %d Funktionen.</translation>
    </message>
    <message>
        <source>Sorry, this file doesn&apos;t exist.
%1</source>
        <translation>Diese Datei existiert nicht.
%1</translation>
    </message>
    <message>
        <source>Sorry, only files in directory %1 are valid.</source>
        <translation>Es sind nur Dateien im Verzeichnis %1 gültig.</translation>
    </message>
    <message>
        <source>&amp;Reset</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <source>Identification</source>
        <translation>Identifikation</translation>
    </message>
    <message>
        <source>&amp;Acceleration:</source>
        <translation>&amp;Beschleunigung:</translation>
    </message>
    <message>
        <source>&amp;Breaking:</source>
        <translation>Bre&amp;msen:</translation>
    </message>
    <message>
        <source>Bus:</source>
        <translation>Bus:</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; not found.</source>
        <translation>Datei &apos;%1&apos; wurde nicht gefunden.</translation>
    </message>
</context>
<context>
    <name>LocoList</name>
    <message>
        <source>Error, could not read file &apos;%1&apos;.</source>
        <translation>Fehler beim Lesen von Datei &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>There is not enough space to load all locos.
Loading is aborted.</source>
        <translation>Es ist nicht genügend Speicher für
            alle Locomotiven vorhanden.
        Laden der Datei wird abgebrochen.</translation>
    </message>
    <message>
        <source>File with %1 locos loaded: %2</source>
        <translation>Datei mit %1 Lokomotiven geladen: %2</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>Could not open %1 for writing.</source>
        <translation>Konnte %1 nicht zum Schreiben öffnen.</translation>
    </message>
    <message>
        <source>File %1 saved.</source>
        <translation>Datei %1 gespeichert.</translation>
    </message>
</context>
<context>
    <name>LocoListView</name>
    <message>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <source>Alias</source>
        <translation>Alias</translation>
    </message>
    <message>
        <source>Visible</source>
        <translation>Sichtbar</translation>
    </message>
    <message>
        <source> x</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <source>Save &amp;as...</source>
        <translation>Speichern &amp;unter...</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <source>&amp;Reset</source>
        <translation>&amp;Zurücksetzen</translation>
    </message>
    <message>
        <source>&amp;NMRA-Decoder...</source>
        <translation>&amp;NMRA-Decoder...</translation>
    </message>
    <message>
        <source>&amp;Uhlenbrock-Decoder...</source>
        <translation>&amp;Uhlenbrock-Decoder...</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <source>&amp;Daemon</source>
        <translation>Dae&amp;mon</translation>
    </message>
    <message>
        <source>&amp;Programmer</source>
        <translation>&amp;Programmierer</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>Über Qt</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Could not open %1 for writing.</source>
        <translation type="obsolete">Konnte %1 nicht zum Schreiben öffnen.</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <source>&amp;Preferences...</source>
        <translation>&amp;Einstellungen...</translation>
    </message>
    <message>
        <source>&amp;Options</source>
        <translation>&amp;Optionen</translation>
    </message>
    <message>
        <source>Loco sets</source>
        <translation>Loksätze</translation>
    </message>
    <message>
        <source>&amp;Toolbar</source>
        <translation>&amp;Symbolleiste</translation>
    </message>
    <message>
        <source>&amp;Statusbar</source>
        <translation>Status&amp;zeile</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <source>Open loco controller file</source>
        <translation>Loksteuerungsdatei öffnen</translation>
    </message>
    <message>
        <source>Save loco controller file</source>
        <translation>Loksteuerungsdatei speichern</translation>
    </message>
    <message>
        <source>Set power on</source>
        <translation>Digitalstrom einschalten</translation>
    </message>
    <message>
        <source>Set power off</source>
        <translation>Digitalstrom ausschalten</translation>
    </message>
    <message>
        <source>Emergency stop</source>
        <translation>Nothalt aktivieren</translation>
    </message>
    <message>
        <source>Emergency stop done</source>
        <translation>Nothalt ausgeführt</translation>
    </message>
    <message>
        <source>&amp;About dtcltiny...</source>
        <translation>Über &amp;dtcltiny...</translation>
    </message>
    <message>
        <source>About &amp;Qt...</source>
        <translation>Über &amp;Qt...</translation>
    </message>
    <message>
        <source>Error: Could not save configuration file: ~/%1</source>
        <translation>Fehler: Konfigurationsdatei ~/%1
        konnte nicht gespeichert werden.</translation>
    </message>
    <message>
        <source>Service name &apos;srcp&apos; not found in local service name database file.</source>
        <translation type="obsolete">Der Dienst &apos;srcp&apos; wurde nicht in der lokalen
        Dienstdatenbankdatei gefunden.</translation>
    </message>
    <message>
        <source>&amp;Toggle statusline</source>
        <translation>&amp;Statuszeile umschalten</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation>&amp;Verbinden</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation>Verbindung &amp;trennen</translation>
    </message>
    <message>
        <source>Shu&amp;tdown</source>
        <translation>&amp;Herunterfahren</translation>
    </message>
    <message>
        <source>&amp;Information...</source>
        <translation>&amp;Informationen...</translation>
    </message>
    <message>
        <source>&amp;Power on/off</source>
        <translation>&amp;Digitalstrom ein-/ausschalten</translation>
    </message>
    <message>
        <source>&amp;Emergency stop</source>
        <translation>&amp;Nothalt</translation>
    </message>
    <message>
        <source>Connect to SRCP server</source>
        <translation>Mit SRCP-Server verbinden</translation>
    </message>
    <message>
        <source>Disconnect SRCP server</source>
        <translation>Verbindung zum SRCP-Server trennen</translation>
    </message>
    <message>
        <source>Reset SRCP server</source>
        <translation>SRCP-Server zurücksetzen</translation>
    </message>
    <message>
        <source>Shutdown SRCP server</source>
        <translation>SRCP-Server herunterfahren</translation>
    </message>
    <message>
        <source>Show SRCP server information</source>
        <translation>Informationen zum verbundenen SRCP-Server anzeigen</translation>
    </message>
    <message>
        <source>There is not enough space to load all locos.
Loading is aborted.</source>
        <translation type="obsolete">Es ist nicht genügend Speicher für
            alle Locomotiven vorhanden.
        Laden der Datei wird abgebrochen.</translation>
    </message>
    <message>
        <source>File with %1 locos loaded: %2</source>
        <translation type="obsolete">Datei mit %1 Lokomotiven geladen: %2</translation>
    </message>
    <message>
        <source>Error, could not read file &apos;%1&apos;.</source>
        <translation type="obsolete">Fehler beim Lesen von Datei &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <source>&amp;Locomotive</source>
        <translation>&amp;Lokomotive</translation>
    </message>
    <message>
        <source>&amp;Add</source>
        <translation>&amp;Hinzufügen</translation>
    </message>
    <message>
        <source>&amp;Edit...</source>
        <translation>&amp;Ändern...</translation>
    </message>
    <message>
        <source>&amp;Duplicate</source>
        <translation>&amp;Duplizieren</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>&amp;Entfernen</translation>
    </message>
    <message>
        <source>List &amp;properties...</source>
        <translation>&amp;Einstellungen...</translation>
    </message>
    <message>
        <source>Add new loco controller</source>
        <translation>Neue Lokomotivsteuerung hinzufügen</translation>
    </message>
    <message>
        <source>New file created.</source>
        <translation>Neue Datei angelegt.</translation>
    </message>
    <message>
        <source>noname</source>
        <translation>Unbenannt</translation>
    </message>
    <message>
        <source>Unnamed file was changed.
Save changes?</source>
        <translation>Unbenannte Datei wurde verändert.
Änderungen speichern?</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; was changed.
Save changes?</source>
        <translation>Datei &apos;%1&apos; wurde geändert.
Ändrungen speichern?</translation>
    </message>
    <message>
        <source>Save changes</source>
        <translation>Änderungen speichern</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Ja</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nein</translation>
    </message>
    <message>
        <source>File %1 saved.</source>
        <translation type="obsolete">Datei %1 gespeichert.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; exists!
Do you want to overwrite it?</source>
        <translation>Datei &apos;%1&apos; existiert bereits!
Wollen Sie sie überschreiben?</translation>
    </message>
    <message>
        <source>Autoloader failed</source>
        <translation>Fehler beim automatischen Laden</translation>
    </message>
    <message>
        <source>The selected autoload file &apos;%1&apos;
does not exist. Please adjust your options.</source>
        <translation>Die zum automatischen Laden gewählte Datei &apos;%1&apos;
existiert nicht. Bitte korrigieren Sie die Einstellungen.</translation>
    </message>
    <message>
        <source>&amp;Now</source>
        <translation>&amp;Jetzt</translation>
    </message>
    <message>
        <source>&amp;Later</source>
        <translation>&amp;Später</translation>
    </message>
    <message>
        <source>Create new file</source>
        <translation>Neue Datei anlegen</translation>
    </message>
    <message>
        <source>Error: Could not open configuration file: ~/%1</source>
        <translation>Fehler: Konfigurationsdatei ~/%1 konnte nicht geöffnet werden</translation>
    </message>
</context>
<context>
    <name>MessageHistory</name>
    <message>
        <source>Hints</source>
        <translation>Hinweise</translation>
    </message>
    <message>
        <source>Commands</source>
        <translation>Kommandos</translation>
    </message>
    <message>
        <source>Infoport</source>
        <translation>Infoport</translation>
    </message>
    <message>
        <source>Feedbacks</source>
        <translation>Rückmeldungen</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Extended Preferences</source>
        <translation>Weitere Einstellungen</translation>
    </message>
    <message>
        <source>&amp;Autoload file on startup</source>
        <translation>&amp;Datei automatisch beim Programmstart laden</translation>
    </message>
    <message>
        <source>&amp;Choose...</source>
        <translation>&amp;Wählen...</translation>
    </message>
    <message>
        <source>&amp;Browser for online help</source>
        <translation>&amp;Browser für Online-Hilfe</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Loco sets</source>
        <translation>Loksätze</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation>Datei wählen</translation>
    </message>
    <message>
        <source>Show time in &amp;history message line</source>
        <translation>&amp;Zeit in Statuszeile anzeigen</translation>
    </message>
    <message>
        <source>&amp;Translate server time in SRCP 0.8 messages</source>
        <translation>Serverzeit bei SRCP 0.8-&amp;Meldungen lesbar
            darstellen</translation>
    </message>
    <message>
        <source>Message history line</source>
        <translation>Statuszeile mit Meldungshistorie</translation>
    </message>
</context>
<context>
    <name>Programmer</name>
    <message>
        <source>Programmer for NMRA-Decoders</source>
        <translation>Programmierer für NMRA-Decoder</translation>
    </message>
    <message>
        <source>Programmer for Uhlenbrock-Decoders</source>
        <translation>Programmierer für Uhlenbrock-Decoder</translation>
    </message>
    <message>
        <source>&amp;Value:</source>
        <translation>&amp;Wert:</translation>
    </message>
    <message>
        <source>&amp;Address:</source>
        <translation>&amp;Adresse:</translation>
    </message>
    <message>
        <source>Programm</source>
        <translation>Programmieren</translation>
    </message>
    <message>
        <source>&amp;Register #:</source>
        <translation>&amp;Registernr.:</translation>
    </message>
    <message>
        <source>Programming completed</source>
        <translation>Programmierung vollständig</translation>
    </message>
    <message>
        <source>Action</source>
        <translation>Aktion</translation>
    </message>
    <message>
        <source>&amp;Set a CV</source>
        <translation>CV &amp;setzen</translation>
    </message>
    <message>
        <source>Set &amp;one bit of a CV</source>
        <translation>&amp;Ein Bit einer CV setzen</translation>
    </message>
    <message>
        <source>Set a re&amp;gister</source>
        <translation>Ein Re&amp;gister setzen</translation>
    </message>
    <message>
        <source>Set Decoder to &amp;NMRA only</source>
        <translation>Decoder auf nur &amp;NMRA setzen</translation>
    </message>
    <message>
        <source>Parameters</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <source>&amp;CV #:</source>
        <translation>&amp;CV Nr.:</translation>
    </message>
    <message>
        <source>&amp;Bit #:</source>
        <translation>&amp;Bit Nr.:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>ServerInfoDialog</name>
    <message>
        <source>SRCP server information</source>
        <translation>SRCP-Server Information</translation>
    </message>
    <message>
        <source>Command session</source>
        <translation>Kommandositzung</translation>
    </message>
    <message>
        <source>Server:</source>
        <translation>Server:</translation>
    </message>
    <message>
        <source>SRCP version:</source>
        <translation>SRCP-Version:</translation>
    </message>
    <message>
        <source>Other SRCP version:</source>
        <translation>Alternative SRCP-Version:</translation>
    </message>
    <message>
        <source>SRCP session id:</source>
        <translation>Sitzungsidentifikationsnummer:</translation>
    </message>
    <message>
        <source>Info session</source>
        <translation>Info-Sitzung</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>SrcpPort</name>
    <message>
        <source>%1: Communication error, no SRCP version found.</source>
        <translation>%1: Kommunikationsfehler, keine SRCP-Version gefunden</translation>
    </message>
    <message>
        <source>%1: Communication error, wrong SRCP version &apos;%2&apos;.</source>
        <translation>1%: Kommunikationsfehler, falsche SRCP-Version &apos;%2&apos;.</translation>
    </message>
    <message>
        <source>connection refused</source>
        <translation>Verbindung verweigert</translation>
    </message>
    <message>
        <source>host not found</source>
        <translation>Host nicht gefunden</translation>
    </message>
    <message>
        <source>socket read error</source>
        <translation>Socketlesefehler</translation>
    </message>
    <message>
        <source>%1: Socket closed.</source>
        <translation>%1: Socket geschlossen.</translation>
    </message>
    <message>
        <source>%1: Parse error, parameter list too long &apos;%2&apos;.</source>
        <translation>1%: Fehler beim Auswerten, Parameterliste zu lang
            &apos;%1&apos;.</translation>
    </message>
    <message>
        <source>%1: Parse error, parameter list too short &apos;%2&apos;.</source>
        <translation>%1: Fehler beim Auswerten, Parameterliste ist zu kurz &apos;%2&apos;.</translation>
    </message>
    <message>
        <source>%1: Communication error CONNECTIONMODE &apos;%2&apos;</source>
        <translation>1%: Kommunikationsfehler CONNECTIONMODE &apos;%2&apos;</translation>
    </message>
    <message>
        <source>%1: Communication error GO &apos;%2&apos;</source>
        <translation>1%: Kommunikationsfehler GO &apos;%2&apos;</translation>
    </message>
    <message>
        <source>%1: Error, wrong SRCP state: %2</source>
        <translation>1%: Fehler, falscher SRCP-Status: %2</translation>
    </message>
    <message>
        <source>%1: Socket connected to host &apos;%2&apos; on port &apos;%3&apos;</source>
        <translation>1%: Socket mit Host &apos;%2&apos; auf Port &apos;%3&apos; verbunden</translation>
    </message>
    <message>
        <source>%1: Host &apos;%2&apos; found</source>
        <translation>%1: Host &apos;%2&apos; gefunden</translation>
    </message>
    <message>
        <source>%1: Socket closed by foreign host.</source>
        <translation>1%: Socket durch Server geschlossen.</translation>
    </message>
    <message>
        <source>%1: Socket error %2 occured (%3).</source>
        <translation>%1: Socketfehler %2 ist aufgetreten (%3)</translation>
    </message>
    <message>
        <source>%1: Communication error PROTOCOL &apos;%2&apos;</source>
        <translation>1%: Kommunikationsfehler PROTOCOL &apos;%2&apos;</translation>
    </message>
</context>
</TS>
