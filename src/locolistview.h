//
// C++ Interface: locolistview
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef LOCO_LIST_VIEW_H
#define LOCO_LIST_VIEW_H

#include <qlistview.h>
#include <qvbox.h>
#include <qwidget.h>

#include "Loco.h"
#include "LocoList.h"

class LocoItem;

class LocoListView : public QWidget
{
    Q_OBJECT
    public:
        LocoListView(QWidget *parent, const char *name, LocoList *list);
        ~LocoListView();
        void update(void);
        void clear(void);
        LocoItem *findLoco(Loco *lc);
        
    public slots:
        void slotLocoCreated(Loco *lc);
        void slotLocoDeleted(Loco *lc);
        void slotLocoPropertyChanged(Loco *lc);
        void slotLocoVisibleChanged(Loco *lc, bool state);
        void clickedLocoItem(QListViewItem *);
        void doubleClickedLocoItem( QListViewItem *, const QPoint &, int );
        
    private:
        LocoList *list;
        QVBox *vBox;
        QListView *locoListView;
};

class LocoItem : public QCheckListItem
{
    public:
        LocoItem(QListView *parent, Loco *lc);
        ~LocoItem();
        bool isMyLoco(Loco *lc);
        void update(void);
        void setVisible(bool);
        
    private:
        Loco *loco; // Pointer to the related loco containing all Data
        
};

#endif /* LOCO_LIST_VIEW_H */
