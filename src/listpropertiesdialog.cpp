/*
 * listpropertiesdialog.cpp
 * ------------------------
 * Begin    : 2004-08-27
 * Copyright: (C) 2007 by Guido Scholz
 * email    : guido.scholz@bayernline.de
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <netdb.h>

#include "listpropertiesdialog.h"


ListPropertiesDialog::ListPropertiesDialog(QWidget* parent): QDialog(parent,
        "ListPropertiesDialog", true)
{
    setCaption(tr("Locomotive list properties"));

    QVBoxLayout* baseLayout = new QVBoxLayout(this, 10, 6);

    QGroupBox* serverGB = new QGroupBox(0, Qt::Horizontal, tr("SRCP-Server"),
            this, "serverGB");
    baseLayout->addWidget(serverGB);

    QVBoxLayout *serverGBL = new QVBoxLayout(serverGB->layout(), 6);

    // line for hostname
    QHBoxLayout* hostLayout = new QHBoxLayout(serverGBL);
    QLabel* hostLabel = new QLabel(tr("&Hostname (IP or DNS)"),
                           serverGB, "hostLabel");
    hostLayout->addWidget(hostLabel);
    hostLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));
    hostLE = new QLineEdit(serverGB, "hostLE");
    hostLE->setMinimumWidth(160);
    hostLayout->addWidget(hostLE);
    hostLabel->setBuddy(hostLE);

    // line for port number
    QHBoxLayout *portLayout = new QHBoxLayout(serverGBL);
    portLabel = new QLabel(tr("&Port (1-65535)"), serverGB);
    portLayout->addWidget(portLabel);
    portLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));
    portLE = new QLineEdit(serverGB, "portLE");
    portLE->setMinimumWidth(160);
    portLayout->addWidget(portLE);
    portLabel->setBuddy(portLE);

    // line for bus number
    QHBoxLayout *busLayout = new QHBoxLayout(serverGBL);
    QLabel* busLabel = new QLabel(tr("B&us (1-65535)"), serverGB);
    busLayout->addWidget(busLabel);
    busLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));
    busLE = new QLineEdit(serverGB, "busLE");
    busLE->setMinimumWidth(160);
    busLayout->addWidget(busLE);
    busLabel->setBuddy(busLE);

    // line for read service checkbox
    readserviceCB = new QCheckBox(tr("&Read port number from service "
                "database file"), serverGB, "readserviceCB");
    serverGBL->addWidget(readserviceCB);
    connect(readserviceCB, SIGNAL(toggled(bool)),
            this, SLOT(slotReadServiceChanged(bool)));


    // file loading action groupbox
    QGroupBox* actionGB = new QGroupBox(3, Qt::Vertical,
            tr("Actions on file loading"), this, "actionGB");
    baseLayout->addWidget(actionGB);

    // line for autologin checkbox
    autoconnectCB = new QCheckBox(tr("Autoconnect to &server"),
                                actionGB, "autoconnectCB");
    connect(autoconnectCB, SIGNAL(stateChanged(int)), this,
            SLOT(autologinChanged(int)));

    // line for autopoweron checkbox
    autopoweronCB = new QCheckBox(tr("Autostart layout &voltage"),
                                actionGB, "autopoweronCB");
    connect(autopoweronCB, SIGNAL(stateChanged(int)), this,
                        SLOT(autopowerChanged(int)));

    // line for autopoweron checkbox
    autosendCB = new QCheckBox(tr("Autosend locomotive &initialization"),
                                actionGB, "autosendCB");



    // OK and Cancel buttons
    QHBoxLayout* buttonLayout = new QHBoxLayout(0, 0, 6);
    baseLayout->addLayout(buttonLayout);
    buttonLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                QSizePolicy::Minimum));

    QPushButton* okPB = new QPushButton(tr("OK"), this);
    buttonLayout->addWidget(okPB);
    okPB->setDefault(true);
    connect(okPB, SIGNAL(clicked()), this, SLOT(accept()));

    QPushButton* cancelPB = new QPushButton(tr("Cancel"), this);
    buttonLayout->addWidget(cancelPB);
    connect(cancelPB, SIGNAL(clicked()), this, SLOT(reject()));
}


void ListPropertiesDialog::setServerData(const QString& hostname,
        unsigned int portnumber, unsigned int bus)
{
    hostLE->setText(hostname);
    portLE->setText(QString::number(portnumber));
    busLE->setText(QString::number(bus));
};


void ListPropertiesDialog::setActionData(bool autoconnect,
        bool autopoweron, bool autosend)
{
    autoconnectCB->setChecked(autoconnect);
    if (!autoconnect)
         autologinChanged(QButton::Off);

    autopoweronCB->setChecked(autopoweron);
    if (!autopoweron)
        autopowerChanged(QButton::Off);
    
    autosendCB->setChecked(autosend);
};


QString ListPropertiesDialog::getHostname()
{
    return hostLE->text();
};


unsigned int ListPropertiesDialog::getPort()
{
    return portLE->text().toUInt();
};


unsigned int ListPropertiesDialog::getBus()
{
    return busLE->text().toUInt();
};


bool ListPropertiesDialog::getAutoConnect()
{
    return autoconnectCB->isChecked();
};


bool ListPropertiesDialog::getAutoPowerOn()
{
    return autopoweronCB->isChecked();
};


bool ListPropertiesDialog::getAutoSend()
{
    return autosendCB->isChecked();
};


void ListPropertiesDialog::slotReadServiceChanged(bool read)
{
    if (read) {
        struct servent *serviceentry;
        unsigned int port = 4303;

        serviceentry = getservbyname("srcp","tcp");

        if (serviceentry == NULL) {
            /*TODO: show error message
              tr("Service name 'srcp' not found in\n"
              "local service name database file.")
              */
        }
        else 
            port = ntohs(serviceentry->s_port);

        portLE->setText(QString::number(port));
    }
}

/* enable/disable autopower option depending on autologin state */
void ListPropertiesDialog::autologinChanged(int state)
{
    if (state == QButton::On)
        autopoweronCB->setEnabled(true);
    else {
        autopoweronCB->setChecked(false);
        autopoweronCB->setEnabled(false);
        autosendCB->setChecked(false);
        autosendCB->setEnabled(false);
    }
}   

/* enable/disable autosendall option depending on autopower state */
void ListPropertiesDialog::autopowerChanged(int state)
{
    if (state == QButton::On)
        autosendCB->setEnabled(true);
    else {
        autosendCB->setChecked(false);
        autosendCB->setEnabled(false);
    }
}

