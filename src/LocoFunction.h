//
// C++ Interface: LocoFunction
//
// Description: 
//
//
// Author: dtcltiny group <dtcltiny-devel@lists.sourceforge.net>, (C) 2007
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef LOCO_FUNCTION_H
#define LOCO_FUNCTION_H

#include <qfile.h>
#include <qobject.h>
#include <qstring.h>

class LocoFunction : public QObject
{
    Q_OBJECT
    public:
        LocoFunction(QObject *parent = 0, const char *name = NULL);
        ~LocoFunction();
        bool load(QFile &file);
        bool save(QFile &file);
        QString findFunc(QString &constant) const;	// gets the translation
    
    private:
        QString funcConstant;			// a constant Text representing the function
        QString funcTranslation;	// the Human Translated String of the function
        QString funcPic;					// Picture of the function
};

#endif /* LOCO_FUNCTION_H */
