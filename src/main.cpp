/***************************************************************************
                               main.cpp
                             ------------
    Begin                : 11.11.2000
    Copyright            : (C) 2000 by Markus Pfeiffer
                         : (C) 2007 by Guido Scholz
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <qapplication.h>
#include <qtextcodec.h>

#include "MainWindow.h"



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // translation file for Qt library
    QTranslator qt(0);

#ifdef QT_TRANSLATIONS_DIR
    if (qt.load(QString("qt_") + QTextCodec::locale(),
                QT_TRANSLATIONS_DIR))
        a.installTranslator(&qt);
    else
        fprintf(stderr, "No Qt translation for %s found.",
                QTextCodec::locale());
#endif

    // translation file for application strings
    QTranslator dtcltinyTr(0);
    if (dtcltinyTr.load(QString("dtcltiny_") + QTextCodec::locale(),
                        RESOURCE_DIR))
        a.installTranslator(&dtcltinyTr);
    else
        fprintf(stderr, "No dtcltiny translation for %s in %s found.",
                QTextCodec::locale(), RESOURCE_DIR);
    MainWindow *myMainWindow = new MainWindow();
    Q_CHECK_PTR(myMainWindow);
    myMainWindow->resize(640, 485);

    a.setMainWidget(myMainWindow);
    myMainWindow->show();

    /*only the first application window autoloads a file*/
    int ac = qApp->argc();

    if (ac == 1)
        myMainWindow->readAutoloadFile();
    else {
        myMainWindow->openFile(qApp->argv()[1]);
        // loop over all arguments -> open more application windows
        if (ac > 2)
            for (int i = 2; i < ac; i++)
                myMainWindow->openFileWindow(qApp->argv()[i]);
    }

    return a.exec();
}
