/***************************************************************************
                               LocoControl.cpp
                             -------------------
    begin                : 11.11.2000
    last update		 : 2004-10-30
    copyright            : (C) 2000 by Markus Pfeiffer
    email                : mail@markus-pfeiffer.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "LocoControl.h"
#include "MainWindow.h"

#include "pixmaps/brake.xpm"
#include "pixmaps/emergency.xpm"

/*
 *constructor to create a new loco with default values
 */
LocoControl::LocoControl(QWidget *parent, const char *name, Loco *lc):
    QWidget(parent, name)
{
    loco = lc;
    initLayout();
    
    loco->sendLocoInit();
    updateDisplay();
    slotUpdateProperties(loco);
    slotUpdateSpeed(loco->getLocoSpeed());
    
    connect(loco, SIGNAL(controllerVisibleChanged(Loco *, bool)), 
            this, SLOT(slotControllerVisibleChanged(Loco *, bool)));
    connect(loco, SIGNAL(valueChanged(void)),
            this, SLOT(slotUpdate(void)));
    connect(loco, SIGNAL(propertyChanged(Loco *)),
            this, SLOT(slotUpdateProperties(Loco *)));
    connect(loco, SIGNAL(editProperties()), 
            this, SLOT(slotProperties()));
}

LocoControl::~LocoControl()
{
    disconnect(this, 0 ,0, 0);
}


void LocoControl::initLayout()
{
    setFixedSize(C_WIDTH_, C_HEIGHT_);

    contextmenu = new QPopupMenu(0, "contextmenu");
    contextmenu->insertItem(tr("&Properties..."), this,
                            SLOT(slotProperties()));
    contextmenu->insertItem(tr("&Hide"), this, SLOT(slotHide()));
    contextmenu->insertSeparator(-1);
    contextmenu->insertItem(tr("&Delete"), this, SLOT(slotDelete()));

    frameController = new QFrame(this, "frameController");
    frameController->resize(C_WIDTH_, C_HEIGHT_);
    frameController->move(0, 0);
    frameController->setFrameStyle(QFrame::Panel | QFrame::Raised);
    frameController->setLineWidth(1);

    frameSpeed = new QFrame(this, "frameSpeed");
    frameSpeed->resize(80, 279);
    frameSpeed->move(C_MID_ - frameSpeed->width() / 2, C_Y_START_ / 2);
    frameSpeed->setFrameStyle(QFrame::Box | QFrame::Sunken);

    sliderSpeed = new QSlider(QSlider::Vertical, this, "sliderSpeed");
    sliderSpeed->resize(20, 200);
    sliderSpeed->move(15, C_Y_START_);
    sliderSpeed->setRange(0, loco->getLocoMaxSpeed());
    sliderSpeed->setTickmarks(QSlider::Left);
    sliderSpeed->setTickInterval(loco->getLocoMaxSpeed() / 10);

    
    // accepts both keyboard and mouseclicks to activate
    sliderSpeed->setFocusPolicy(QWidget::StrongFocus);
    QToolTip::add(sliderSpeed, tr("Change speed"));
    
    updateSpeedSliderSettings();
    
    connect(sliderSpeed, SIGNAL(valueChanged(int)),
            SLOT(slotUpdateSpeed(int)));
    //sliderSpeed->setFocus();

    QPushButton *m1, *m2, *m3;
    m_group = new QButtonGroup(this, "m_group");
    m_group->hide();
    float fFaktor = 0.97 * sliderSpeed->height() / (float) loco->getLocoMaxSpeed();

    buttBreak = new QPushButton(this, "buttBreak");
    buttBreak->setPixmap(QPixmap(brake));
    buttBreak->resize(33, 22);
    buttBreak->move(15, C_Y_START_ + sliderSpeed->height() + 2);
    buttBreak->setFocusPolicy(QWidget::NoFocus);
    QToolTip::add(buttBreak, tr("Brake to halt"));
    m_group->insert(buttBreak);

    // colored speed bars
    m1 = new QPushButton(this, "m1");
    m1->setBackgroundMode(PaletteBackground);
    m1->setPalette(QPalette(QColor(0, 128, 0)));
    m1->resize(9, (int) (fFaktor * loco->getLocoMinSpeed()));
    m1->move(38, C_Y_START_ + sliderSpeed->height() - m1->height() - 4);
    m1->setFocusPolicy(QWidget::NoFocus);
    QToolTip::add(m1, tr("Change speed to minimum"));
    m_group->insert(m1);

    m2 = new QPushButton(this, "m2");
    m2->setBackgroundMode(PaletteBackground);
    m2->setPalette(QPalette(QColor(255, 234, 0)));
    m2->resize(9, (int) (fFaktor * (loco->getLocoAvgSpeed() - loco->getLocoMinSpeed())));
    m2->move(38,
             C_Y_START_ + sliderSpeed->height() - m1->height() -
             m2->height() - 4);
    m2->setFocusPolicy(QWidget::NoFocus);
    QToolTip::add(m2, tr("Change speed to average"));
    m_group->insert(m2);

    m3 = new QPushButton(this, "m3");
    m3->setBackgroundMode(PaletteBackground);
    m3->setPalette(QPalette(QColor(255, 0, 0)));
    m3->resize(9, (int) (fFaktor * (loco->getLocoMaxSpeed() - loco->getLocoAvgSpeed())));
    m3->move(38,
             C_Y_START_ + sliderSpeed->height() - m1->height() -
             m2->height() - m3->height() - 4);
    m3->setFocusPolicy(QWidget::NoFocus);
    QToolTip::add(m3, tr("Change speed to maximum"));
    m_group->insert(m3);
    connect(m_group, SIGNAL(clicked(int)), this, SLOT(slotM_GroupClicked(int)));

    pixEmergency = QPixmap(emergency);
    buttEmergency = new QPushButton(this, "buttEmergency");
    buttEmergency->setPixmap(pixEmergency);
    buttEmergency->resize(33, 46);
    buttEmergency->move(52, C_Y_START_ + sliderSpeed->height() + 2);
    //  buttEmergency->setEnabled( iLocoSpeed != 0 );
    buttEmergency->setFocusPolicy(QWidget::NoFocus);
    QToolTip::add(buttEmergency, tr("Emergency break for THIS loco"));
    connect(buttEmergency, SIGNAL(clicked()), SLOT(slotEmergencyHalt()));

		pixDirection = loco->getDirectionPix();
		
    buttDirection = new QPushButton(this, "buttDirection");
    buttDirection->setPixmap(pixDirection);
    buttDirection->setFocusPolicy(QWidget::NoFocus);
    buttDirection->resize(33, 22);
    buttDirection->move(15, C_Y_START_ + sliderSpeed->height() + 26);
    QToolTip::add(buttDirection, tr("Changes the direction of travel"));
    connect(buttDirection, SIGNAL(clicked()),
            SLOT(slotReverseDirection()));

    buttSpeed = new QPushButton(this, "buttSpeed");
    buttSpeed->resize(70, 20);
    buttSpeed->setFont(QFont(my_font_, 12, QFont::Bold));
    buttSpeed->move(C_MID_ - buttSpeed->width() / 2,
                    C_Y_START_ + sliderSpeed->height() +
                    buttEmergency->height() + 6);
    buttSpeed->setFocusPolicy(QWidget::NoFocus);
//  updateSpeedButtonText();
    connect(buttSpeed, SIGNAL(clicked()), SLOT(slotToggleSpeedDisplay()));

    groupFuncs = new QButtonGroup(this, "groupFuncs");
    groupFuncs->resize(32, 200);
    groupFuncs->move(53, 7);
    groupFuncs->setFrameStyle(QFrame::NoFrame);

    for (int i = 0; i <= 4; i++) 
    {
        buttF[i] = new QPushButton(groupFuncs, "buttFn");
        //groupFuncs->insert(buttF[i]);       // insert for correct ID order
        QString prefix;
        prefix.sprintf("%1d", loco->getLocoFuncState(i));
        pixFuncs = QPixmap(PIX_DIR + prefix + loco->getLocoFuncIcon(i));
        buttF[i]->setPixmap(pixFuncs);
        buttF[i]->resize(30, 30);
        buttF[i]->setToggleButton(loco->getLocoFuncType(i));
        buttF[i]->setOn(loco->getLocoFuncState(i));
        buttF[i]->setFocusPolicy(QWidget::NoFocus);
        QString FuncToolTip;
        FuncToolTip.sprintf("F%1d", i);
        QToolTip::add(buttF[i], FuncToolTip);
        if (loco->getLocoFuncAvail(i) == false)
            buttF[i]->hide();
    }
    buttF[0]->move(2, 2);
    buttF[1]->move(2, 30 + 2 + 5);
    buttF[2]->move(2, 60 + 2 + 5 * 2);
    buttF[3]->move(2, 90 + 2 + 5 * 3);
    buttF[4]->move(2, 120 + 2 + 5 * 4);

    connect(groupFuncs, SIGNAL(clicked(int)),
            SLOT(slotUpdateFuncsSwitched(int)));
    connect(groupFuncs, SIGNAL(pressed(int)),
            SLOT(slotUpdateFuncsTouched(int)));
    connect(groupFuncs, SIGNAL(released(int)),
            SLOT(slotUpdateFuncsTouched(int)));

    frameLoco = new QVBox(this, "frameLoco");
    frameLoco->resize(80, 110);
    frameLoco->setMargin(3);
    frameLoco->move(C_MID_ - frameLoco->width() / 2,
                    C_Y_START_ +
                    frameSpeed->height() /* +groupFuncs->height()+5 */ );
    frameLoco->setFrameStyle(QFrame::Box | QFrame::Sunken);

    labelAddress = new QLabel(QString::number(loco->getAddress()), frameLoco,
            "labelAddress");
    labelAddress->setFont(QFont(my_font_, 20, QFont::Bold));
    labelAddress->setAlignment(AlignCenter);
    labelAddress->setFrameStyle(QFrame::NoFrame);
    QToolTip::add(labelAddress, tr("Decoder address"));

    pixPicture.load(loco->getLocoPicture());
    // Filename im Klartext in loco.dat ohne .xpm !!
    labelPicture = new QLabel(frameLoco, "labelPicture");
    labelPicture->setPixmap(pixPicture);
    labelPicture->setAutoResize(true);
    labelPicture->setAlignment(AlignCenter);

    buttName = new QPushButton(frameLoco, "buttName");

    // toggle between name and alias
    if (loco->getLocoShow() == NAME) {
        buttName->setText(loco->getLocoName());
        QToolTip::add(buttName, tr("Name (loco name)"));
    }
    else if (loco->getLocoShow() == ALIAS) {
        buttName->setText(loco->getLocoAlias());
        QToolTip::add(buttName, tr("Alias (train name)"));
    }

    buttName->setFont(QFont(my_font_, 12, QFont::Bold));
    //buttName->resize(70, 30);
    buttName->setMinimumWidth(70);
    buttName->setMinimumHeight(30);
    buttName->setFocusPolicy(QWidget::NoFocus);
    connect(buttName, SIGNAL(clicked()), SLOT(slotToggleNameAlias()));
}

void LocoControl::updateSpeedSliderSettings()
{
    sliderSpeed->setRange(0, loco->getLocoMaxSpeed());
    sliderSpeed->setTickInterval(loco->getLocoMaxSpeed() / 10);
    updateSliderValue(loco->getLocoSpeed());
    
    sliderSpeed->setTickInterval(loco->getLocoMaxSpeed() / 10);
    if (loco->getLocoMaxSpeed() >= 28)
        sliderSpeed->setSteps(1, loco->getLocoMaxSpeed() / 14);
    else
        sliderSpeed->setSteps(1, 2);
}

void LocoControl::slotToggleNameAlias()
{
    loco->setLocoShow(!loco->getLocoShow());
    updateNameDisplay();
}

void LocoControl::updateNameDisplay()
{
    if (loco->getLocoShow() == NAME) 
    {
        buttName->setText(loco->getLocoName());
        QToolTip::add(buttName, tr("Name (loco name)"));
    }
    else if (loco->getLocoShow() == ALIAS) 
    {
        buttName->setText(loco->getLocoAlias());
        QToolTip::add(buttName, tr("Alias (train name)"));
    }
}

void LocoControl::slotToggleSpeedDisplay()
{
    QString speedText;

    loco->setLocoShowSpeed(!loco->getLocoShowSpeed());
    updateSpeedButtonText();
}

void LocoControl::slotEmergencyHalt()
{
    loco->setEmergencyHalt();
    // call of setValue is enough to set speed to zero
    updateSliderValue(loco->getLocoSpeed());
}

// kann man diese zwei noch zusammenfassen?
void LocoControl::slotUpdateFuncsTouched(int iFuncID)
{
    if (loco->getLocoFuncType(iFuncID) == PUSHBUTTON) 
    {
        loco->setFunctionState(iFuncID, !loco->getLocoFuncState(iFuncID));
        pixFuncs = loco->getFunctionPix( iFuncID );
        buttF[iFuncID]->setPixmap(pixFuncs);
    }
}


void LocoControl::slotUpdateFuncsSwitched(int iFuncID)
{
    if (loco->getLocoFuncType(iFuncID) == SWITCH) 
    {
        loco->setFunctionState(iFuncID, !loco->getLocoFuncState(iFuncID));
        pixFuncs = loco->getFunctionPix( iFuncID );
        buttF[iFuncID]->setPixmap(pixFuncs);
    }
}

/*
 * slider changes trigger this method to set new speed
 */
void LocoControl::slotUpdateSpeed(int iNewSpeed)
{
    qWarning("LocoControl::slotUpdateSpeed()");
    loco->setSpeed(getSliderValue());

    // disable direction change button if speed != 0
    buttDirection->setEnabled(loco->getLocoDirectionFwd() == 0);

    updateSpeedButtonText();
    updateDirectionEnable();
}

void LocoControl::slotReverseDirection()
{
    loco->setDirectionFwd(!loco->getLocoDirectionFwd());
    updateDirectionPix();
}

void LocoControl::updateDirectionPix()
{
    buttDirection->setPixmap(loco->getDirectionPix());
}

void LocoControl::updateSpeedButtonText()
{
    QString speedText;

    if (loco->getLocoShowSpeed() == STEPS) 
    {
        speedText.sprintf("%02d", loco->getLocoSpeed());
        QToolTip::add(buttSpeed, tr("Decoder speedstep"));
    }
    else if (loco->getLocoShowSpeed() == REAL) 
    {
        speedText.sprintf("%3.0f %s", loco->getLocoSpeed() * loco->getLocoSpeedFactor(),
                          loco->getLocoSpeedUnit().data());
        QToolTip::add(buttSpeed, tr("Real speed"));
    }
    buttSpeed->setText(speedText);
}


void LocoControl::slotProperties()
{
    loco->setSpeed( 0 );	// Stop Loco
    updateSliderValue(loco->getLocoSpeed());       // first stop the Loco
    slotUpdateSpeed(loco->getLocoSpeed());
    LocoDialog *editLoco = new LocoDialog(this, loco, this);

    // refresh view if settings were changed
    if (editLoco->exec() == QDialog::Accepted) 
    {
        updateNameDisplay();

        pixPicture.load(loco->getLocoPicture());
        labelPicture->setPixmap(pixPicture);
        labelAddress->setText(QString::number(loco->getAddress()));

        for (int i = 0; i <= 4; i++) 
        {
            if (loco->getLocoFuncAvail(i))
                buttF[i]->show();
            else 
            {
                buttF[i]->hide();
                if (loco->getLocoFuncState(i))
                    buttF[i]->toggle(); // Unbenutzte Funktionen abschalten
                loco->setFunctionState(i,FALSE);
            }
            if (loco->getLocoFuncType(i) == PUSHBUTTON) 
            {
                // Pushbutton has to be off by default
                if (loco->getLocoFuncState(i))
                    buttF[i]->toggle(); // Unbenutzte Funktionen abschalten
                loco->setFunctionState(i,FALSE);
            }
            buttF[i]->setToggleButton(loco->getLocoFuncType(i));
            buttF[i]->setPixmap(loco->getFunctionPix(i));
        }

        updateSpeedSliderSettings();
        
        QButton *m1, *m2, *m3;
        m1 = m_group->find(1);
        m2 = m_group->find(2);
        m3 = m_group->find(3);

        float fFaktor = 0.97 * sliderSpeed->height() / (float) loco->getLocoMaxSpeed();
        m1->resize(8, (int) (fFaktor * loco->getLocoMinSpeed()));
        m1->move(37,
                 C_Y_START_ + sliderSpeed->height() - m1->height() - 4);
        m2->resize(8, (int) (fFaktor * (loco->getLocoAvgSpeed() - loco->getLocoMinSpeed())));
        m2->move(37, C_Y_START_ + sliderSpeed->height() - m1->height()
                 - m2->height() - 4);
        m3->resize(8, (int) (fFaktor * (loco->getLocoMaxSpeed() - loco->getLocoAvgSpeed())));
        m3->move(37, C_Y_START_ + sliderSpeed->height() - m1->height()
                 - m2->height() - m3->height() - 4);

        updateSpeedButtonText();
        updateDirectionPix();
        emit modified();
    }
}


void LocoControl::mousePressEvent(QMouseEvent* event)
{
    if (event->button() == RightButton) {
        contextmenu->exec(QCursor::pos());
        event->accept();
    }
}


void LocoControl::keyPressEvent(QKeyEvent* event)
{
    if (!event->isAutoRepeat())
        switch (event->key()) {
            case Key_Space:
                if (buttDirection->isEnabled())
                    slotReverseDirection();
                event->accept();
                break;
            case Key_X:
                if (event->state() == Qt::ControlButton) {
                    buttEmergency->animateClick();
                    event->accept();
                }
                else
                    event->ignore();
                break;
            case Key_B:
                if (event->state() == Qt::ControlButton) {
                    buttBreak->animateClick();
                    event->accept();
                }
                else
                    event->ignore();
                break;
            case Key_F1:
                handleFuncKeyPress(0);
                event->accept();
                break;
            case Key_F2:
                handleFuncKeyPress(1);
                event->accept();
                break;
            case Key_F3:
                handleFuncKeyPress(2);
                event->accept();
                break;
            case Key_F4:
                handleFuncKeyPress(3);
                event->accept();
                break;
            case Key_F5:
                handleFuncKeyPress(4);
                event->accept();
                break;
            default:
                event->ignore();
                break;
        }
}


void LocoControl::keyReleaseEvent(QKeyEvent* event)
{
    if (!event->isAutoRepeat())
        switch (event->key()) {
            case Key_F1:
                handleFuncKeyRelease(0);
                event->accept();
                break;
            case Key_F2:
                handleFuncKeyRelease(1);
                event->accept();
                break;
            case Key_F3:
                handleFuncKeyRelease(2);
                event->accept();
                break;
            case Key_F4:
                handleFuncKeyRelease(3);
                event->accept();
                break;
            case Key_F5:
                handleFuncKeyRelease(4);
                event->accept();
                break;
            default:
                event->ignore();
                break;
        }
}

void LocoControl::slotDelete()
{
    loco->setControllerVisible(false);
    delete loco;    // will take care of the rest
}

void LocoControl::handleFuncKeyPress(int fn)
{
    if (loco->getLocoFuncAvail(fn)) 
    {
        if (loco->getLocoFuncType(fn) == SWITCH) 
        {
            buttF[fn]->toggle();
            slotUpdateFuncsSwitched(fn);

        }
        else if (!buttF[fn]->isDown()) 
        {
            buttF[fn]->setDown(true);
            slotUpdateFuncsTouched(fn);
        }
    }
}


void LocoControl::handleFuncKeyRelease(int fn)
{
    if ((loco->getLocoFuncAvail(fn)) & (loco->getLocoFuncType(fn) == PUSHBUTTON)) 
    {
        buttF[fn]->setDown(false);
        slotUpdateFuncsTouched(fn);
    }
}

void LocoControl::updateDisplay()
{
    // update sliderSpeed
    if ((loco->getLocoSpeed()) != getSliderValue()) 
    {
        disconnect(sliderSpeed, SIGNAL(valueChanged(int)), this,
                   SLOT(slotUpdateSpeed(int)));
        updateSliderValue(loco->getLocoSpeed());
        updateDirectionEnable();
        connect(sliderSpeed, SIGNAL(valueChanged(int)),
                SLOT(slotUpdateSpeed(int)));

        // Update speed-display    
        updateSpeedButtonText();
    }
    updateDirectionFwd();

    // update functions
    for (int i = 0; i <= 4; i++) 
    {
        if (loco->getLocoFuncAvail(i)) 
        {
            if (loco->getLocoFuncType(i) == SWITCH) 
            {
                if (loco->getLocoFuncState(i) != buttF[i]->isOn()) 
                {
                    buttF[i]->setOn(loco->getLocoFuncState(i));
                    buttF[i]->setPixmap(loco->getFunctionPix(i));
                }
            }
        }
    }
}

void LocoControl::updateDirectionFwd()
{
    //if (loco->getLocoDirectionFwd() != )
    //TODO This should only be done on a change
    updateDirectionPix();
}

/*
 * set new bus value
 */
void LocoControl::setSrcpBus(unsigned int sbus)
{
    loco->setBus(sbus);
}

void LocoControl::slotHide()
{
    loco->setControllerVisible(false);
}

void LocoControl::slotControllerVisibleChanged(Loco *lc, bool state)
{
    if (state == false)
    {
        delete this;    // is not nice, but save if no more functions follow
    }
    
}

bool LocoControl::isMyLoco(Loco *lc)
{
    return (lc == loco);
}

void LocoControl::slotUpdate(void)
{
    qWarning("LocoControl::slotUpdate()");
    updateDisplay();
}

void LocoControl::updateSliderValue(int value)
{
    sliderSpeed->setValue(loco->getLocoMaxSpeed() - value);
}

unsigned int LocoControl::getSliderValue()
{
    return (loco->getLocoMaxSpeed() - sliderSpeed->value());
}

void LocoControl::updateDirectionEnable()
{
    // disable direction button if speed != 0
    if ((loco->getLocoSpeed() == 0) != buttDirection->isEnabled())
        buttDirection->setEnabled(loco->getLocoSpeed() == 0);
}

void LocoControl::slotM_GroupClicked( int i)
{
    if (i >= 0)
        loco->slotSetAutoSpeed((unsigned int)i);
}

void LocoControl::slotUpdateProperties(Loco *)
{
    //updateDisplay();    //TODO This should be more specific !
    updateDirectionPix();
    updateSpeedButtonText();
    updateSpeedSliderSettings();
    updateSliderValue(loco->getLocoSpeed());
    updateNameDisplay();
}
